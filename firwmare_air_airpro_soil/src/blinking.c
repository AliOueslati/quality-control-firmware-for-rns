#include "stm32l0xx.h"
#include "delay.h"
#include "LPtimer.h"
#include "rcc.h"
#include "led.h"
#include "config.h"

#define BEEPS TIME_BLINKING

static uint16_t count=0;


void blinking_start(void){
	
		count=0;
    RCC_ENABLE_APB1_PERIPHERAL(LPTIM1_APB1);
	  RCC->CCIPR |=(0x1<<18);	
	  ENABLE_LPTIMER(LPTIM1);
    SET_LPTIMER_ARR(LPTIM1,180);		
		EXTI->IMR |=(1UL<<29); //Unmask EXT_29		
		NVIC_SetPriority(LPTIM1_IRQn,2);          //Low priority
	  SET_LPTIMER_ARR_INTERRUPT_ENABLE(LPTIM1);
	  START_LPTIMER(LPTIM1);		
	  NVIC_EnableIRQ(LPTIM1_IRQn);
}


void LPTIM1_IRQHandler() {
	
	LPTIM1->ICR |= 0x2;
	
	count++;
  led_toggle(1);
	
	if (count==BEEPS)
	{
		NVIC_DisableIRQ(LPTIM1_IRQn);
		SET_LPTIMER_ARR_INTERRUPT_DISABLE(LPTIM1);
		DISABLE_LPTIMER(LPTIM1);
		EXTI->IMR &=~(1UL<<29); //mask EXT_29		
    RCC_DISABLE_APB1_PERIPHERAL(LPTIM1_APB1);
	}
	
}
