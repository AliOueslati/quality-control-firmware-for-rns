#ifndef RTC_H
#define RTC_H

#include "stm32l0xx.h"



void UNBLOCK_RTC_REGISTER(PWR_TypeDef *, RTC_TypeDef *);
uint8_t START_RTC_INIT(RTC_TypeDef *);
void SET_RTC_PREDIVS(RTC_TypeDef *, uint16_t);
uint8_t STOP_RTC_INIT(RTC_TypeDef *);
void SET_RTC_CALENDAR(RTC_TypeDef *, uint32_t *);
uint8_t GET_RTC_CALENDAR(RTC_TypeDef *, uint32_t *);
void SET_RTC_ALARM_A(RTC_TypeDef *, uint32_t, uint32_t);
void SET_RTC_ALARM_B(RTC_TypeDef *, uint32_t, uint32_t);


#endif
