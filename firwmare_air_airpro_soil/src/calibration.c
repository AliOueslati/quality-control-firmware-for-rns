#include "stm32l0xx.h"
#include "rcc.h"
#include "node.h"
#include "stdlib.h"
#include "config.h"
#include "calibration.h"
#include "eeprom.h"


void load_calibration_defaults(calibration_values *nodeCalibration) {
  nodeCalibration->calib_par_a1 = CALIB_PAR_A1;
  nodeCalibration->calib_soil_humidity_a0 = CALIB_SOIL_HUMIDITY_A0;
  nodeCalibration->calib_soil_humidity_a1 = CALIB_SOIL_HUMIDITY_A1;
}

/*
 * Read configuration struct from EEPROM.
 * If the data in EEPROM is a different version load the default configuration
 * values.
 */
void read_calibration(calibration_values *nodeCalibration) {
  EEPROM_READ(0x00, (uint8_t*)nodeCalibration, sizeof(*nodeCalibration));
  // Check if it was previuosly calibrated. If not, default values are stored
	if (nodeCalibration->calibrated == 0){
		load_calibration_defaults(nodeCalibration);
	}
}

/*
 * Write configuration struct to EEPROM.
 */
uint8_t write_calibration(calibration_values *nodeCalibration) {
  return EEPROM_WRITE(0x00, (uint8_t*)nodeCalibration, sizeof(*nodeCalibration));
}
