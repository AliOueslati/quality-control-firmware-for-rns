#ifndef DELAY_H
#define DELAY_H

#include "stm32l0xx.h"
void delay_init(void);
void delay_deinit(void);
void delay_us(uint16_t);
void delay_ms(uint16_t);
void delay_sec(uint8_t);

#endif
