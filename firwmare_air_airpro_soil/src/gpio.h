#ifndef GPIO_H
#define GPIO_H

#include "stm32l0xx.h"

/* PIN */
#define PIN0				0
#define PIN1				1
#define PIN2				2
#define PIN3				3
#define PIN4				4
#define PIN5				5
#define PIN6				6
#define PIN7				7
#define PIN8				8
#define PIN9				9
#define PIN10				10
#define PIN11				11
#define PIN12				12
#define PIN13				13
#define PIN14				14
#define PIN15				15

/* ALTERNATE FUNCTION */
#define AF0				0
#define AF1				1
#define AF2				2
#define AF3				3
#define AF4				4
#define AF5				5
#define AF6				6
#define AF7				7

void SET_GPIO_AS_OUTPUT(GPIO_TypeDef *, uint8_t);
void SET_GPIO_AS_INPUT(GPIO_TypeDef *, uint8_t);
void SET_GPIO_AS_AF(GPIO_TypeDef *, uint8_t, uint8_t);
void SET_GPIO_AS_ANALOG(GPIO_TypeDef *, uint8_t);
void SET_GPIO_PULL_UP(GPIO_TypeDef *, uint8_t);
void SET_GPIO_PULL_DOWN(GPIO_TypeDef *, uint8_t);
void SET_GPIO_NO_PULL(GPIO_TypeDef *, uint8_t);
void SET_GPIO_OPEN_DRAIN(GPIO_TypeDef *, uint8_t);
void SET_GPIO_SPEED_LOW(GPIO_TypeDef *, uint8_t);
void SET_GPIO_SPEED_MEDIUM(GPIO_TypeDef *, uint8_t);
void SET_GPIO_SPEED_HIGH(GPIO_TypeDef *, uint8_t);
void GPIO_CLEAR(GPIO_TypeDef *, uint8_t);
void GPIO_SET(GPIO_TypeDef *, uint8_t);
void GPIO_TOGGLE(GPIO_TypeDef *, uint8_t);
uint8_t GPIO_GET_INPUT(GPIO_TypeDef *, uint8_t);

#endif
