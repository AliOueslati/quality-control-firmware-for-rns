/*CAUTION: NOT ALL THE TIMERS SUPPORT THE SAME FEATURES. 
READ THE REFERENCE MANUAL*/

#include "stm32l0xx.h"
#include "timer.h"


/* ENABLE - DISABLE TIMER*/
void ENABLE_TIMER(TIM_TypeDef *TIMER){
   TIMER->CR1 |= 0x1;	
}	

void DISABLE_TIMER(TIM_TypeDef *TIMER){
   TIMER->CR1 &= ~0x1;
}	

/* SET PRESCALER*/
void SET_TIMER_PRESCALER(TIM_TypeDef *TIMER, uint16_t VALUE){
   TIMER->PSC =VALUE;
}	

/* UPDATE REGISTER */
void UPDATE_TIMER_REGISTER(TIM_TypeDef *TIMER){
   TIMER->EGR |=0x01;
}	

/* OVERFLOW*/
void SET_TIMER_OV_INTERRUPT_ENABLE(TIM_TypeDef *TIMER){
   TIMER->SR &= ~0x01;
   TIMER->DIER |= 0x1;	
}
void SET_TIMER_OV_INTERRUPT_DISABLE(TIM_TypeDef *TIMER){
   TIMER->SR &= ~0x01;
   TIMER->DIER &= ~0x1;	
}

/* PWM Channel 1*/
void SET_TIMER_PWM_MODE(TIM_TypeDef *TIMER){
   TIMER->ARR    =      1;      // period
   SET_TIMER_CMP_REG1(TIMER,1); // duty cycle
   
   TIMER->CCMR1 &= ~(3<<4);     // PWM mode 1
   TIMER->CCMR1 |=  (6<<4);
   
   TIMER->CCMR1 |=  (1<<3);     // OC1PE ON
   TIMER->CR1   |=  (1<<7);     // ARPE ON
   
   TIMER->CCER  |=   1;         // Activate output pin            
}

void SET_TIMER_PWM_MODE_mod(TIM_TypeDef *TIMER, uint16_t PERIOD){
   uint16_t cmp;
   TIMER->ARR    =  PERIOD;      // auto-reload register: period
   
   cmp=PERIOD>>1;
   if(cmp==0) cmp=1;
   
   SET_TIMER_CMP_REG1(TIMER, cmp); // duty cycle
   
   TIMER->CCMR1 &= ~(3<<4);     // PWM mode 1
   TIMER->CCMR1 |=  (6<<4);
   
   TIMER->CCMR1 |=  (1<<3);     // OC1PE ON
   TIMER->CR1   |=  (1<<7);     // ARPE ON
   
   TIMER->CCER  |=   1;         // Activate output pin            
}

/* COMPARE 1 FUNCTIONS*/
void SET_TIMER_CMP_REG1(TIM_TypeDef *TIMER,uint16_t VALUE){
   TIMER->SR &= ~0x02; // Clear previous channel matches
   TIMER->CCR1 = VALUE;	
}	

void SET_TIMER_CMP_REG1_INTERRUPT_ENABLE(TIM_TypeDef *TIMER){
   TIMER->DIER |= 0x2;	
}	

void SET_TIMER_CMP_REG1_INTERRUPT_DISABLE(TIM_TypeDef *TIMER){
   TIMER->DIER &= ~0x2;	
}	
