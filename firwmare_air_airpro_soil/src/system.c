#include "stm32l0xx.h"
#include "config.h"
#include "timer.h"
#include "rcc.h"
#include "delay.h"
#ifdef DEBUG_PRINT
   #include "print.h"
#endif
void system_sleep(void){
	delay_ms(100);
	__WFI();
}

void system_deep_sleep(void){
	delay_ms(100);

	//Apagar VrefInt(OK), BOR, PVD(OK), ADC(IA), DAC(NU), LCD(NU), sensor de temperatura (DF)

	SCB->SCR |= (uint32_t)SCB_SCR_SLEEPDEEP_Msk;
	RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);

	PWR->CR |= (1<<9);	//ULP
	PWR->CR &= ~0x02;   //PVD OFF
	PWR->CR &= ~0x02;   //PDDS
	PWR->CR |= 0x01;    //LPDSR
	RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);

	__WFI();


	//PWR->CR &= ~(1<<9);	//ULP
}

/* TIM14 used as timeOut timer. NOTE: Only valid is Timer Clock = SystemCoreClock */
void system_reset(void){
	delay_ms(200);
	NVIC_SystemReset();
}

//Low power range
uint8_t system_range(uint8_t range){
	uint32_t timeOut=0;
	uint8_t error=0;

	if (range>3)range=3;

	RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);
	while((PWR->CSR & (1UL<<4))!=0)
	{
   	if ((timeOut)++==0xFFFF){error=1; break;}
	}
	PWR->CR &= ~(0x3<<11);
	PWR->CR |= (range<<11);

	timeOut=0;
	while((PWR->CSR & (1UL<<4))!=0)
	{
		if ((timeOut)++==0xFFFFF){error=1; break;}
	}

 	RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);

	return error;
}

void change_voltage(uint8_t range) {
   uint32_t timeOut=0;
   static uint32_t back;
   uint8_t  error=0;

#if (DEBUG_PRINT>=2)   
   uint32_t aux;
   aux=PWR->CR;
   dbg_print("change_voltage: range="); print8(&range,1);
   dbg_print(",CR="); print32(&aux,1);
#endif   
   
   // TODO: Check correct <range> value
   
   // enable PWR interface
   RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);

   if (range!=3) { // backup of the LP config...
      back=PWR->CR;
      PWR->CR = (range<<11);
   }
   else {          // restore the LP config
      PWR->CR=back;
   }
   
   // Poll VOSF bit of PWR_CSR until is reset to 0
   while((PWR->CSR & (1UL<<4))!=0) {
      if ((timeOut)++==0xFFFF){error=1; break;}
   }

#if (DEBUG_PRINT>=2)   
   if(error!=0) 
     dbg_print("failed");
   aux=PWR->CR;
   dbg_print("--> CR="); print32(&aux,1);   dbg_print("\n\r");
#endif
   
   // disable PWR interface
   RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);
}
