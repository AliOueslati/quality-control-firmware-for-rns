// HX711 library
// allows interfacing to HX711 24bit ADC for weight scales
// Based on:
//   - Jose Garcia: peso
//   - ARM mbed: Cr300-Litho HX711
//   - Github bogde/HX711
// History:
//  17 Jun 2016: JChavez start
//  28 Jun 2016: JChavez hack to avoid hangups
//   7 Jul 2016: JChavez filter suppress Var computing, convergence with diff-Mean
//   4 Aug 2016: JChavez added timeout feature (to avoid hang if not connected)
//   5 Aug 2016: JChavez changed timeout function for timeout-for
// --------------------------------

#include "hx711.h"

#include "config.h"
#include "rcc.h"
#include "gpio.h"

// For dummy test
#include "uart.h"
#include "led.h"
#include "print.h"
#include "delay.h"
#include "button.h"

// External global variables
extern uint8_t BUTTON_LONG_INT;
extern uint8_t BUTTON_SHORT_INT;

#define MAX_TIMEOUT 0xffff

// Local global variable
// It is useful to use as a function argument to change in execution time?
//  25; // Channel A, gain factor 128
//  26; // Channel A, gain factor  64
//  27; // Channel A, gain factor  32
#define HX711_GAIN_PULSES  25

// Bits to suppress
#define HX711_FILTER_QBIT 10
#define HX711_FILTER_QPOW (1<<HX711_FILTER_QBIT)

// Value to set scaleValue (button long press)
#define HX711_SET_SCALE 200
uint32_t hx711OffsetValue;
int32_t  hx711ScaleValue=432; // for 5Kg scale


void hx711_init  (void) {
#ifdef HAS_HX711
   // HX711 scale initialization
   // ********************************
   // The 2-wire serial (SCK/SDA-DT) protocol is not compatible with I2C
   // - When SCK signal is held High for more than 60u, the IC goes PowerDown
   // - According the datasheet, once the SCK signal is low, the device goes
   // normal mode. For security we force a 100u delay
   // 
   // Not needed, its not I2C!!
   //RCC_ENABLE_APB1_PERIPHERAL(I2C1_APB1); // Pag63, I2C1 is APB1 periph
   
   // a) Enable Vdd (Chefi-Beta feature)
   // Rise Vdd to measure NTC (due to transitor is inverted)
   SET_GPIO_AS_OUTPUT(   SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   GPIO_CLEAR(           SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   
   // b) Pin definition 
   //GPIO_CLEAR(HX711_SCK_PORT,HX711_SCK_PIN);        // SCK low
   SET_GPIO_AS_OUTPUT(   HX711_SCK_PORT,  HX711_SCK_PIN);
   SET_GPIO_AS_INPUT(    HX711_SDA_PORT,  HX711_SDA_PIN);
   
   // c) HX711 reset
   GPIO_SET(             HX711_SCK_PORT,  HX711_SCK_PIN); // SCK high for 100us
   delay_us(100);
   GPIO_CLEAR(           HX711_SCK_PORT,  HX711_SCK_PIN); // SCK low again
#endif
}

void hx711_deinit(void) {
#ifdef HAS_HX711
   /// a) Vdd disable (avoids power waste)
   GPIO_SET(  SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);

   // b) Leave pin SCK high (device reset)
   GPIO_SET(HX711_SCK_PORT,HX711_SCK_PIN); // SCK high for 100us
#endif
}


uint32_t hx711_get_raw  (void) {
#ifdef HAS_HX711
   // HX711 get raw
   // ********************************
   // As commented, the serial protocol must be low-level made
   // - Keeping SCK low, the HX711 keeps SDA signal until conversion has 
   // finished, and puts DT signal low
   // - Each time a SCK rising edge is detected, the HX711 puts the next
   // data bit
   // - First data bit is LSB, and 24th is the MSB of conversion
   // - As a function of the total number of SCK pulses received, 
   // the "next conversion" is:
   //    25 SCK pulses: CH-A, gain 128
   //    26 SCK pulses: CH-B, gain 32
   //    27 SCK pulses: CH-A, gain 64
   // According to the datasheet
   // "Channel A can be programmed with a gain of 128 or 64, corresponding to a
   // full-scale differential input voltage of �20mV or �40mV respectively,
   // when a 5V supply is connected to AVDD analog power supply pin.
   // Channel B has a fixed gain of 32.".
   // - We are connected to CH-A
   // 
   // - Due the processing delays of the GPIO_SET/GPI_CLEAR functions,
   // using STM32 internal clock, delays are satisfied without further added
   // delays
   // - WARNING: if delay_us is executing in data retrieval, the device
   // can go PowerDown, if SCK high is higher than 60us limit!!
   // 
   // 
   uint8_t  pulse; // SCK pulse counter
   uint32_t value;  // RX-Data storage
   uint32_t timeOut;
   
   // Set SCK to low, and configure SCK as output and SDA as input
   GPIO_CLEAR(             HX711_SCK_PORT,HX711_SCK_PIN);
   SET_GPIO_AS_OUTPUT(     HX711_SCK_PORT,HX711_SCK_PIN);
   SET_GPIO_AS_INPUT (     HX711_SDA_PORT, HX711_SDA_PIN);
   
   // Wait until data is ready (SDA should change high to low)
   // TODO: what if device is no present??
   
   timeOut=0;
   while( (timeOut<MAX_TIMEOUT) && GPIO_GET_INPUT(HX711_SDA_PORT, HX711_SDA_PIN) ) 
     timeOut++;
   
   if(timeOut==MAX_TIMEOUT) {
      dbg_print("hx711_get_raw: no hx711!!\n\r");
      return 0;
   }
   
   value=0;
   // Read the 24 bits serialized
   for(pulse=0;pulse<24;pulse++) {
      // SCK high    
      GPIO_SET  (HX711_SCK_PORT, HX711_SCK_PIN); 
      //delay_us(4);
      // Sample bit and shift
      value|=((uint32_t)GPIO_GET_INPUT(HX711_SDA_PORT, HX711_SDA_PIN))<<(23-pulse);
      // SCK low
      GPIO_CLEAR(HX711_SCK_PORT, HX711_SCK_PIN); 
      //delay_us(4);
   }
   
   // Set Gain for next conversion
   for(; pulse<HX711_GAIN_PULSES;pulse++ ) {
      GPIO_SET  (HX711_SCK_PORT,HX711_SCK_PIN); 
      //delay_us(4);
      GPIO_CLEAR(HX711_SCK_PORT,HX711_SCK_PIN); 
      //delay_us(4);
   }
   value=value^0x800000;

   // Hold SCK high to reset
   GPIO_SET  (HX711_SCK_PORT,HX711_SCK_PIN); 

   return value;
#else
   return 0;
#endif
}


uint16_t hx711_get_value  (void) {
#ifdef HAS_HX711
   // Get the raw value
   // The return quantized value
   uint32_t rawValue; // 24bits resolution
   uint16_t qValue;   // 14bits resolution
   
   
   delay_ms(100);
   rawValue=hx711_get_raw();
   
   // Quantize
   qValue=rawValue>>HX711_FILTER_QBIT;
   
#if(DEBUG_PRINT>=2)
   dbg_print("hx711_get_value: ");
   print32(&rawValue,1);
   dbg_print("\r\n");
#endif
   
   return qValue;
#else
   return 0;
#endif
}

void  hx711_do_calibration  (uint32_t Mode) {
#ifdef HAS_HX711
   // Calibration Function
   // Mode=0  get the offset value
   // Mode=1  get the scale value, for an external
   // returns meanValue
   // --------------------------------
   uint16_t meanValue;
   
   meanValue=hx711_get_value();
   
   switch(Mode) {
    case 0: // Compute the offset
      hx711OffsetValue=meanValue;
      break;
    case 1: // Compute Scaling (offset should be previously computed)
      hx711ScaleValue=((meanValue-hx711OffsetValue)*HX711_FILTER_QPOW)/HX711_SET_SCALE;
      break;
    default:
      dbg_print("hx711_do_calibration ERROR\n\r");
      break;
   }
   
#if(DEBUG_PRINT>=1)
   dbg_print("hx711_cal: Offset "); print32(&hx711OffsetValue,1);
   dbg_print(" Scale ");  print32((uint32_t *)&hx711ScaleValue,1);
   dbg_print("\n\r");
#endif   
#endif
}

int16_t hx711_get_weight (void) {
#ifdef HAS_HX711
   // Get stable value and convert to grams
   // WARN: callibration has to be performed previously
   // --------------------------------
   uint16_t meanValue;
   int32_t  realValue;
   
   meanValue=hx711_get_value();
   
   realValue=((int32_t)(meanValue-hx711OffsetValue)*10*HX711_FILTER_QPOW)/hx711ScaleValue;
   
   return (int32_t) realValue;
#else
   return 0;
#endif
}

int16_t hx711_procedure(void) {
#ifdef HAS_HX711
   // Procedure to get the load-cell weight sensor
   // SHORT-BUTTON-PRESS  the measure is Offset
   // LONG-BUTTON-PRESS   computes scale
   // NOTE: as the returned value is the scaled one, calibration must be made
   // prior to get correct values
   int16_t  hx711Value;

   
   hx711_init();
   
   hx711Value=(int16_t) -1;
   
   if (BUTTON_SHORT_INT) {
      led_on(LED_GREEN);
      
      hx711_do_calibration(0);
      
      led_off(LED_GREEN);
      dbg_print("weight Offset compute\n\r");
      BUTTON_SHORT_INT=0;
   } else {
      if(BUTTON_LONG_INT) {
	 led_on(LED_GREEN);
	 
	 hx711_do_calibration(1);
	 
	 led_off(LED_GREEN);
	 dbg_print("weight Scale compute\n\r");
	 BUTTON_LONG_INT=0;
      }
      else {
	 led_on(LED_RED);
	 
	 hx711Value=hx711_get_weight();
	 
	 led_off(LED_RED);
	 
#if(DEBUG_PRINT>=1)
	 dbg_print("hx711_proc: weight=");
	 print_BCD(hx711Value,1);
	 dbg_print("(0x"); print32((uint32_t *)&hx711Value,1);
	 dbg_print(")\r\n");
#endif	 
      }
   }   

   hx711_deinit();
   
   return hx711Value;
#else
   return 0;
#endif
}
