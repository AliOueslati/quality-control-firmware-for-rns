#ifndef I2C_H
#define I2C_H


#include "stm32l0xx.h"



//#define FAST_MODE_400K  0x00901A53
//#define STD_MODE_100K   0xB0421B17

#define FAST_MODE_400K  0x0010020A
#define STD_MODE_100K   0x00000609


void I2C_DISABLE(I2C_TypeDef *);

void I2C_ENABLE(I2C_TypeDef *, uint32_t, uint8_t SMBus);

void I2C_SET_NBYTES(I2C_TypeDef *, uint8_t);

void I2C_SET_SADD_7BITS(I2C_TypeDef *, uint8_t);

void I2C_SET_READ(I2C_TypeDef *);

void I2C_SET_WRITE(I2C_TypeDef *);

uint8_t I2C_START(I2C_TypeDef *);

uint8_t I2C_STOP(I2C_TypeDef *);

uint8_t I2C_WAIT_TC(I2C_TypeDef *);

uint8_t I2C_WRITE(I2C_TypeDef *, uint8_t);

uint8_t I2C_PECBYTE(I2C_TypeDef *I2C);

uint8_t I2C_READ(I2C_TypeDef *, uint8_t *);

#endif
