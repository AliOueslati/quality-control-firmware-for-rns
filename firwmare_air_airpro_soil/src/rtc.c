#include "rtc.h"
#include "hexutil.h"
#include "rcc.h"

#define TIMEOUT_ERROR 0xFFFF

uint8_t START_RTC_INIT(RTC_TypeDef *TIME) {
	uint16_t timeOut=0;
	uint8_t error=0;

	RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);
	PWR->CR           |= (1UL<<8);
	RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);
	
	RCC->CSR &= ~(0x3UL<<16);  
	RCC->CSR |= (1UL<<17);     //LSI clock to RTC
	RCC->CSR |= (1UL<<18);     //RTC enable
	
	TIME->WPR          = 0xCA;					          // Disable write protection
	TIME->WPR          = 0x53;
	
	TIME->ISR          = 1<<7;                    // Init mode
	while ((TIME->ISR  & (1<<6))==0)                // Init mode succesfull
  {
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		
	
	
	return error;
	
}

uint8_t STOP_RTC_INIT(RTC_TypeDef *TIME) {
	uint16_t timeOut;
	uint8_t error=0;
	
	TIME->ISR &= ~(1<<7);
	
	TIME->ISR &= ~(1<<5);
	while ((TIME->ISR  & (1<<5))==0)               
  {
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		
		
	return error;
}


void SET_RTC_CALENDAR(RTC_TypeDef *TIME, uint32_t *date) {
	TIME->TR=*date;
	TIME->DR=*(date+1);
}

void SET_RTC_PREDIVS(RTC_TypeDef *TIME, uint16_t value) {
	uint32_t reg;
	reg  =TIME->PRER;
	reg  =reg&~(0x7FFF);
	reg |=value;
	TIME->PRER= reg;
}


uint8_t GET_RTC_CALENDAR(RTC_TypeDef *TIME, uint32_t *date) {
	uint16_t timeOut;
	uint8_t error=0;
	
	TIME->ISR &= ~(1<<5);
	while ((TIME->ISR  & (1<<5))==0)               
  {
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		
	
	*(date+2) = TIME->SSR;
	
	*date     = TIME->TR;

	*(date+1) = TIME->DR;


	
	return error;
}



void SET_RTC_ALARM_A(RTC_TypeDef *TIME, uint32_t alarm, uint32_t subsec) {
	
	TIME->CR &= ~(1<<8);
	TIME->CR &= ~(1<<12);
	
	TIME->ISR &= ~(1<<8);
	TIME->ALRMAR =alarm;
	
	TIME->ALRMAR |= (1UL<<31); //Day do not care
	TIME->ALRMAR &=~(1UL<<23);
	TIME->ALRMAR &=~(1UL<<15);
	TIME->ALRMAR &=~(1UL<<7);
	
	TIME->ALRMASSR = subsec;
	TIME->ALRMASSR |= 0x0F000000;

	//Configure EXT
	EXTI->IMR|=(1UL<<17); //Unmask EXT_17	
	EXTI->RTSR|=(1UL<<17); //RISING edge
	
	TIME->CR |= (1<<8);
	TIME->CR |= (1<<12);
	
	NVIC_EnableIRQ(RTC_IRQn);
	
}

void SET_RTC_ALARM_B(RTC_TypeDef *TIME, uint32_t alarm, uint32_t subsec) {
	
	TIME->CR &= ~(1<<9);
	TIME->CR &= ~(1<<13);
	
	TIME->ISR &= ~(1<<9);
	TIME->ALRMBR =alarm;
	
	TIME->ALRMBR |= (1UL<<31); //Day do not care
	TIME->ALRMBR &=~(1UL<<23);
	TIME->ALRMBR &=~(1UL<<15);
	TIME->ALRMBR &=~(1UL<<7);

	TIME->ALRMBSSR = subsec;
	TIME->ALRMBSSR |= 0x0F000000;
	
  //Configure EXT
	EXTI->IMR|=(1UL<<17); //Unmask EXT_17	
	EXTI->RTSR|=(1UL<<17); //RISING edge
	
	TIME->CR |= (1<<9);
	TIME->CR |= (1<<13);
	
	NVIC_EnableIRQ(RTC_IRQn);
	
	
}
