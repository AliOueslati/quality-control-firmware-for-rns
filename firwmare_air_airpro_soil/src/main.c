// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// NR Beta code
// Check config.h for adequate config
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Behaviour:
// Boot: 
//    Both leds on for 20ms
// Normal state:
//    Then both leds off
// - Red      on while radio receive
//            while RX timeout
// - Blue     on while radio receive
// HardFault:
//    Both leds on, forever
#include "stm32l0xx.h"
#include "led.h"
#include "qc.h"
#include "config.h"
#include "battery.h"
#include "gpio.h"
#include "delay.h"
#include "config.h"
#include "rcc.h"
#include "sensors.h"
#include "button.h"
#include "system.h"
#include "lora.h"
#include "lora.h"
#include "node.h"
#include "id.h"
#include "calendar.h"
#include "hexutil.h"
#include "calibration.h"
#include "soil.h"
#include "blinking.h"
#include "uart.h"
#include "print.h"
#include "calendar.h"
#include "hx711.h"
#include "cozir.h"
#include "watermelon.h"
// Use green LED when no blue available
#ifndef LED_BLUE
#define LED_BLUE LED_GREEN
#endif

#define SWAP(z) ((z&0xFF00)>>8)|((z&0x00FF)<<8)


extern volatile uint8_t ALARM_0_PENDING;
extern uint8_t Cozir_Humid_Counter;


uint8_t BUTTON_LONG_INT   =0;
uint8_t BUTTON_SHORT_INT  =0;
uint8_t LORA_INT          =0;
uint8_t MEASURE           =1;

LORAData            loraData;
LORAConfig          loraConfig;
node_info           nodeInfo;
calibration_values  calibrationValues;

uint16_t RX_OK=0;
uint16_t RX_KO=0;

int main (void) {
   
   enum {LIVE, SEND_MEASURES} node_state = LIVE;
   uint32_t id;
   uint32_t timeOut;
   rtc_calendar tmpDate = {0,0,0,0,0,0,0,0};
   uint16_t tSample;
   
   // Use Lora uint8_t data buffer as uint16_t
   uint16_t *idxData=(uint16_t *)loraData.DATA;
   extern uint32_t SystemCoreClock;
   
  
	
	 // set main clock to 25Mhz
	//RCC_CLOCK_MSI_4();
	 //RCC_CLOCK_HSE_25();
	 //use external oscillator for RTC

	 //RCC_ENABLE_CLOCK_LSE32K();
	  //system_range(3);
	 
	 
	 // Set main clock to 2 MHz
   RCC_CLOCK_MSI_2();
   // Use internal oscillator for RTC
   RCC_ENABLE_CLOCK_LSI();
    
	 
   // Set internal voltage regulator to range 3, 1.2V
   system_range(3);

   //RCC_SET_CLK( CLK_SRC_MSI, 1);
   //RCC_SET_CLK( CLK_SRC_HSI, 1);
   //RCC_SET_CLK( CLK_SRC_HSE, 1);
   
   RCC_ENABLE_GPIO(GPIOA_IO);    //Enable GPIOA clock
   RCC_ENABLE_GPIO(GPIOB_IO);    //Enable GPIOB clock
   // needed for ntc (TODO: #ifdef for soil board?)
   RCC_ENABLE_GPIO(GPIOC_IO);
   
   //Lora Modem Configuration 
   // Taken fron config.h
   loraConfig.BW       = LORA_BW;
   loraConfig.SF       = LORA_SPREADING_FACTOR;
   loraConfig.CR       = LORA_CODING_RATE;
   // Harcoded
   loraConfig.TX_POWER = 17;
   loraConfig.FREQ     = LORA_FREQ;
   
   xtal_init();
   mux_init();
   led_init();
   delay_init();
   calendar_init();
   calendar_set(&tmpDate);
   led_on(LED_BLUE);
   led_on(LED_RED);
   delay_ms(200);
   led_off(LED_BLUE);
   led_off(LED_RED);
   delay_ms(800);
   sensors_init();
   button_init();
   lora_init();
   dbg_print_init();
   WatermelonInit();
	 qc_init();

	 
   //cozirInit();		//Initialize CO2 sensor  -Thimo
#ifndef SENSOR_WATERMELON
	cozirDisable();
#endif 
/*#ifndef SENSOR_CO2_COZIR
   soil_deinit(); // Guarantee the soil alg's don't harm...
#endif*/

   ID_READ(&id);      
   read_calibration(&calibrationValues);
   
  // nodeInfo.centralID=0;          //Start unpaired
   nodeInfo.nodeID=(uint16_t)id;
   nodeInfo.msg_type = MSG_TYPE;
   nodeInfo.tsample = TSAMPLE;
   
   // JChavez DBG
#ifdef DEBUG_PRINT
   delay_sec(2);
   dbg_print("*** Chefi One DBG (v0.92)\n\r");
   dbg_print("UID=");
   print_unsBCD( nodeInfo.nodeID,0);
   dbg_print("(0x"); print16(&nodeInfo.nodeID,1); dbg_print(")\r\n");
	 dbg_print("LORA Frequency = "); print_BCD(LORA_FREQ,0);
   dbg_print(", msg_type=");print_BCD(nodeInfo.msg_type,0);
   dbg_print(" SysClk="); print_BCD(SystemCoreClock/1000,0);
   dbg_print("kHz\n\r");
   dbg_print("Lora cfg: ");
# ifdef LORA_CRC
   dbg_print("CRCon,");
# else   
   dbg_print("CRCoff,");
# endif   
   dbg_print( (loraConfig.SF==MC2_SF12)? "SF12\n\r":
	      (loraConfig.SF==MC2_SF9)? "SF9\n\r":"??\n\r"
	    );
#ifdef HAS_HX711
   dbg_print("HAS_HX711: Need Load Cell connected...\n\r");
#endif
#else
	 srv_print("UID=");
   print_unsBCD( nodeInfo.nodeID,0);
#endif
   
   calendar_alarm(nodeInfo.tsample, 0);

   while(1){
      switch (node_state){

	 /****** LIVE ******/
       case LIVE:
	 dbg_print("**state=Live\n\r");
	 LORA_INT = 0;
	 radio_init_tx(&loraConfig);
	 radio_start_sleep();
	 if(!(ALARM_0_PENDING || BUTTON_SHORT_INT)){
#ifndef DEBUG
		 if(Cozir_Humid_Counter == 0){
	    system_deep_sleep();
		 }
#else
	    while (!(ALARM_0_PENDING || BUTTON_SHORT_INT)){};
#endif
      }
	 if(ALARM_0_PENDING){
	    ALARM_0_PENDING = 0;
	    node_state = SEND_MEASURES;
	 }
	 if(BUTTON_SHORT_INT){
	    dbg_print("Short Button, UID=");
	    print_unsBCD( nodeInfo.nodeID,0);
	    dbg_print("\n\r");
		 	
#ifndef HAS_HX711
	    BUTTON_SHORT_INT=0; // Hx711 only activated when defined (for scales)
#endif
	    node_state = SEND_MEASURES;
	 }
	 if(BUTTON_LONG_INT){
		dbg_print("Long Button\n\r");
		 dbg_print("\n\r");
		 dbg_print("******* QC PART*********\r\n");
     dbg_print("\r\n");	
		 dbg_print("Start quality control test for the RN-ID:");print_unsBCD( nodeInfo.nodeID,0);
		 	dbg_print("\n\r");
	    dbg_print("\n\r");		 
		qc_read(&nodeInfo);
#ifdef SENSOR_WATERMELON
		//cozirCalibrate();
		node_state = SEND_MEASURES;
#endif
#ifdef WATERMELON_TEMP
		node_state = SEND_MEASURES;
#endif
#ifdef WATERMELON_EC
		node_state = SEND_MEASURES;
#endif
		 //BUTTON_LONG_INT=0;
	 }
	 break;
	 /****** SEND MEASURES ******/
       case SEND_MEASURES:
	if (BUTTON_LONG_INT ==1)
		{
		dbg_print("\r\n");	
		dbg_print("//=======Lora verification======//\r\n");
    dbg_print("\r\n");	
		}	
	 dbg_print("**state=Send\n\r");
	 button_mask_irq();			
	 loraData.LENGTH = compose_msg (idxData, &nodeInfo);
	 timeOut=0;
	 LORA_INT=0;
	 if(loraData.LENGTH > 0){
	    radio_start_tx(&loraData,CENTRAL_NODE_ADDRESS);
	    while(!LORA_INT) {
	       if (timeOut++ == TRANSMIT_TIMEOUT){
		  dbg_print("Lora TX TimeOut\n\r");
		  break;
	       }
	    }
	    button_unmask_irq();
	    
	    node_state = LIVE;                    //Default next state and alarm
	    tSample=nodeInfo.tsample;
	    
	    if (timeOut < TRANSMIT_TIMEOUT){      //TX success
	       LORA_INT=0;
	       radio_init_rx(&loraConfig); //Reconfigure modem for RX
	       radio_start_rx_single();    //Start RX mode for single reception
	       timeOut=0;
	       
	       led_on(LED_BLUE);// here
	       while(!LORA_INT) {
		  if (timeOut++ == RECEIVE_TIMEOUT){
		     dbg_print("RX TimeOut\n\r");
				 led_off(LED_BLUE); delay_ms(200);				//to show if RX received late or not at all//
				 led_on(LED_RED); delay_ms(200);
		     RX_KO++;
		     break;
		  }
	       }
	       led_off(LED_RED);
				 led_off (LED_BLUE);

	       
	       if (timeOut < RECEIVE_TIMEOUT){  
		  RX_OK++;
					 delay_ms(3000);
	
#ifdef DEBUG_PRINT						 
			 dbg_print("UID=");
			 print_unsBCD( nodeInfo.nodeID,0);
			// dbg_print("(0x"); print16(&nodeInfo.nodeID,1); dbg_print(")\r\n");
			// dbg_print("centralID=" );print_BCD(nodeInfo.centralID,0);
			 dbg_print(", msg_type=");print_BCD(nodeInfo.msg_type,0);
			 dbg_print("\r\n");
			 dbg_print("LORA Frequency = "); print_BCD(LORA_FREQ,0);
			//dbg_print("Tsample=" );print_BCD(nodeInfo.tsample,0);//}
      dbg_print("\n\r");
		  dbg_print("RX Success, destId=0x");
		  print16(&idxData[1],1);
		 // dbg_print(" msgId=0x");
		 // print16(&idxData[0],1);
		  dbg_print(" rssi=0x");
		  print8(&loraData.RSSI,1);
		  dbg_print("\n\r");
					 if (BUTTON_LONG_INT ==1){
						 dbg_print("\r\n");	
						 dbg_print("//======= END OF Lora verification======//\r\n");
             dbg_print("\r\n");
						 dbg_print("*******END QC PART*********\r\n");
             dbg_print("\r\n");	
					 }
	 BUTTON_LONG_INT=0;
#endif  
		  //Rx is done in IRQ handler
		  if (loraData.VALID) {
		     
		     nodeInfo.signal=loraData.RSSI;
		     if (idxData[0]==nodeInfo.nodeID){
			
			if (idxData[2]==0x44){ //pair
			   nodeInfo.centralID=idxData[1];
			   ALARM_0_PENDING=1;
			}
			
			if ((idxData[2]==0x66) & (nodeInfo.centralID==idxData[1])){ //unpair
			   nodeInfo.centralID=0; 
			   ALARM_0_PENDING=1;
			}
			
			if (idxData[2]==0xEE){ //unpair Sigrow
			   nodeInfo.centralID=0; 
			   ALARM_0_PENDING=1;
			}
			
			if (idxData[2]==0x50){ //change sampling time
			   nodeInfo.tsample=SWAP(idxData[3]);
			   if (nodeInfo.tsample<MIN_TSAMPLE)nodeInfo.tsample=MIN_TSAMPLE;
			   if (nodeInfo.tsample>MAX_TSAMPLE)nodeInfo.tsample=MAX_TSAMPLE;
			   tSample=nodeInfo.tsample;
			   ALARM_0_PENDING=1;
			   dbg_print("RX Tsample=");print_BCD(nodeInfo.tsample,0); dbg_print("\r\n");
			}
			
			if (idxData[2]==0x51){ //blinking led
			   blinking_start();
			}
			if (idxData[2]==0x52){ //Calibration Mode
			   
			   if (nodeInfo.PAR > 1) {
			      if ((SWAP(idxData[3])) < 32000) {
				 // calibrate PAR
				 calibrationValues.calib_par_a1 *= (float)(SWAP(idxData[3]))/ (float)nodeInfo.PAR;
				 // limit values to feasible range
				 if (calibrationValues.calib_par_a1 < 0.001) calibrationValues.calib_par_a1 = 0.001;
				 if (calibrationValues.calib_par_a1 > 10.0)  calibrationValues.calib_par_a1 = 10.0;
			      }
			      
			      if ((SWAP(idxData[4]))< 32000) {
				 // calibrate soil humidity a1 (a0 is calibrated at startup)
				 calibrationValues.calib_soil_humidity_a1 *= (float)(SWAP(idxData[4])) / (float)nodeInfo.SoilHum;
				 // limit values to feasible range
				 if (calibrationValues.calib_soil_humidity_a1 > -0.001) calibrationValues.calib_soil_humidity_a1 = -0.001;
				 if (calibrationValues.calib_soil_humidity_a1 < -10.0)  calibrationValues.calib_soil_humidity_a1 = -10.0;
			      }
			      
			      calibrationValues.calibrated = 1; //Calibration succesful
			      write_calibration(&calibrationValues);
			      calendar_alarm(nodeInfo.tsample, 0);
			       
			      led_on(LED_RED);
			      delay_ms(500);
			      led_off(LED_RED);
			      delay_ms(500);
			      led_on(LED_RED);
			      delay_ms(500);
						led_off(LED_RED);
			      ALARM_0_PENDING=1;
			   }
			}
			if (idxData[2]==0x53){ //Set radio to low power
			   //future implementation
			}
			if (idxData[2]==0x54){ //Set radio to high power
			   //future implementation
			}
			if (idxData[2]==0x55){ //Set node to default
			   radio_init_tx(&loraConfig); //Reconfigure modem for TX
			   radio_start_sleep();
			   nodeInfo.tsample = TSAMPLE;
			   ALARM_0_PENDING=1;
			}							
		     }
		  }
	       }
	    }
	    
	 }
	 else{
	    node_state = LIVE;                    //Default next state and alarm
	    tSample=nodeInfo.tsample;
	    led_on(LED_RED);
	    delay_ms(1000);
	    led_off(LED_RED);
	 }
	 
	 calendar_alarm(tSample, 0);	
	 break;
      }
   }
}

// IRQ handler of radio and button is defined here because they share the IRQ on v2k board
void EXTI4_15_IRQHandler(void){
   if (EXTI->PR & 1UL<< LORA_DIO0_PIN){
      
      uint8_t flags = readReg(LORARegIrqFlags);
      if( flags & IRQ_LORA_RXDONE_MASK ){
	 radio_receive(&loraData);
	 // go from stanby to sleep
	 radio_start_sleep();
      }
      //clear radio IRQ flags
      writeReg(LORARegIrqFlags, 0xFF);
      EXTI->PR |= 1UL<< LORA_DIO0_PIN;
      LORA_INT=1;
   }
   
   // Process button interupt
   if (EXTI->PR & 1UL<<BUTTON_PIN)	{
      BUTTON_SHORT_INT = 0;
      BUTTON_LONG_INT = 1;
      for (int n = 0; n<400; n++) {
	 if (!button_get_status()) {
	    if (n < 10) {
	       BUTTON_SHORT_INT = 0;
	    }
	    
	    else {
	      BUTTON_SHORT_INT = 1;
	    }
	    
	    BUTTON_LONG_INT = 0;
	    break;
	 }
	 delay_ms(1);
      }
      EXTI->PR |= 1UL<<BUTTON_PIN;
   }
}

void HardFault_Handler(void) {//HERE
   led_on(LED_RED);
   led_on(LED_BLUE);
   led_on(LED_BLUE);
   //NVIC_SystemReset();
}
