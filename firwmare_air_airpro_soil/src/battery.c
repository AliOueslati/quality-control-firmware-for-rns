#include "stm32l0xx.h"
#include "pvd.h"
#include "rcc.h"
#include "print.h"
#include "adc.h"

/* Result 3 bits
0->     Vdd<1.9
1-> 1.9<Vdd<2.1
2-> 2.1<Vdd<2.3
3-> 2.3<Vdd<2.5
4-> 2.5<Vdd<2.7
5-> 2.7<Vdd<2.9
6-> 2.9<Vdd<3.1
7-> 3.1<Vdd
*/

/* Result 2 bits

0->     Vdd<2.1
1-> 2.1<Vdd<2.5
2-> 2.5<Vdd<2.9
3-> 2.9<Vdd
*/


void battery_measure(uint8_t *level){
   uint8_t th=0;
   uint8_t stop=0;
   uint8_t status;
   *level=7;
   
   PVD_DISABLE();
   while ((!stop) && (th<7))  {
      PVD_SET_THRESHOLD(th);
      PVD_ENABLE();
      PVD_GET_STATUS(&status);
      if (status) {
	 stop=1;
	 *level=th;
      }
      PVD_DISABLE();
      th++;
   }
}

// Get the battery voltage (before the regulator), using ADC2 (A2 pin)
uint16_t get_batt_voltage ( ) {
   // TODO: error ignored?
   // Fix enable error? calib=0enable=1read=1batt=3886error=1
   uint8_t error;
   uint16_t value;
   
   // Adc setup
   RCC_ENABLE_APB2_PERIPHERAL(ADC_APB2);
   ADC_CLK_SOURCE(ADC1, CK_PCLK_2);
   error=ADC_CALIBRATION(ADC1);
//   dbg_print("calib="); print_unsBCD( (uint16_t) error,0);//1
   
   ADC_SET_OV(ADC1,OV_256);
   error = ADC_ENABLE(ADC1);
  // dbg_print("enable="); print_unsBCD( (uint16_t) error,0);//2

   ADC_SET_AUTOFF(ADC1);
   ADC_SET_SINGLE_CONVERSION(ADC1);
   ADC_SET_CHANNELS(ADC1, CH_2);
   
   // ADC start
   ADC_START(ADC1);
   
   error |= ADC_READ_DATA(ADC1, &value);
   //dbg_print("read="); print_unsBCD( (uint16_t) error,0);//3
   
   // Disable ADC after use
   error |= ADC_DISABLE(ADC1);
   RCC_DISABLE_APB2_PERIPHERAL(ADC_APB2);
   //4
   //dbg_print("batt=");  print_unsBCD( (uint16_t) value,0);
   //dbg_print("error="); print_unsBCD( (uint16_t) error,0); dbg_print("\n\r");
   
   return value;
}
