#include "stm32l0xx.h"

#define TIMEOUT_ERROR 0x1FFF

void SPI_DISABLE(SPI_TypeDef *SPI){
	SPI->CR1 &=~(1UL<<6);
}

void SPI_ENABLE(SPI_TypeDef *SPI){
	SPI->CR1 |= (1UL<<6);
}

void SPI_SET_UNIDIRECTIONAL_MODE(SPI_TypeDef *SPI){
	SPI->CR1 &= ~(1UL<<15);
}

void SPI_SET_DFF_8BITS(SPI_TypeDef *SPI){
	SPI->CR2 |= (1<<12);
	SPI->CR2 &= ~(15<<8); 
	SPI->CR2 |= 7<<8;

}

void SPI_SET_FULL_DUPLEX_MODE(SPI_TypeDef *SPI){
	SPI->CR1 &= ~(1UL<<10);
}

void SPI_SEND_MSB_FIRST(SPI_TypeDef *SPI){
	SPI->CR1 &= ~(1UL<<7);
}

void SPI_ENABLE_SOFTWARE_SLAVE_MANAGEMENT(SPI_TypeDef *SPI){
	SPI->CR1 |= 1UL<<9;
}

void SPI_SET_NSS_HIGH(SPI_TypeDef *SPI){
	SPI->CR1 |= 1UL<<8;
}

void SPI_SET_BAUDRATE_PRESCALER(SPI_TypeDef *SPI, uint8_t prescaler){

	if (prescaler > 7) {
		return;
	}
	SPI->CR1 &= ~(0x38);
	SPI->CR1 |= (prescaler << 3);
}

void SPI_SET_MASTER_MODE(SPI_TypeDef *SPI){
	SPI->CR1 |= 1UL<<2;
}

void SPI_SET_CLOCK_POLARITY_0(SPI_TypeDef *SPI){
	SPI->CR1 &= ~(1UL<<1);
}

void SPI_SET_CLOCK_PHASE_0(SPI_TypeDef *SPI){
	SPI->CR1 &= ~(1UL<<0);
}

void SPI_ENABLE_SS_OUTPUT(SPI_TypeDef *SPI){
	SPI->CR2 |= (1UL<<2);
}

uint8_t SPI_WRITE_8(SPI_TypeDef *SPI, uint8_t dataTx){
	uint8_t error=0;
	uint16_t timeOut=0;
	
	while ((SPI->SR & 0x02)==0)
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}	
	*((__IO uint8_t *)(&(SPI->DR)))=dataTx;
	
	return error;
}

uint8_t SPI_WRITE_16(SPI_TypeDef *SPI, uint16_t dataTx){
	uint8_t error=0;
	uint16_t timeOut=0;
	
	while ((SPI->SR & 0x02)==0)
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}
	SPI->DR = dataTx;
	
	return error;
}

uint8_t SPI_XFER_8(SPI_TypeDef *SPI, uint8_t dataTx, uint8_t *dataRx){
	uint8_t error=0;
	uint16_t timeOut=0;
	
	SPI_WRITE_8(SPI, dataTx);

	while ((SPI->SR & 0x01)==0) // Wait for transfer finished
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}	
	*dataRx=(uint8_t)SPI->DR; 	
	
	return error;
}

uint8_t SPI_XFER_16(SPI_TypeDef *SPI, uint16_t dataTx, uint16_t *dataRx){
	uint8_t error=0;
	uint16_t timeOut=0;
	
	SPI_WRITE_16(SPI, dataTx);

	while ((SPI->SR & 0x01)==0) // Wait for transfer finished
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		

	*dataRx=SPI->DR; 	// Read the 16-bit data from DR. 
	
	return error;
}
