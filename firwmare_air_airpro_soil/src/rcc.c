#include "stm32l0xx.h"
#include "config.h"
#include "rcc.h"
#include "system.h"
#include "print.h"

/////////* ENABLE - DISABLE AHB PERIPHERALS*////////////
void RCC_ENABLE_AHB_PERIPHERAL (uint32_t PER){
   RCC->AHBENR |=PER;
}	
void RCC_DISABLE_AHB_PERIPHERAL (uint32_t PER){
   RCC->AHBENR &=~PER;
}	

/////////* ENABLE - DISABLE GPIOS*////////////
void RCC_ENABLE_GPIO (uint32_t PER){
   RCC->IOPENR |=PER;
}	
void RCC_DISABLE_GPIO (uint32_t PER){
   RCC->IOPENR &=~PER;
}	


/////////* ENABLE - DISABLE APB1 PERIPHERALS*////////////
void RCC_ENABLE_APB1_PERIPHERAL (uint32_t PER){
   RCC->APB1ENR |=PER;
}	
void RCC_DISABLE_APB1_PERIPHERAL (uint32_t PER){
   RCC->APB1ENR &=~PER;
}

/////////* ENABLE - DISABLE APB2 PERIPHERALS*////////////
void RCC_ENABLE_APB2_PERIPHERAL (uint32_t PER){
   RCC->APB2ENR |=PER;
}	
void RCC_DISABLE_APB2_PERIPHERAL (uint32_t PER){
   RCC->APB2ENR &=~PER;
}

/*
 * Set LS clock source to external 32.768kHz crystal
 */
uint8_t RCC_ENABLE_CLOCK_LSE32K (void){
   uint32_t timeOut=0;
   uint8_t error=0;
   
   RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);
   PWR->CR           |= (1UL<<8);
   
   RCC->CSR       |=1UL<<19;      // Reset BCDR
   RCC->CSR        &=~(1UL<<19);
   
   RCC->CSR        |=1UL<<8;      // LSEON=1
   
   while((RCC->CSR & (1UL<<9)) == 0)     {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	
	
   RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);
   
   return error;
}

/*
 * Set LS clock source to internal oscillator
 */
uint8_t RCC_ENABLE_CLOCK_LSI (void){
   uint32_t timeOut=0;
   uint8_t error=0;
   
   RCC_ENABLE_APB1_PERIPHERAL(PWR_APB1);
   PWR->CR           |= (1UL<<8);
   
   RCC->CSR       |=1UL<<19;	   // Reset BCDR
   RCC->CSR        &=~(1UL<<19);
   RCC->CSR        |=1UL;	   // LSEIN=1
   
   while((RCC->CSR & (1UL<<1)) == 0) {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	
   
   RCC_DISABLE_APB1_PERIPHERAL(PWR_APB1);
   
   return error;
}

uint8_t RCC_ENABLE_CLOCK_HSI16_4 (void){
   uint32_t timeOut=0;
   uint8_t error=0;
   
   system_range(3); 
   
   RCC_DISABLE_CLOCK_MSI();
   
   /* Set HSION16 bit */
   RCC->CR |= 0x01;
   RCC->CR |= (0x01<<3);  //Divided by 4
   
   
   /* Wait till HSI16 is ready */
   while((RCC->CR & 0x00000004)==0) {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	
   
   return error;
}


/*
 * Set main clock source to internal oscillator, 4MHz
 */
uint8_t RCC_CLOCK_MSI_4 (void){
   uint32_t timeOut=0;
   uint8_t error=0;
   
   system_range(3); 
   RCC_DISABLE_CLOCK_HSI ();
   
   RCC->ICSCR &= ~(uint32_t)0x0000E000;
   RCC->ICSCR |=  (uint32_t)0x0000C000;
   
   while((RCC->CR & (1UL<<9))==0) {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	
   SystemCoreClockUpdate();
   
   return error;
}

/*
 * Set main clock source to internal oscillator, 2MHz
 */
uint8_t RCC_CLOCK_MSI_2 (void){
   
   uint32_t timeOut=0;
   uint8_t error=0;
   
   system_range(3); 
   RCC_DISABLE_CLOCK_HSI ();
   
   RCC->ICSCR &= ~(uint32_t)0x0000E000;
   RCC->ICSCR |=  (uint32_t)0x0000A000;
   
   while((RCC->CR & (1UL<<9))==0) {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	
   SystemCoreClockUpdate();
   
   return error;
}

/*
 * Set main clock source to external oscillator, 25MHz
 */
uint8_t RCC_CLOCK_HSE_25 (void){
   uint32_t timeOut=0;
   uint8_t error=0;
   
   system_range(1); //Mandatory system range 1
   RCC_DISABLE_CLOCK_MSI();
   RCC_DISABLE_CLOCK_HSI();
   
   RCC->CR &=~(1UL<<18); //Disable HSE BYP
   RCC->CR &=~(1UL<<24); //Disable PLL
   RCC->CR |= 1UL<<16;   //Enable  HSE
   
   while((RCC->CR & (1UL<<17))==0) {
      if ((timeOut)++==0xFFFFF){error=1; break;}
   }	

   SystemCoreClockUpdate();
   
   return error;
}

void RCC_DISABLE_CLOCK_HSI (void){
   
   /* Reset HSION16 bit */
   RCC->CR &= ~(0x01);
}

// JCh: I think its useless...
// This clock source can't be stopped while its been using as System Clock
void RCC_DISABLE_CLOCK_MSI (void){
   /* Reset MSION bit */
   RCC->CR &= ~(0x100);
}

void RCC_DISABLE_CLOCK_HSE (void) {
   /* Reset MSION bit */
   RCC->CR &= ~(1<<16);
   // Select MSI as clock source
   RCC->CFGR &= ~(0x03);
}

// Unified function for clock change
void RCC_SET_CLK (uint8_t clk_idx, uint8_t set_source) {
   uint32_t timeOut=0;
   uint8_t error=0;
#if (DEBUG_PRINT>=2)
   uint32_t aux;
   
   aux=RCC->CR;
#endif
   
   switch(clk_idx) {
    case CLK_SRC_MSI: // MSI
      change_voltage(3);
      
      RCC->CR |= 1UL<<16;   //Enable  HSE
      while((RCC->CR & (1UL<<17))==0) {
	 if ((timeOut)++==0xFFFFF){error=1; break;}
      }	
      break;
    case CLK_SRC_HSI: // HSI
      change_voltage(2);
      
      RCC->CR |= 0x01;   //Enable  HSI
      while((RCC->CR & (1UL<<2))==0) {
	 if ((timeOut)++==0xFFFFF){error=1; break;}
      }	
      
      break;
    case CLK_SRC_HSE: // HSE
      change_voltage(1);
      RCC->CR &=~(1UL<<18); //Disable HSE BYP
      RCC->CR &=~(1UL<<24); //Disable PLL
      RCC->CR |= 1UL<<16;   //Enable  HSE
      while((RCC->CR & (1UL<<17))==0) {
	 if ((timeOut)++==0xFFFFF){error=1; break;}
      }	
      break;
   }
   
#if (DEBUG_PRINT>=2)
   dbg_print("RCC_SET_CLK: idx="); print8(&clk_idx,1); dbg_print(",CR="); print32(&aux,1);
   if(error!=0)
     dbg_print(" failed");
   aux=RCC->CR;
   dbg_print("--> CR="); print32(&aux,1); dbg_print(",");
#endif

   if(set_source) {
      switch(clk_idx) {
       case 0: // MSI
	 RCC->CFGR &= ~(0x03);
	 break;
       case 1: // HSI
	 RCC->CFGR &= ~(0x03);
	 RCC->CFGR |=   0x01;
	 break;
       case 2: // HSE
	 // Select HSE as clock source
	 // Dangerous
	 RCC->CFGR &= ~(0x03);
	 RCC->CFGR |=   0x02;
	 break;
      }
      SystemCoreClockUpdate();
      
   }

   
#if (DEBUG_PRINT>=2)
   aux=RCC->CFGR; 
   dbg_print("CFGR="); print32(&aux,1);  dbg_print("\n\r");
#endif
   
}


