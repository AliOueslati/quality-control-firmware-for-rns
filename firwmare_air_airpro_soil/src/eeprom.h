#ifndef EEPROM_H
#define EEPROM_H

#include "stm32l0xx.h"


void EEPROM_READ(uint16_t Addr,uint8_t *, uint16_t);
uint8_t EEPROM_WRITE(uint16_t Addr,uint8_t *,uint16_t); 


#endif
