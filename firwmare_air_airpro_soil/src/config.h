#ifndef CONFIG_H
#define CONFIG_H

#include "stm32l0xx.h"

//#define DEBUG              // Uncomment for debug purposes
                             // Micro does not sleep, watch out power!!
#define DEBUG_PRINT	2  // dbg_print

// Only one must be uncommented
//#define CENTRAL_NODE
#define REMOTE_NODE
    // Tests with/without Weight (hx711)
    //needs to be commented for CO2 sensors
    //#define HAS_HX711

//#include "board_chefi_v2k.h"
//#include "board_chefi_v06.h"
#include "board_chefi_beta00.h"

#ifdef REMOTE_NODE
// Remote only config
// ********************************

    ///////////////////////
    //Blink configuration//
    ///////////////////////
    #define TIME_BLINKING        60  //secs

    //////////////////////////
    //Sampling configuration//
    //////////////////////////
    #define TSAMPLE              300   //Sampling Time (s)
    #define MAX_TSAMPLE          600 //secs
    #define MIN_TSAMPLE          5   //secs

    //////////////
    //Board type//
    //////////////
    //  Uncomment the desired board
		 #define BOARD_WATERMELON
    //#define BOARD_SOIL              
    //#define BOARD_AIR
    //#define BOARD_AIR_CO2           // Not implemented...
    //#define BOARD_WATER_LEV
    //#define BOARD_DEBUG
    //#define BOARD_COMBI						//Test of CO2 plus Soil

    ////////////////////////
    //Sensor configuration//
    ////////////////////////
    #define SENSOR_HUMIDITY_SHT2X           //Comment Sensors not placed,
    #define SENSOR_PAR_TSL2561              //
#ifdef BOARD_SOIL
//    #define SENSOR_SOIL_HUMIDITY
    #define SENSOR_MANDARIN                 //new Soil Sensor with EC by default
//Choose if SoilSensor measures Temp or EC  -  only define one
//TODO: Calibrate the read value before sending to DB in sensor.
    //#define MANDARIN_TEMP
    #define MANDARIN_EC
#endif

#ifdef BOARD_WATERMELON
 #define SENSOR_WATERMELON
 #define WATERMELON_EC
 #define WATERMELON_TEMP
#endif
#ifdef BOARD_AIR_CO2
	#define SENSOR_CO2_COZIR
#endif

#ifdef BOARD_COMBI
//    #define SENSOR_SOIL_HUMIDITY
    #define SENSOR_MANDARIN                 //new Soil Sensor with EC by default
//Choose if SoilSensor measures Temp or EC  -  only define one
//TODO: Calibrate the read value before sending to DB in sensor.
    //#define MANDARIN_TEMP
    #define MANDARIN_EC
	#define SENSOR_CO2_COZIR
#endif

    #define TSL2561_ADDDRESS     0x39 // 00111001
    #define AM2321_ADDDRESS      0x5C // 01011100
    #define MLX90614_ADDDRESS    0x5A // 01011010
    #define SSD1306_ADDRESS      0x3C // 00111100

    // Chefi Beta algotihm to use
    #define SOIL_ALG_ID  3
    
    // Defined messages
    //#define MSG_MEASUREMENT_AIR_V1     0x0001 
    //#define MSG_MEASUREMENT_SOIL_V1    0x0002
    //#define MSG_CALIBRATION_V1         0x0003
    //#define MSG_MEASUREMENT_DEBUG_V1   0x0042
    #define MSG_MEASUREMENT_AIR        11 
    #define MSG_MEASUREMENT_SOIL       12
    #define MSG_MEASUREMENT_AIR_CO2    13
    #define MSG_MEASUREMENT_WATER_LEV  14
    #define MSG_MEASUREMENT_COMBI      15
		#define MSG_MEASUREMENT_WATERMELON 16
    #define MSG_MEASUREMENT_DEBUG      67

    // Dependent
    #ifdef BOARD_SOIL
        #define MSG_TYPE      MSG_MEASUREMENT_SOIL
    #elif defined BOARD_AIR
        #define MSG_TYPE      MSG_MEASUREMENT_AIR
    #elif defined BOARD_AIR_CO2
        #define MSG_TYPE	  MSG_MEASUREMENT_AIR_CO2
    #elif defined BOARD_WATER_LEV
        #define MSG_TYPE      MSG_MEASUREMENT_WATER_LEV
		#elif defined BOARD_COMBI
				#define MSG_TYPE			MSG_MEASUREMENT_COMBI
		#elif defined BOARD_WATERMELON
				#define MSG_TYPE			MSG_MEASUREMENT_SOIL
    #elif defined BOARD_DEBUG
        #define MSG_TYPE      MSG_MEASUREMENT_DEBUG
    #else
        #error "BOARD not defined"
    #endif


    /////////////////////////////////////////
    //Calibration configuration Y=A1*(x-A0)//
    /////////////////////////////////////////
    #define TIME_IN_CALIBRATION       120    	 // secs
    #define CALIB_PAR_A1              0.0605  	 // Light calibration value
		// SOIL_VPP_TO_WC_GAIN
    #define CALIB_SOIL_HUMIDITY_A1    -0.0973	 //-0.0714//-0.066 // Javi's calculation   
		// SOIL_VPP_TO_WC_OFFSET
    #define CALIB_SOIL_HUMIDITY_A0    389.4		 //3700   	 //offset
		// SCALE CALIBRATION VALUE        normally 8 for 4 Load Cells
    #define CALIB_SCALE               8
    //Mandarin Soil Sensor Calibration
    //Calibration for Wcc						implemented in CN at the moment
	#define CALIB_WCC_ADD						0
	#define CALIB_WCC_MULT						1
    //Calibration for Temp            not implemented yet
    #define CALIB_MANDARIN_TEMP       1
    //Calibration for EC              not implemented yet
    #define CALIB_MANDARIN_EC         1
    
    //////////////////////////////////
    // Battery related
    //////////////////////////////////
    #define VBAT_THRESHOLD_TX		2
/* VBAT LEVELS
		0:				Vdd < 1.9
		1:	1.9 < Vdd < 2.1
		2:	2.1 < Vdd < 2.3
		3:	2.3 < Vdd < 2.5
		4:	2.5 < Vdd < 2.7
		5:	2.7 < Vdd < 2.9
		6:	2.9 < Vdd < 3.1
		7:	3.1 < Vdd
*/

#elif defined(CENTRAL_NODE)
// Central only config
// ********************************

   //Network configuration
   #define MAX_NODES              36 //Up to 36

   //Radio configuration 
   #define TRANSMISSION_POWER     0
   //#define TRANSMIT_TIMEOUT       0xFFFF0
   //#define CENTRAL_NODE_ADDRESS   0x20
   //#define REMOTE_NODE_ADDRESS    0x21

#endif

// Common defines
// ********************************
///////////////////////
//Modem configuration//
///////////////////////

//change LORA_FREQ to adjust Frequency
#define LORA_FREQ              868

#define TRANSMIT_TIMEOUT       0xFFFF0
#define RECEIVE_TIMEOUT        0x6FFFF
#define CENTRAL_NODE_ADDRESS   0x20
#define REMOTE_NODE_ADDRESS    0x21
#define LORA_SPREADING_FACTOR  MC2_SF9
//#define LORA_SPREADING_FACTOR  MC2_SF12
#define LORA_CODING_RATE       MC1_CR_4_5
#define LORA_BW                MC1_BW_125
#define PAYLOAD_SIZE           64
#define LORA_CRC      
// LORA DBG
// #define LORA_DBG_CNT

//MSG CN2NR Max size
#define MAX_SIZE_CN2NR_COMMAND 16

#endif

