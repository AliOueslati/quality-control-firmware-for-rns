//==============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//==============================================================================
// Project   :  SHT2x Sample Code (V1.2)
// File      :  SHT2x.c
// Author    :  MST
// Controller:  NEC V850/SG3 (uPD70F3740)
// Compiler  :  IAR compiler for V850 (3.50A)
// Brief     :  Sensor layer. Functions for sensor access
//==============================================================================

//---------- Includes ----------------------------------------------------------
#include "SHT2x.h"
#include "stm32l0xx.h"
#include "i2c.h"
#include "delay.h"

#define I2C I2C1

//==============================================================================
uint8_t SHT2x_CheckCrc(uint8_t data[], uint8_t nbrOfBytes, uint8_t checksum)
//==============================================================================
{
  uint8_t crc = 0;
  uint8_t byteCtr;
  uint8_t bit;
  //calculates 8-Bit checksum with given polynomial
  for (byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr)
  {
    crc ^= (data[byteCtr]);
    for (bit = 8; bit > 0; --bit)
    {
      if (crc & 0x80)
        crc = (crc << 1) ^ SHT2X_POLYNOMIAL;
      else
        crc = (crc << 1);
    }
  }
  if (crc != checksum) return CHECKSUM_ERROR; // TODO: fix ^
  else return 0;
}

//===========================================================================
uint8_t SHT2x_ReadUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
  uint8_t checksum;   //variable for checksum byte
  uint8_t error=0;    //variable for error code

  I2C_SET_NBYTES(I2C, 1);
  I2C_SET_WRITE(I2C);
  error= I2C_START(I2C)
      || I2C_WRITE(I2C, USER_REG_R)
      || I2C_WAIT_TC(I2C)
      || I2C_STOP(I2C);

  if (error) return error;

  I2C_SET_NBYTES(I2C, 2);
  I2C_SET_READ(I2C);
  return I2C_START(I2C)
      || I2C_READ(I2C, pRegisterValue)
      || I2C_READ(I2C, &checksum)
      || SHT2x_CheckCrc(pRegisterValue, 1, checksum)
      || I2C_WAIT_TC(I2C)
      || I2C_STOP(I2C);
}

//===========================================================================
uint8_t SHT2x_WriteUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
  I2C_SET_NBYTES(I2C, 2);
  I2C_SET_WRITE(I2C);
  return I2C_START(I2C)
      || I2C_WRITE(I2C, USER_REG_W)
      || I2C_WRITE(I2C, *pRegisterValue)
      || I2C_WAIT_TC(I2C)
      || I2C_STOP(I2C);
}

//===========================================================================
uint8_t SHT2x_MeasureHM(etSHT2xMeasureType eSHT2xMeasureType, uint16_t *pMeasurand)
//===========================================================================
{
  uint8_t  checksum;   //checksum
  uint8_t  data[2];    //data array for checksum verification
  uint8_t  error=0;    //error variable


  //-- write I2C sensor address and command --
  I2C_SET_NBYTES(I2C, 1);
  I2C_SET_WRITE(I2C);
  I2C_START(I2C);
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2C_WRITE(I2C, TRIG_RH_MEASUREMENT_HM); break;
    case TEMP    : error |= I2C_WRITE(I2C, TRIG_T_MEASUREMENT_HM);  break;
    default      : error |= I2C_WRITE(I2C, TRIG_T_MEASUREMENT_HM);
  }
  error |= I2C_WAIT_TC(I2C);
  I2C_STOP(I2C);

  I2C_SET_NBYTES(I2C, 3);
  I2C_SET_READ(I2C);
  I2C_START(I2C);
	//-- wait until hold master is released (Figure 15 (datasheet)
	//measurement is done after I2C address + Read
  error |= I2C_READ(I2C, &data[0]);
  error |= I2C_READ(I2C, &data[1]);
  error |= I2C_READ(I2C, &checksum);
  *pMeasurand = data[0] << 8 | data[1];
  //-- verify checksum --
  //error |= SHT2x_CheckCrc (data,2,checksum);
  error |= I2C_WAIT_TC(I2C);
  I2C_STOP(I2C);
  return error;
}

//===========================================================================
uint8_t SHT2x_MeasurePoll(etSHT2xMeasureType eSHT2xMeasureType, uint16_t *pMeasurand)
//===========================================================================
{
  uint8_t  checksum;   //checksum
  uint8_t  data[2];    //data array for checksum verification
  uint8_t  error=0;    //error variable
  uint16_t i=0;        //counting variable

  //-- write I2C sensor address and command --
  I2C_SET_NBYTES(I2C, 1);
  I2C_SET_WRITE(I2C);
  I2C_START(I2C);
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2C_WRITE(I2C, TRIG_RH_MEASUREMENT_POLL); break;
    case TEMP    : error |= I2C_WRITE(I2C, TRIG_T_MEASUREMENT_POLL);  break;
    default: error = 16;
  }
  error |= I2C_WAIT_TC(I2C);
  I2C_STOP(I2C);

  //-- poll every 10ms for measurement ready. Timeout after 20 retries (200ms)--
  I2C_SET_NBYTES(I2C, 3);
  I2C_SET_READ(I2C);
  do
  {
    delay_us(10000);  //delay 10ms
    I2C_START(I2C);
    if(i++ >= 20) break;
  } while(I2C_READ(I2C, &data[0]) == ACK_ERROR);
  if (i>=20) error |= TIME_OUT_ERROR;

  //-- read two data bytes and one checksum byte --
  //I2C_SET_NBYTES(I2C, 3);
  //I2C_SET_READ(I2C);
  //I2C_START(I2C);
  //error |= I2C_READ(I2C, &data[0]);
  error |= I2C_READ(I2C, &data[1]);
  error |= I2C_READ(I2C, &checksum);
  *pMeasurand = data[0] << 8 | data[1];
  //-- verify checksum --
  //error |= SHT2x_CheckCrc (data,2,checksum);
  error |= I2C_WAIT_TC(I2C);
  I2C_STOP(I2C);

  return error;
}

//===========================================================================
uint8_t SHT2x_SoftReset(void)
//===========================================================================
{
  uint8_t  error=0;           //error variable

  I2C_SET_NBYTES(I2C, 1);
  I2C_SET_WRITE(I2C);
  I2C_START(I2C);
  error |= I2C_WRITE(I2C, SOFT_RESET);
  error |= I2C_WAIT_TC(I2C);
  I2C_STOP(I2C);

  delay_us(15000); // wait till sensor has restarted

  return error;
}

//==============================================================================
uint16_t SHT2x_CalcRH(uint16_t u16sRH)
//==============================================================================
{
  uint16_t humidityRH;              // variable for result

  u16sRH &= ~0x0003;          // clear bits [1..0] (status bits)

	//-- calculate relative humidity [%RH] --
  // RH = -6 + 125 * SRH/2^16
  humidityRH = -60 + (uint16_t)(((uint32_t)u16sRH * 1250) >> 16);
  return humidityRH;
}

//==============================================================================
int16_t SHT2x_CalcTemperatureC(uint16_t u16sT)
//==============================================================================
{
  int16_t temperatureC;            // variable for result

  u16sT &= ~0x0003;           // clear bits [1..0] (status bits)

  //-- calculate temperature [deg C] --
  // T = -46.85 + 175.72 * ST/2^16
  temperatureC= -4685 + (uint16_t)(((uint32_t)u16sT * 17572) >> 16);
  return temperatureC;
}

//==============================================================================
uint8_t SHT2x_GetSerialNumber(uint8_t u8SerialNumber[])
//==============================================================================
{
  uint8_t  error=0;                          //error variable
/*
  //Read from memory location 1
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFA);         //Command for readout on-chip memory
  error |= I2c_WriteByte (0x0F);         //on-chip memory address
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address
  u8SerialNumber[5] = I2c_ReadByte(ACK); //Read SNB_3
  I2c_ReadByte(ACK);                     //Read CRC SNB_3 (CRC is not analyzed)
  u8SerialNumber[4] = I2c_ReadByte(ACK); //Read SNB_2
  I2c_ReadByte(ACK);                     //Read CRC SNB_2 (CRC is not analyzed)
  u8SerialNumber[3] = I2c_ReadByte(ACK); //Read SNB_1
  I2c_ReadByte(ACK);                     //Read CRC SNB_1 (CRC is not analyzed)
  u8SerialNumber[2] = I2c_ReadByte(ACK); //Read SNB_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNB_0 (CRC is not analyzed)
  I2c_StopCondition();

  //Read from memory location 2
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFC);         //Command for readout on-chip memory
  error |= I2c_WriteByte (0xC9);         //on-chip memory address
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address
  u8SerialNumber[1] = I2c_ReadByte(ACK); //Read SNC_1
  u8SerialNumber[0] = I2c_ReadByte(ACK); //Read SNC_0
  I2c_ReadByte(ACK);                     //Read CRC SNC0/1 (CRC is not analyzed)
  u8SerialNumber[7] = I2c_ReadByte(ACK); //Read SNA_1
  u8SerialNumber[6] = I2c_ReadByte(ACK); //Read SNA_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNA0/1 (CRC is not analyzed)
  I2c_StopCondition();
*/
  return error;
}

uint8_t SHT2x_read(sht2x_result* result)
{
  uint8_t error = 0;
  uint8_t userRegister;        //variable for user register
  uint16_t sRH;                    //variable for raw humidity ticks
  uint16_t sT;                     //variable for raw temperature ticks

  // Configure I2C //
  I2C_ENABLE(I2C, STD_MODE_100K,0);

  I2C_SET_SADD_7BITS(I2C, SHT2X_ADDRESS);   // Address setting

  // reset error status
  // --- Reset sensor by command ---
  //error |= SHT2x_SoftReset();

  // --- Read the sensors serial number (64bit) ---
  //error |= SHT2x_GetSerialNumber(SerialNumber_SHT2x);

  // --- Set Resolution e.g. RH 10bit, Temp 13bit ---
  error = SHT2x_ReadUserRegister(&userRegister);  //get actual user reg
  if (error) {
    result->TEMPERATURE = 32000 + error;
    result->HUMIDITY    = 32000;
    I2C_DISABLE(I2C);
    return error;
  }
  //userRegister = (userRegister & ~SHT2x_RES_MASK) | SHT2x_RES_10_13BIT;
  userRegister = (userRegister & ~SHT2x_RES_MASK) | SHT2x_RES_8_12BIT;
  error |= SHT2x_WriteUserRegister(&userRegister); //write changed user reg

  // --- measure humidity with "Hold Master Mode (HM)"  ---
  error = SHT2x_MeasureHM(HUMIDITY, &sRH);
  if (error) {
    result->HUMIDITY    = 32000 + (error<<1);
  } else {
    result->HUMIDITY    = SHT2x_CalcRH(sRH);
  }
  // --- measure temperature with "Polling Mode" (no hold master) ---
  error = SHT2x_MeasurePoll(TEMP, &sT);
  if (error) {
    result->TEMPERATURE = 32000 + (error<<1);
  } else {
    result->TEMPERATURE = SHT2x_CalcTemperatureC(sT);
  }

  I2C_DISABLE(I2C);

  return error;
}
