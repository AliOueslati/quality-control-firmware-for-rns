#ifndef HX711_H
#define HX711_H

#include "stm32l0xx.h"

void     hx711_init       (void);
void     hx711_deinit     (void);
uint32_t hx711_get_raw    (void);
uint16_t hx711_get_value  (void);
void     hx711_do_calibration  (uint32_t Mode);
int16_t  hx711_get_weight (void);
int16_t  hx711_procedure(void);
//void     hx711_dummy_test(void);

#endif
