#include "stm32l0xx.h"
#include "timer.h"
#include "rcc.h"
#include "rtc.h"
#include "calendar.h"
#include "hexutil.h"
#include "led.h"

volatile uint8_t ALARM_0_PENDING=0;
volatile uint8_t ALARM_1_PENDING=0;

uint8_t calendar_init(){
   uint8_t error;
   
   error=START_RTC_INIT(RTC);
   SET_RTC_PREDIVS(RTC,300);	 //Only if LSI is the clock source
   error=STOP_RTC_INIT(RTC);
   
   return error;
}

uint8_t calendar_set(rtc_calendar *date){
   uint32_t dateReg[2];
   uint8_t error;
   
   dateReg[0]  = (uint32_t) (date->SECOND);
   dateReg[0] |= ((uint32_t)(date->MINUTE))<<8;
   dateReg[0] |= ((uint32_t)(date->HOUR))<<16;
   dateReg[1]  = (uint32_t) (date->DAY);
   dateReg[1] |= ((uint32_t)(date->MONTH))<<8;
   dateReg[1] |= ((uint32_t)(date->YEAR))<<16;
   
   error=START_RTC_INIT(RTC);
   SET_RTC_CALENDAR(RTC, dateReg);							 // Set Date
   error=STOP_RTC_INIT(RTC);
   
   return error;
}


uint8_t calendar_get(rtc_calendar *date){
   uint32_t dateReg[3];
   uint8_t error;
   error=GET_RTC_CALENDAR(RTC, dateReg);							 // Set Date
   
   date->YEAR   =(uint8_t)(dateReg[1]>>16);
   date->MONTH  =(uint8_t)(dateReg[1]>>8);
   date->DAY    =(uint8_t) dateReg[1];
   date->HOUR   =(uint8_t)(dateReg[0]>>16);
   date->MINUTE =(uint8_t)(dateReg[0]>>8);
   date->SECOND =(uint8_t) dateReg[0];
   
   return error;
}


void calendar_alarm(uint16_t seconds, uint8_t Nalarm){
   uint8_t minutes, hours;
   uint32_t dateReg[3];
   uint32_t alarm;
   uint32_t subsec;
   
   hours=seconds/3600;
   seconds=(seconds%3600);
   minutes= seconds/60;
   seconds=(seconds%60);
   
   GET_RTC_CALENDAR(RTC, dateReg);
   subsec= dateReg[2];
   
   seconds+=bcd2hex((uint8_t)(dateReg[0]&0x7F));
   
   if (seconds>59) {
      seconds=seconds%60;
      minutes+=1;
   }
   
   minutes+=bcd2hex((uint8_t)((dateReg[0]>>8)&0x7F));
   
   if (minutes>59) {
      minutes=minutes%60;
      hours+=1;
   }
   
   hours+=bcd2hex((uint8_t)((dateReg[0]>>16)&0x3F));
   hours=hours%24;
   
   hours=hex2bcd(hours);
   minutes=hex2bcd(minutes);
   seconds=hex2bcd(seconds);
   
   alarm=(((uint32_t)hours)<<16) | (((uint16_t)minutes)<<8) | seconds;
   
   if (Nalarm) SET_RTC_ALARM_B(RTC,alarm,subsec);
   else SET_RTC_ALARM_A(RTC,alarm,subsec);
}

void RTC_IRQHandler(void){
   
   if (RTC->ISR & (1UL<<8)) {
      RTC->ISR &= ~(1<<8);
      ALARM_0_PENDING=1;
   }
   
   if (RTC->ISR & (1UL<<9)) {
      RTC->ISR &= ~(1<<9);
      ALARM_1_PENDING=1;
   }
   
   RTC->ISR &= ~(1<<8);
   EXTI->PR|=(1UL<<17);
}
