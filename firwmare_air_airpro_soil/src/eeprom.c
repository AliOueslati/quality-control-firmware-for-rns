#include "stm32l0xx.h"
#include "rcc.h"

#define EEPROM_BASE_ADDR    0x08080000    
#define EEPROM_BYTE_SIZE    0x07FF  
#define PEKEY1              0x89ABCDEF      //FLASH_PEKEYR  
#define PEKEY2              0x02030405      //FLASH_PEKEYR  
#define EN_INT                
#define DIS_INT               

/*------------------------------------------------------------ 
Func: EEPROM 
Note: 
 -------------------------------------------------------------*/  
void EEPROM_READ(uint16_t Addr,uint8_t *Buffer, uint16_t Length)  
{  
	uint8_t *wAddr;  
  wAddr=(uint8_t *)(EEPROM_BASE_ADDR)+Addr;  
  while(Length--)
		{  
  *Buffer++=*wAddr++;  
		}
	}		
 
 /*------------------------------------------------------------ 
 Func: EEPROM 
     Note: 
-------------------------------------------------------------*/  
uint8_t EEPROM_WRITE(uint16_t Addr,uint8_t *Buffer,uint16_t Length)  
{  
	uint8_t error=0;
  uint8_t *wAddr;  
  wAddr=(uint8_t *)(EEPROM_BASE_ADDR)+Addr;  
  __disable_irq();
	
  FLASH->PEKEYR=PEKEY1;                //unlock  
  FLASH->PEKEYR=PEKEY2;  
  while(FLASH->PECR&FLASH_PECR_PELOCK);  
 

	while(Length--)
		{  
     *wAddr++=*Buffer++;  
     while(FLASH->SR&FLASH_SR_BSY);  
    }  
  
		FLASH->PECR|=FLASH_PECR_PELOCK;  
	  __enable_irq(); 
		
		return error;
}  

