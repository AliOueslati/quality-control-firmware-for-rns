#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "stm32l0xx.h"
#include "config.h"


typedef struct {
	uint32_t   calibrated;
  float     calib_par_a1;
  float     calib_soil_humidity_a0;
  float     calib_soil_humidity_a1;
} calibration_values;


typedef struct {
	uint16_t            MSGtype;   // MSGtype
  //int16_t             Temp;      // 100 * Temperature [deg Celcius] (-40 - 120) (for SHT2x)
  uint16_t            PAR;       //   1 * Photo Active Radiation [umol/m2/s] (0 - 2000) (at tropical midday)
  //uint16_t            Humid;     //  10 * Relative Humidity [%] (0 - 100)
  uint16_t            SoilHum;   //   ? * Soil Humidity [?]
  //uint16_t            SoilEC;    //   ? * Soil Electrical Conduction? [?]
	//uint16_t            SoilTemp;  //   ? * Soil Temperature
	uint16_t            dummy1;   //   ? * Soil Humidity [?]
} calibration_msg;


void load_calibration_defaults(calibration_values *);
void read_calibration(calibration_values *);
uint8_t write_calibration(calibration_values *);

#endif
