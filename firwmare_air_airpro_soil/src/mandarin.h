#ifndef MANDARIN_H
#define MANDARIN_H

#include "stm32l0xx.h"
#include "config.h"

typedef struct{

   uint16_t CH1;                // Wc analog channel
   uint16_t CH2;                // EC/TEMP analog channel

} mandarin_analog_result;

typedef struct{

   uint16_t mandarin_wc;        //WC from Mandarin Soil Sensor
   uint16_t mandarin_ec_temp;   //EC/Temp from Mandarin Soil Sensor

} mandarin_result;

void MandarinInit(void);
void MandarinDeinit(void);
void GetMandarinWc(mandarin_analog_result* Result);
void GetMandarinEcTemp(mandarin_analog_result* Result);
void MandarinRead(mandarin_result* Result);

#endif
