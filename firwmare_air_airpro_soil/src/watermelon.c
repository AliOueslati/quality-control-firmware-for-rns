//Date: 2017-01-25
//Author: Thimo Findeisen
//
//Comment:
//The Soil Sensor used has one Channel reserved for Water Content
//The other Channel can be either Temp or EC, depending on which Hardware version has been purchased

#include "watermelon.h"

#include "config.h"
#include "rcc.h"
#include "gpio.h"
#include "adc.h"

#include "print.h"
#include "delay.h"

void WatermelonInit(){
#ifdef SENSOR_WATERMELON

//Init Vpp for Soil Sensor
   SET_GPIO_AS_OUTPUT(   SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   GPIO_CLEAR(           SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);

   RCC_ENABLE_APB2_PERIPHERAL(ADC_APB2);
  
   ADC_CLK_SOURCE(ADC1, CK_PCLK_2);

   ADC_CALIBRATION(ADC1);

   ADC_SET_OV(ADC1,OV_256);
   ADC_SET_AUTOFF(ADC1);
   ADC_SET_SINGLE_CONVERSION(ADC1);

//SET GPIOs to read Wc and EC/TEMP (not completely implemented)
   SET_GPIO_AS_INPUT(SOIL_ADC1_PORT, SOIL_ADC1_PIN);    //WC
   SET_GPIO_AS_INPUT(SOIL_ADC2_PORT, SOIL_ADC2_PIN);    //TEMP 
	 SET_GPIO_AS_INPUT(SOIL_NTC_ADC0_PORT, SOIL_NTC_ADC0_PIN);//EC
  
   //ADC_SET_CHANNELS(ADC1, CH_0|CH_1);
	 ADC_SET_CHANNELS(ADC1, CH_0|CH_1|CH_8);

#endif
}

void WatermelonDeinit(){
#ifdef SENSOR_WATERMELON

//disable to avoid Power Wast
   GPIO_SET(SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);

//disable ADC
   RCC_DISABLE_APB2_PERIPHERAL(ADC_APB2);

#endif
}

void GetWatermelonWc(watermelon_analog_result* Result){
#ifdef SENSOR_WATERMELON

//get Analog value for Wc
   ADC_READ_DATA(ADC1, &(Result->CH1));

#endif
}

void GetWatermelonTemp(watermelon_analog_result* Result){
#ifdef WATERMELON_TEMP

//get Analog value for Temp
   ADC_READ_DATA(ADC1, &(Result->CH2));

#endif
}

void GetWatermelonEc(watermelon_analog_result* Result){
#ifdef WATERMELON_EC

//get Analog value for Ec
   ADC_READ_DATA(ADC1, &(Result->CH8));

#endif
}

void WatermelonRead(watermelon_result* Result){
#ifdef SENSOR_WATERMELON
  watermelon_analog_result   measuredWatermelon;
  
  WatermelonInit();
  delay_ms(500);

  ADC_START(ADC1);

//get ADC values
	
	GetWatermelonWc(&measuredWatermelon);
	GetWatermelonTemp(&measuredWatermelon);
	GetWatermelonEc(&measuredWatermelon);

//get the values read by Soil Sensor
	 
   Result->watermelon_wc = measuredWatermelon.CH1;
   Result->watermelon_temp = measuredWatermelon.CH2;
	 Result->watermelon_ec = measuredWatermelon.CH8;


  WatermelonDeinit();
#endif
}
