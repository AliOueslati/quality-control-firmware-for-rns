#include "stm32l0xx.h"
#include "spi.h"
#include "led.h"
#include "config.h"
#include "lora.h"
#include "gpio.h"
#include "rcc.h"
#include "delay.h"
#include "print.h"

#define TIMEOUT_ERROR 0xFFFF

#define SPI          SPI1

#ifdef LORA_DBG_CNT
lora_dbg_cnt loraDbg={0,0,0,0,0,0,0,0,0,0};
#endif

///////////////////
// SPI functions //
///////////////////
void lora_reset(void){
   GPIO_CLEAR(LORA_RESET_PORT, LORA_RESET_PIN);
   delay_ms(400);
   GPIO_SET(LORA_RESET_PORT, LORA_RESET_PIN);
}

void lora_spi_csh(void){
   GPIO_SET(LORA_SPI_CS_PORT, LORA_SPI_CS_PIN);
}

void lora_spi_csl(void){
   GPIO_CLEAR(LORA_SPI_CS_PORT, LORA_SPI_CS_PIN);
}

void writeReg (uint8_t addr, uint8_t data ) {
   uint8_t dummy;
   
   lora_spi_csl();
   SPI_XFER_8(SPI, addr | 0x80, &dummy);
   SPI_XFER_8(SPI, data, &dummy);
   lora_spi_csh();
}

uint8_t readReg (uint8_t addr) {
    uint8_t val,dummy;
    
    lora_spi_csl();
    SPI_XFER_8(SPI, addr & 0x7F, &dummy);
    SPI_XFER_8(SPI, 0x00, &val);
    
    lora_spi_csh();
    return val;
 }

 void writeBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
    uint8_t dummy;
    
    lora_spi_csl();
    SPI_XFER_8(SPI, addr | 0x80, &dummy);
    for (uint8_t i=0; i<len; i++) {
       SPI_XFER_8(SPI, buf[i], &dummy);
    }
    lora_spi_csh();
 }

void readBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
   uint8_t dummy;
   
   lora_spi_csl();
   SPI_XFER_8(SPI, addr & 0x7F, &dummy);
   for (uint8_t i=0; i<len; i++) {
      SPI_XFER_8(SPI, 0x00, &buf[i]);
   }
   lora_spi_csh();
}

////////////////////
////////////////////
////////////////////


void opmode (uint8_t mode) {
   writeReg(RegOpMode, (readReg(RegOpMode) & ~OPMODE_MASK) | mode);
}

void opmodeLora() {
   writeReg(RegOpMode, OPMODE_LORA);
}


// configure LoRa modem (cfg1, cfg2)
// 
void configLoraModem (LORAConfig *Config) {
   
   uint8_t mc1 = 0, mc2 = 0, mc3 = 0;
   // Frequency 
   mc1 |= Config->BW;
   // Coding Rate    
   mc1 |= Config->CR;
   // CRC mode ON
   // Explicit Header ON
   //mc1 |= 1;
   // set ModemConfig1
   writeReg(LORARegModemConfig1, mc1);
   
   
   // Spreading Factor
   mc2 |= Config->SF; 
   // CRC mode ON    
   mc2 |= MC2_RX_PAYLOAD_CRCON;
   // set ModemConfig2      
   writeReg(LORARegModemConfig2, mc2);	 
   
   // AGC auto mode     
   mc3 = MC3_AGCAUTO;
   // LOW Data Rate Optimizer (depending on the MC1 and MC2 configuration)
   if (((mc2&0xB0) || (mc2&0xC0)) && (mc1&0x70)) { //(SF11 OR SF12) AND (BW125)
      mc3 |= MC3_LOW_DATA_RATE_OPTIMIZE;
   }
   writeReg(LORARegModemConfig3, mc3);
}

void configChannel (LORAConfig *Config) {
   // set frequency: FQ = (FRF * 32 Mhz) / (2 ^ 19)
   uint32_t frf = (Config->FREQ << 19) / 32;
   writeReg(RegFrfMsb, (uint8_t)(frf>>16));
   writeReg(RegFrfMid, (uint8_t)(frf>> 8));
   writeReg(RegFrfLsb, (uint8_t)(frf>> 0));
}

void configPower (LORAConfig *Config) {
   //17 dBm
   writeReg(RegPaConfig, (uint8_t)(0x80|(Config->TX_POWER-2)));
   //Enable 20 dBm on PA_BOOST
   writeReg(RegPaDac, readReg(RegPaDac)|0x7);
}

void radio_start_tx (LORAData *Payload, uint8_t ui8_Address) {
   
   uint8_t txBuffer[PAYLOAD_SIZE];
#ifdef REMOTE_NODE
   led_on(LED_GREEN);
#elif defined CENTRAL_NODE
   led_on(LED_TX);
#endif
   
   Payload->ADDRESS=ui8_Address;
   
   // enter standby mode (required for FIFO loading))
   opmode(OPMODE_STANDBY);	 
   // create buffer
   txBuffer[0]=Payload->ADDRESS;
   txBuffer[1]=Payload->LENGTH;
   for (uint8_t k = 0; k<Payload->LENGTH; k++){
      txBuffer[k+2]=Payload->DATA[k];
   }
   // CRC
#ifdef LORA_CRC   
   txBuffer[(Payload->LENGTH)+2] = ui8_CRCGen(txBuffer,(Payload->LENGTH)+2);
   // download buffer to the radio FIFO
   writeReg(LORARegPayloadLength, Payload->LENGTH+3);
   writeBuf(RegFifo, txBuffer, Payload->LENGTH+3);	//ADDR + LEN + DATA + CRC
#else
   writeReg(LORARegPayloadLength, Payload->LENGTH+2);
   writeBuf(RegFifo, txBuffer, Payload->LENGTH+2);	//ADDR + LEN + DATA 
#endif
   // now we actually start the transmission
   opmode(OPMODE_TX);
#ifdef REMOTE_NODE
   led_off(LED_GREEN);
#elif defined CENTRAL_NODE
   led_off(LED_TX);
#endif
   // Update Stats
#ifdef LORA_DBG_CNT
   loraDbg.LoraTx++;
#endif
}

void radio_receive (LORAData *Payload) {
   uint8_t rxBytes,crcOK;
   uint8_t rxBuffer[PAYLOAD_SIZE];
   
#ifdef LORA_DBG_CNT
   loraDbg.LoraRx++;
#endif
   
   // set FIFO read address pointer
   writeReg(LORARegFifoAddrPtr, readReg(LORARegFifoRxCurrentAddr)); 
   // now read the FIFO
   rxBytes=readReg(LORARegRxNbBytes); 
   //read CRC
   crcOK=readReg(LORARegIrqFlags); 
   writeReg(LORARegIrqFlags, crcOK); //Clear IRQs
   crcOK&=(1<<5);
   
#ifdef DEBUG_PRINT
   dbg_print("radio receive: rxBytes=");
   print_unsBCD(rxBytes,0);
#endif
   
   if(rxBytes<=PAYLOAD_SIZE && !crcOK) {
      readBuf(RegFifo, rxBuffer, rxBytes);
      
      Payload->ADDRESS= rxBuffer[0];
      Payload->LENGTH = rxBuffer[1]; 
      for (uint8_t k = 0; k<Payload->LENGTH; k++) {
	 Payload->DATA[k]=rxBuffer[k+2];
      }
      
#ifdef LORA_DBG_CNT
      //RxCrcOk++;
      loraDbg.FwCrcOk++;
#endif
      
      //CRC
      dbg_print(",loraCRC ok");
#ifdef LORA_CRC      
      if(rxBuffer[(Payload->LENGTH)+2] == ui8_CRCGen(rxBuffer,(Payload->LENGTH)+2)) {
#ifdef LORA_DBG_CNT
	 loraDbg.LoraCrcOk++;
#endif
#ifdef DEBUG_PRINT
	 dbg_print(",FwCRC ok");
#endif
	 
#endif	 
	 //Check destination addr
#ifdef REMOTE_NODE        
	 if(Payload->ADDRESS == REMOTE_NODE_ADDRESS) {
#elif defined CENTRAL_NODE
	 if(Payload->ADDRESS == CENTRAL_NODE_ADDRESS) {
#endif
	    // read rx quality parameters
		
#ifdef LORA_DBG_CNT
	    loraDbg.AdrOk++;
#endif
	    
	    Payload->SNR  = readReg(LORARegPktSnrValue); // SNR [dB] * 4
	    Payload->RSSI = readReg(LORARegPktRssiValue) - 125 + 64; // RSSI [dBm] (-196...+63)
	    Payload->VALID=1;
#ifdef DEBUG_PRINT
	    // Show a portion, starting Remote-Address
	    dbg_print(",valid ok [...");
	    print8( &rxBuffer[6],6);
	    dbg_print("...]");
	    
	    // Update Stats
#endif
	    
	 }
	 else {
	    Payload->VALID=0;
#ifdef DEBUG_PRINT
	    dbg_print(",valid KO");
#endif
#ifdef LORA_DBG_CNT
	    // Update Stats
	    loraDbg.AdrKo++;
#endif
	 }
#ifdef LORA_CRC	 
      }
# ifdef DEBUG_PRINT
      else {
	 Payload->VALID=0;
	 dbg_print(",loraCRC err");
#ifdef LORA_DBG_CNT
	 // Update Stats
	 loraDbg.LoraCrcKo++;
#endif	 
      }
# endif
#else
#endif      
   }
   else {
      Payload->VALID=0;
#ifdef DEBUG_PRINT
      dbg_print(",valid KO");
      
#ifdef LORA_DBG_CNT
      // Update Stats
      //KoRxCrC++;
      loraDbg.FwCrcKo++;
#endif
#endif
   }
      
#ifdef DEBUG_PRINT
      dbg_print("\n\r");
#endif
      
}
   
   
   
   // start LoRa receiver (time=LMIC.rxtime, timeout=LMIC.rxsyms, result=LMIC.frame[LMIC.dataLen])
   void radio_start_rx_cont () {
      opmode(OPMODE_RX); 
   }

void radio_start_sleep () { 
    opmode(OPMODE_SLEEP); 
}


void radio_start_rx_single () {
   
   //enable antenna switch for RX
   //hal_pin_rxtx(0);
   
   // now instruct the radio to receive
   //if (rxmode == RXMODE_SINGLE) { // single rx
   //    hal_waitUntil(LMIC.rxtime); // busy wait until exact rx time
   //    opmode(OPMODE_RX_SINGLE);
   //} else { // continous rx (scan or rssi)
   opmode(OPMODE_RX_SINGLE); 
   //}
}

void lora_init() {
   
   
   // Configure IRQ & CS port & pins
   GPIO_SET(LORA_SPI_CS_PORT, LORA_SPI_CS_PIN); 	               //Disable cc1101 by default
   SET_GPIO_AS_OUTPUT(LORA_SPI_CS_PORT, LORA_SPI_CS_PIN);
   SET_GPIO_SPEED_LOW(LORA_SPI_CS_PORT, LORA_SPI_CS_PIN);
   
   
   SET_GPIO_AS_INPUT(LORA_DIO0_PORT, LORA_DIO0_PIN);
   
   // Enable SPI and configuration
   RCC_ENABLE_APB2_PERIPHERAL(SPI1_APB2);
   SPI_SET_BAUDRATE_PRESCALER(SPI, PCLK_2);
   SPI_SET_CLOCK_POLARITY_0(SPI);
   SPI_SET_CLOCK_PHASE_0(SPI);
   SPI_SET_UNIDIRECTIONAL_MODE(SPI);
   SPI_SEND_MSB_FIRST(SPI);
   SPI_SET_FULL_DUPLEX_MODE(SPI);
   SPI_ENABLE_SOFTWARE_SLAVE_MANAGEMENT(SPI);
   SPI_SET_NSS_HIGH(SPI);
   SPI_SET_MASTER_MODE(SPI);
   SPI_SET_DFF_8BITS(SPI);
   SPI_ENABLE_SS_OUTPUT(SPI);
   SPI_ENABLE(SPI);
   
   SET_GPIO_AS_AF(SPI_SCK_PORT, SPI_SCK_PIN, AF0);  //SCK
   SET_GPIO_AS_AF(SPI_MISO_PORT, SPI_MISO_PIN, AF0); //MISO
   SET_GPIO_AS_AF(SPI_MOSI_PORT, SPI_MOSI_PIN, AF0); //MOSI
   
   RCC_ENABLE_APB2_PERIPHERAL(SYSCFG_APB2);
   
   SYSCFG->EXTICR[LORA_DIO0_PIN/4] = ( SYSCFG->EXTICR[LORA_DIO0_PIN/4]
				       & ~(0x0F << (LORA_DIO0_PIN & 0x03)*4) )
                                       |   0x01 << (LORA_DIO0_PIN & 0x03)*4;    //0x01 for PORTB
   
   
   EXTI->IMR  |= (1UL<<LORA_DIO0_PIN);  //Unmask EXT_[LORA_IRQ_PIN]
   EXTI->RTSR |= (1UL<<LORA_DIO0_PIN); //Rising edge
   
   NVIC_SetPriority(EXTI4_15_IRQn,1);
   NVIC_EnableIRQ(EXTI4_15_IRQn);
}


void radio_init_tx (LORAConfig *Config) {
   
   // manually reset radio
   lora_reset();
   //
   
   opmode(OPMODE_SLEEP);
   
   // select LoRa modem (from sleep mode)
   opmodeLora();
   // enter standby mode (required for FIFO loading))
   opmode(OPMODE_STANDBY);
   // configure LoRa modem (cfg1, cfg2)
   configLoraModem(Config);
   // configure frequency
   configChannel(Config);
   // configure output power
   writeReg(RegPaRamp, (readReg(RegPaRamp) & 0xF0) | 0x08); // set PA ramp-up time 50 uSec
   configPower(Config);
   // set sync word
   writeReg(LORARegSyncWord, LORA_MAC_PREAMBLE);
   
   // set the IRQ mapping DIO0=TxDone DIO1=NOP DIO2=NOP
   writeReg(RegDioMapping1, MAP_DIO0_LORA_TXDONE|MAP_DIO1_LORA_NOP|MAP_DIO2_LORA_NOP);
   // clear all radio IRQ flags
   writeReg(LORARegIrqFlags, 0xFF);
   // mask all IRQs but TxDone
   writeReg(LORARegIrqFlagsMask, ~IRQ_LORA_TXDONE_MASK);
   
   // initialize the payload size and address pointers    
   writeReg(LORARegFifoTxBaseAddr, 0x00);
   writeReg(LORARegFifoAddrPtr, 0x00);
   
   opmode(OPMODE_SLEEP);
   
}

void radio_init_rx (LORAConfig *Config) {
    
   // manually reset radio
   lora_reset();
   //////////////////////
   
   
   opmode(OPMODE_SLEEP);
   
   // select LoRa modem (from sleep mode)
   opmodeLora();
   // enter standby mode (warm up))
   opmode(OPMODE_STANDBY);
   // don't use MAC settings at startup
   // single or continuous rx mode
   // configure LoRa modem (cfg1, cfg2)
   configLoraModem(Config);
   // configure frequency
   configChannel(Config);
   // set LNA gain
   writeReg(RegLna, LNA_RX_GAIN); 
   // set max payload size
   //writeReg(LORARegPayloadMaxLength, PAYLOAD_SIZE);
   
   // set symbol timeout (for single rx)
   writeReg(LORARegSymbTimeoutLsb, 0xFF);
   // set sync word
   writeReg(LORARegSyncWord, LORA_MAC_PREAMBLE);
   
   // configure DIO mapping DIO0=RxDone DIO1=RxTout DIO2=NOP
   writeReg(RegDioMapping1, MAP_DIO0_LORA_RXDONE|MAP_DIO1_LORA_RXTOUT|MAP_DIO2_LORA_NOP);
   // clear all radio IRQ flags
   writeReg(LORARegIrqFlags, 0xFF);
   // enable required radio IRQs
   writeReg(LORARegIrqFlagsMask, (uint8_t)~(IRQ_LORA_RXDONE_MASK + IRQ_LORA_RXTOUT_MASK));
   
   opmode(OPMODE_SLEEP);

}

const unsigned char CRC7_POLY = 0x91;

uint8_t ui8_CRCGen(uint8_t * message, uint8_t length){
   uint8_t i;
   uint8_t crc;
/*  
  unsigned char i, j, crc = 0;
 for (i = 0; i < length; i++)
  {
    crc ^= message[i];
    for (j = 0; j < 8; j++)
    {
      if (crc & 1)
        crc ^= CRC7_POLY;
      crc >>= 1;
    }
  }*/
   // 1.Enable the CRC peripheral clock
   RCC_ENABLE_AHB_PERIPHERAL(CRC_AHB);   
   // 2. Set DR to reset value, by configuring CRC_INIT
   CRC->INIT=0xffffffff;
   // 3. Set mode REV_IN, REV_OUT in CRC_CR
   CRC->CR=0;
   // 4. Set the Poly
   CRC->POL=0x91;
   // 5. Reset the CRC data register
   CRC->CR |= 0x01;
   
   i=0;
   do {
      // 6. Set the CRC data register
      CRC->DR = message[i];
      i++;
   } while(i<length);
   
   crc=CRC->DR;
   // Disable the CRC peripheral clock
   RCC_DISABLE_AHB_PERIPHERAL(CRC_AHB);
   
  return crc;
}

void DebugLoraCrc(void){
#if defined(DEBUG_PRINT) && defined(LORA_DBG_CNT)
   dbg_print("Rx=");           print_BCD(loraDbg.LoraRx, 0);
   dbg_print(",Tx=");          print_BCD(loraDbg.LoraTx, 0);
   dbg_print(",fwCRCok=");     print_BCD(loraDbg.FwCrcOk, 0);
   dbg_print(",fwCRCko=");     print_BCD(loraDbg.FwCrcKo, 0);
   dbg_print(",loraCRCok=");   print_BCD(loraDbg.LoraCrcOk, 0);
   dbg_print(",loraCRCko=");   print_BCD(loraDbg.LoraCrcKo, 0);
   dbg_print(",addrOK=");      print_BCD(loraDbg.AdrOk, 0);
   dbg_print(",addrKO=");      print_BCD(loraDbg.AdrKo, 0);
   dbg_print(",Valid=");       print_BCD(loraDbg.ValidOk, 0);
   dbg_print(",!Valid= ");     print_BCD(loraDbg.ValidKo, 0);
   dbg_print("\r\n");
#endif 
}

/*   
void DebugNtc(void){
#ifdef DEBUG_PRINT
  dbg_print("Bad NTCs = ");    print_BCD(bad_ntc, 0);
        dbg_print(", Bad Soil Temps = ");    print_BCD(bad_soil_temp, 0);
  dbg_print("\r\n");
#endif
}

void DebugMsgType(void){
#ifdef DEBUG_PRINT
        dbg_print("Bad MSGs = ");      print_BCD(bad_msg, 0);
        dbg_print("\r\n");
#endif  
}
*/
