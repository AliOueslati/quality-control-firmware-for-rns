#include "stm32l0xx.h"
#include "adc.h"

#define TIMEOUT_ERROR 0x1FFF


uint8_t ADC_CALIBRATION(ADC_TypeDef *ADCperiph){
	uint16_t timeOut=0;
	uint8_t error=0;
	
	ADCperiph->CR   |= 1UL<<31;               // ADCAL
	while (ADCperiph->CR & (1UL<<31))         // WAIT ADCAL=0
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		

  return error;
}


uint8_t ADC_ENABLE(ADC_TypeDef *ADCperiph){
	uint16_t timeOut=0;
	uint8_t error=0;
	
	ADCperiph->ISR |=0x01;                   // CLEAR ADRDY
	
	ADCperiph->CR   |= 0x01;                 // ENABLE
	while ((ADCperiph->ISR & 0x01) == 0)     // WAIT ADRDY
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		
	ADCperiph->ISR |=0x01;                   // CLEAR ADRDY
  ADC->CCR    |= (1UL<<25);
	ADCperiph->SMPR |=0x05;                  // SAMPLING TIME TO MEDIUM
	
  return error;
}

uint8_t ADC_DISABLE(ADC_TypeDef *ADCperiph){
	uint16_t timeOut=0; 
	uint8_t error=0;
	
	if  ((ADCperiph->CR & (0x01<<4)))           //ADC on going ==1?
	{
		ADCperiph->CR |= (0x01)<<4;               //STOP
		
		while (ADCperiph->CR & (0x01<<4))         //WAIT ADSTP
		{
		  if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
		}	
		
	}
	
	ADCperiph->ISR  |= (0x01<<3);               //CLEAR EOS
	ADCperiph->CR   |= (0x01<<1);               //ADDIS 
	
	timeOut=0;
	while (ADCperiph->CR & 0x01)                //UNTIL ADEN=0
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}	
	
	return error;
}
void ADC_SET_AUTOFF(ADC_TypeDef *ADCperiph){
	ADCperiph->CFGR1  |= (1UL<<15);             
}

void ADC_SET_OV(ADC_TypeDef *ADCperiph,uint8_t Log2){
  ADCperiph->CFGR2  &= ~(0xF<<5);
  ADCperiph->CFGR2  |= ((Log2+1)<<5);	
	ADCperiph->CFGR2  &= ~(0x07<<2);
  ADCperiph->CFGR2  |= (Log2<<2);	
	
	ADCperiph->CFGR2  |= 0x01;	
}

void ADC_CLK_SOURCE(ADC_TypeDef *ADCperiph, uint8_t CKMODE){
	ADCperiph->CFGR2  |= ((CKMODE&0x3)<<30);             
}

void ADC_SET_SINGLE_CONVERSION(ADC_TypeDef *ADCperiph){
	ADCperiph->CFGR1  &= ~(0x01<<13);
}

void ADC_SET_CHANNELS(ADC_TypeDef *ADCperiph, uint32_t CHSEL){
	ADCperiph->CHSELR = (CHSEL&0x3FFFF); 
}

void ADC_START(ADC_TypeDef *ADCperiph){
    ADCperiph->ISR |= (0x01)<<3; //Clear EOSEQ
    ADCperiph->CR |= (0x01)<<2;
}

uint8_t ADC_READ_DATA(ADC_TypeDef *ADCperiph, uint16_t * Result){
	uint16_t timeOut=0; 
	uint8_t error=0;

	while ((ADCperiph->ISR & (0x01<<2)) ==0)    // WAIT EOC
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}	
	
	*Result=ADCperiph->DR;
	
	return error;
}
