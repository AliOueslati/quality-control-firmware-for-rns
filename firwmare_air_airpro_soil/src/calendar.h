#ifndef CALENDAR_H
#define CALENDAR_H

#include "stm32l0xx.h"
#include "rtc.h"

typedef struct{ //BCD format
	uint8_t  YEAR;
	uint8_t  MONTH;
	uint8_t  DAY;
	uint8_t  HOUR;
	uint8_t  MINUTE;
	uint8_t  SECOND;
	uint8_t  dummy1;
	uint8_t  dummy2;
} rtc_calendar;

uint8_t calendar_init(void);
uint8_t calendar_set(rtc_calendar *);
uint8_t calendar_get(rtc_calendar *);
void    calendar_alarm(uint16_t, uint8_t);

#endif
