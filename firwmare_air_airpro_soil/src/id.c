#include "stm32l0xx.h"
#include "rcc.h"

#define ID_BASE_ADDR        0x1FF80050 
             

/*------------------------------------------------------------ 
Func: EEPROM 
Note: 
 -------------------------------------------------------------*/  
void ID_READ(uint32_t *Buffer)  
{  
	uint32_t *wAddr;  
  wAddr=(uint32_t *)(ID_BASE_ADDR);  

  //*Buffer=(((*wAddr)&0x00FF0000)>>8)|(((*wAddr)&0x000000FF));
 
	// Enable the CRC peripheral clock
  RCC_ENABLE_AHB_PERIPHERAL(CRC_AHB);
  // Reset the CRC data register (initial value 0xFFFFFFFF)
  CRC->CR |= 0x01;
                                                                   
  CRC->DR = *(wAddr);
	CRC->DR = *(wAddr+1);
	CRC->DR = *(wAddr+5);
     
  // Final CRC32
  *Buffer = CRC->DR;                                                                                   
  // Disable the CRC peripheral clock
   RCC_DISABLE_AHB_PERIPHERAL(CRC_AHB);

	}
