#include "stm32l0xx.h"
#include "tsl2561.h" 
#include "i2c.h" 
#include "delay.h" 
#include "gpio.h" 
#include "rcc.h"
#include "timer.h"
#include "config.h"

#define I2C I2C1


/*
 * TSL2561 I2C Commands
 */
#define COMMAND_CLEAR_BIT             0x40    // Clears any pending interrupt (write 1 to clear)
#define COMMAND_WORD_BIT              0x20    // 1 = read/write word (rather than byte)
#define COMMAND_BLOCK_BIT             0x10    // 1 = using block read/write
#define COMMAND_ADDRESS_CONTROL       0x80    // Addresses include CMD bit
#define COMMAND_ADDRESS_TIMING        0x81    
#define COMMAND_ADDRESS_ID            0x8A    
#define COMMAND_ADDRESS_CHAN0_LOW     0x8C
#define COMMAND_ADDRESS_CHAN0_HIGH    0x8D
#define COMMAND_ADDRESS_CHAN1_LOW     0x8E
#define COMMAND_ADDRESS_CHAN1_HIGH    0x8F
#define CONTROL_POWEROFF              0x00
#define CONTROL_POWERON               0x03


/*
 * TSL2561 Errors definitions
 */
#define TSL2561_OK           0
#define TSL2561_TIMEOUT			 1


/*
 * PRIVATE TSL2561 FUNCTIONS DECLARATIONS
 */
uint8_t tsl2561_read_byte(uint8_t, uint8_t *);
uint8_t tsl2561_write_byte(uint8_t, uint8_t);
uint8_t tsl2561_read_word(uint8_t, uint16_t *);
void tsl2561_wait_conversion(void);



uint8_t CURRENT_TSL2561_TIMING=TIMING_INTG_TIME_402MS; //Default

/*
 * PRIVATE TSL2561 FUNCTIONS DEFINITIONS
 */
uint8_t tsl2561_read_byte(uint8_t COMMAND, uint8_t * Result){
  
	uint8_t error, exitCode=TSL2561_OK ;
	
	// Configure I2C //
  I2C_ENABLE(I2C, STD_MODE_100K,0);
	
	I2C_SET_SADD_7BITS(I2C, TSL2561_ADDDRESS);  // Address setting

  I2C_SET_NBYTES(I2C, 1);                     // 1 byte write (COMMAND byte)
	 
	I2C_SET_WRITE(I2C);                         // Write mode

	// Start communication //
	I2C_START(I2C);														 	// START			
	
	error=I2C_WRITE(I2C, COMMAND);              // Write byte

	error|=I2C_WAIT_TC(I2C);                    // byte transfer wait

  I2C_SET_NBYTES(I2C, 1);                     // 1 byte read 
	
	I2C_SET_READ(I2C);                          // Read mode
	
  I2C_START(I2C);                             // Rep START	
	
	error|=I2C_READ(I2C, Result);               // Read value		

	error|=I2C_WAIT_TC(I2C);                    // byte transfer wait	

	error|=I2C_STOP(I2C);	                      // STOPT

	I2C_DISABLE(I2C);
	
	if (error) exitCode=TSL2561_TIMEOUT;
	return exitCode;

}
uint8_t tsl2561_write_byte(uint8_t COMMAND, uint8_t VALUE){
  
	uint8_t error,exitCode=TSL2561_OK;
	
	// Configure I2C //
  I2C_ENABLE(I2C, STD_MODE_100K,0);
	 
	I2C_SET_SADD_7BITS(I2C, TSL2561_ADDDRESS);  // Address setting

  I2C_SET_NBYTES(I2C, 2);                     // 2 byte write (COMMAND byte and VALUE)
	 
	I2C_SET_WRITE(I2C);                         // Write mode

	// Start communication //
	I2C_START(I2C);															// Rep START			
	
	error=I2C_WRITE(I2C, COMMAND);              // Write byte

	error|=I2C_WRITE(I2C, VALUE);               // Write byte
 
	error|=I2C_WAIT_TC(I2C);                    // byte transfer wait

  error|=I2C_STOP(I2C);	                      // STOPT

	I2C_DISABLE(I2C);
	
	if (error) exitCode=TSL2561_TIMEOUT;
	return exitCode;
	
}

uint8_t tsl2561_read_word(uint8_t COMMAND, uint16_t * Result){  
  
	uint8_t msb,lsb,error,exitCode=TSL2561_OK;
	 
	COMMAND= COMMAND | COMMAND_WORD_BIT;
	
	// Configure I2C //
  I2C_ENABLE(I2C, STD_MODE_100K,0);
	 
	I2C_SET_SADD_7BITS(I2C, TSL2561_ADDDRESS);  // Address setting

  I2C_SET_NBYTES(I2C, 1);                     // 1 byte write (COMMAND byte)
	 
	I2C_SET_WRITE(I2C);                         // Write mode

	// Start communication //
	I2C_START(I2C);															// START			
	
	error=I2C_WRITE(I2C, COMMAND);              // Write byte

	error|=I2C_WAIT_TC(I2C);                    // byte transfer wait

	I2C_SET_NBYTES(I2C, 2);                     // 2 byte read 
	
	I2C_SET_READ(I2C);                          // Read mode
	
	I2C_START(I2C);                             // Rep START	
	
	error|=I2C_READ(I2C, &lsb);                 // Read value

	error|=I2C_READ(I2C, &msb);                 // Read value			

  error|=I2C_WAIT_TC(I2C);                    // byte transfer wait

	error|=I2C_STOP(I2C);	                      // STOPT
						
	I2C_DISABLE(I2C);
	
	*Result=((((uint16_t)msb)<<8) | lsb);
	
	if (error) exitCode=TSL2561_TIMEOUT;
	return exitCode;
	
}

void tsl2561_wait_conversion(void){

	switch (CURRENT_TSL2561_TIMING&0x03)
	{
		case 0x00:
			delay_ms(20);
			break;
		case 0x01:
			delay_ms(120);
			break;
		case 0x02:
			delay_ms(500);
			break;
	}
}

/*
 * PUBLIC TSL2561 FUNCTIONS DEFINITIONS
 */
uint8_t tsl2561_power_on(void){
	uint8_t error;
  error=tsl2561_write_byte(COMMAND_ADDRESS_CONTROL,CONTROL_POWERON);
	if (!error)
	{
		return TSL2561_OK;
	}
	else
	{
		return TSL2561_TIMEOUT;
	}
			
}

uint8_t tsl2561_power_off(void){   //Solo convertidores
	uint8_t error;
  error=tsl2561_write_byte(COMMAND_ADDRESS_CONTROL,CONTROL_POWEROFF);
	if (!error)
	{
		return TSL2561_OK;
	}
	else
	{
		return TSL2561_TIMEOUT;
	}
}


uint8_t tsl2561_get_light(uint16_t *Light){
	uint8_t error;
	error= tsl2561_read_word(COMMAND_ADDRESS_CHAN0_LOW, Light);
	if (!error)
	{
		return TSL2561_OK;
	}
	else
	{
		return TSL2561_TIMEOUT;
	}
}

uint8_t tsl2561_get_ir(uint16_t *IR){
	uint8_t error;
	error= tsl2561_read_word(COMMAND_ADDRESS_CHAN1_LOW, IR);
	if (!error)
	{
		return TSL2561_OK;
	}
	else
	{
		return TSL2561_TIMEOUT;
	}
}

uint8_t tsl2561_set_timing(uint8_t Command){
	uint8_t error;
  error=tsl2561_write_byte(COMMAND_ADDRESS_TIMING,Command);
	if (!error)
	{
		CURRENT_TSL2561_TIMING=Command;
		return TSL2561_OK;
	}
	else
	{
		return TSL2561_TIMEOUT;
	}

}


// Recommended function Low Power (power on, wait, read and power off)
uint8_t tsl2561_read(tsl2561_result* Result){
	uint8_t  exitCode;
	
	tsl2561_set_timing(TIMING_INTG_TIME_101MS);
	
	exitCode=tsl2561_power_on();
	
	tsl2561_wait_conversion();
	
	exitCode|=tsl2561_get_light(&(Result->VISIBLE_IR));

  exitCode|=tsl2561_get_ir(&(Result->IR));
	
	Result->VISIBLE=(Result->VISIBLE_IR)-(Result->IR);

	exitCode|=tsl2561_power_off();

	return exitCode;

}
