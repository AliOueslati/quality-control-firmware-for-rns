#ifndef RCC_H
#define RCC_H

// must be modified in RTE/Device/STM32L051C8/system_stm32l0xx.c
//#define MSI_VALUE    ((uint32_t) 2000000) /*!< Value of the Internal oscillator in Hz*/
//#define HSI_VALUE    ((uint32_t)16000000) /*!< Value of the Internal oscillator in Hz*/
//#define HSE_VALUE    ((uint32_t)25000000) /*!< Value of the External oscillator in Hz */

#include "stm32l0xx.h"

/* AHB PERIPHERALS */
#define     DMA_AHB 1UL<<0
#define     MIF_AHB 1UL<<8
#define     CRC_AHB 1UL<<12
#define   TOUCH_AHB 1UL<<16
#define     RNG_AHB 1UL<<20
#define   CRYPT_AHB 1UL<<24

/* APB1 PERIPHERALS */
#define    TIM2_APB1 1UL<<0
#define    TIM6_APB1 1UL<<4
#define     LCD_APB1 1UL<<9
#define    WWDG_APB1 1UL<<11
#define    SPI2_APB1 1UL<<14
#define  USART2_APB1 1UL<<17
#define LPUART1_APB1 1UL<<18
#define    I2C1_APB1 1UL<<21
#define    I2C2_APB1 1UL<<22
#define     CRS_APB1 1UL<<27
#define     PWR_APB1 1UL<<28
#define     DAC_APB1 1UL<<29
#define  LPTIM1_APB1 1UL<<31

/* APB2 PERIPHERALS */
#define  SYSCFG_APB2 1UL<<0
#define   TIM21_APB2 1UL<<2
#define   TIM22_APB2 1UL<<5
#define    MIFI_APB2 1UL<<5
#define     ADC_APB2 1UL<<9
#define    SPI1_APB2 1UL<<12
#define  USART1_APB2 1UL<<14
#define  DBGMCU_APB2 1UL<<22

/* GPIOS PERIPHERALS */
#define   GPIOA_IO 1UL<<0
#define   GPIOB_IO 1UL<<1
#define   GPIOC_IO 1UL<<2
#define   GPIOD_IO 1UL<<3
#define   GPIOH_IO 1UL<<7


void RCC_ENABLE_AHB_PERIPHERAL(uint32_t);
void RCC_DISABLE_AHB_PERIPHERAL(uint32_t);
void RCC_ENABLE_APB1_PERIPHERAL(uint32_t);
void RCC_DISABLE_APB1_PERIPHERAL(uint32_t);
void RCC_ENABLE_APB2_PERIPHERAL(uint32_t);
void RCC_DISABLE_APB2_PERIPHERAL(uint32_t);
void RCC_ENABLE_GPIO (uint32_t);
void RCC_DISABLE_GPIO (uint32_t);
uint8_t RCC_ENABLE_CLOCK_LSE32K (void);
uint8_t RCC_ENABLE_CLOCK_LSI(void);
uint8_t RCC_ENABLE_CLOCK_HSI16_4 (void);
uint8_t RCC_CLOCK_MSI_4 (void);
uint8_t RCC_CLOCK_MSI_2 (void);
uint8_t RCC_CLOCK_HSE_25 (void);
void RCC_DISABLE_CLOCK_HSI (void);
void RCC_DISABLE_CLOCK_MSI (void);
void RCC_DISABLE_CLOCK_HSE (void);
void RCC_SET_CLK (uint8_t clk_idx, uint8_t set_source);

#define  CLK_SRC_MSI   0
#define  CLK_SRC_HSI   1
#define  CLK_SRC_HSE   2

#endif

