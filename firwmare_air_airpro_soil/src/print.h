#ifndef PRINT_H
#define PRINT_H

#include "stm32l0xx.h"

void dbg_print_init(void);
void dbg_print(char *Buf);
void srv_print(char *Buf);


void print (uint8_t *, uint16_t, uint8_t );
void print8  (uint8_t  *, uint16_t);
void print16 (uint16_t *, uint16_t);
void print32 (uint32_t *, uint16_t);
void print_BCD (int16_t , uint8_t);
void print_unsBCD (uint16_t , uint8_t);


#endif

