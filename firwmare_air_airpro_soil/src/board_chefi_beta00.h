/*
 * Board pinout of Chefi Beta 00 (includes 00, 00air and 01)
 */

#define BOARD_CHEFI_BETA

// LEDs (the ones in the board are identical to Monofi sensor)
#define LED1_PORT           	GPIOA
#define LED1_PIN            	PIN12
#define LED_GREEN           	1
#define LED2_PORT           	GPIOA
#define LED2_PIN            	PIN8
#define LED_RED             	2

// LEDs activation
#define GPIO_LED_ON         	GPIO_SET
#define GPIO_LED_OFF        	GPIO_CLEAR

// Button
#define BUTTON_PORT         	GPIOA  // Must be GPIOA, see button.c
#define BUTTON_PIN          	PIN11

// LORA RADIO
#define LORA_RESET_PORT		GPIOA
#define LORA_RESET_PIN		PIN3
#define LORA_SPI_CS_PORT	GPIOA
#define LORA_SPI_CS_PIN		PIN4
#define LORA_DIO0_PORT		GPIOB
#define LORA_DIO0_PIN		PIN14
#define LORA_DIO1_PORT      	GPIOB
#define LORA_DIO1_PIN       	PIN13
#define LORA_DIO2_PORT      	GPIOB
#define LORA_DIO2_PIN       	PIN12
#define LORA_DIO4_PORT      	GPIOB
#define LORA_DIO4_PIN       	PIN15

// CC1101 Radio (not present)
/*
#define CC1101_IRQ_PORT_GPIOB
#define CC1101_IRQ_PORT GPIOB
#define CC1101_IRQ_PIN  PIN2  // SPI-IRQ1 on schematic, GDO2 pin on CC1101
#define CC1101_GDO0_PORT GPIOB
#define CC1101_GDO0_PIN  PIN10 // SPI-CE1 on schematic
#define CC1101_SPI_CS_PORT GPIOB
#define CC1101_SPI_CS_PIN  PIN1  // SPI-CSN1 on schematic
*/

// SD card (not present)
/*
#define SD_SPI_CS_PORT GPIOB
#define SD_SPI_CS_PIN  PIN0  // SPI-CE2 on schematic
*/

// SPI
#define SPI_SCK_PORT 		GPIOA
#define SPI_SCK_PIN  		PIN5
#define SPI_MISO_PORT 		GPIOA
#define SPI_MISO_PIN  		PIN6
#define SPI_MOSI_PORT 		GPIOA
#define SPI_MOSI_PIN  		PIN7

// I2C
#define I2C_SCL_PORT 		GPIOB
#define I2C_SCL_PIN  		PIN6
#define I2C_SDA_PORT 		GPIOB
#define I2C_SDA_PIN  		PIN7

// Bluetooth (not present)
/*
#define BT_KEY_PORT 		GPIOB
#define BT_KEY_PIN  		PIN15
#define BT_RST_PORT 		GPIOA
#define BT_RST_PIN  		PIN8
*/

// Soil Sensor
#define SOIL_ADC1_PORT 		GPIOA
#define SOIL_ADC1_PIN  		PIN0
#define SOIL_ADC2_PORT 		GPIOA
#define SOIL_ADC2_PIN  		PIN1
#define SOIL_NTC_ADC0_PORT 	GPIOB
#define SOIL_NTC_ADC0_PIN  	PIN0
#define SOIL_NTC_ADC1_PORT 	GPIOB
#define SOIL_NTC_ADC2_PIN  	PIN1
#define SOIL_NTC_VDD_PORT 	GPIOB
#define SOIL_NTC_VDD_PIN  	PIN2
#define SOIL_X1_STZ_PORT 	GPIOB
#define SOIL_X1_STZ_PIN  	PIN9
#define SOIL_X2_STZ_PORT 	GPIOB
#define SOIL_X2_STZ_PIN  	PIN8
#define SOIL_R1_0_PORT 		GPIOB
#define SOIL_R1_0_PIN  		PIN3
#define SOIL_R1_1_PORT 		GPIOB
#define SOIL_R1_1_PIN  		PIN5
#define SOIL_SWEEP_PORT		GPIOA
#define SOIL_SWEEP_PIN		PIN15
#define SOIL_EN_MULT_PORT	GPIOC
#define SOIL_EN_MULT_PIN	PIN13
#define SOIL_SQ_MULT_PORT	GPIOB
#define SOIL_SQ_MULT_PIN	PIN4

// VBAT
#define VBAT_PORT		GPIOA
#define VBAT_PIN		PIN2

// Serial Ports
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// UART1 (header for Log/Host comm)
#define UART1_TX_PORT		GPIOA 
#define UART1_TX_PIN 		PIN9
#define UART1_RX_PORT 		GPIOA
#define UART1_RX_PIN 		PIN10

#ifdef HAS_HX711
   // HX711 
   // Weight-Scale module for use with load cells
   // Seems I2C, but can't share serial bus
   // SDA = TX
   // SCK = RX
   #define HX711_SCK_PORT          GPIOB
   #define HX711_SCK_PIN           PIN11
   #define HX711_SDA_PORT          GPIOB
   #define HX711_SDA_PIN           PIN10
#else
   // CO2 sensor
   // Carefull:
   //   - Uses LPUART1
   //   - Sch/drive signal is confusing: RX <-> TX
   //   - Silk drawings are OK

   #define CO2_RX_PORT			GPIOB
   #define CO2_RX_PIN			PIN11
   #define CO2_TX_PORT			GPIOB
   #define CO2_TX_PIN			PIN10

   // UARTL1 (LPUART1): DBG print (in central node)
   /*
   #define UARTL1_RX_PORT 		GPIOB
   #define UARTL1_RX_PIN 		PIN11
   #define UARTL1_TX_PORT		GPIOB 
   #define UARTL1_TX_PIN 		PIN10
    */
#endif


// Select the correct serial port
#if defined(CENTRAL_NODE)
#define UART_HOST_TX 		UART1_SEND
#define UART_HOST_RX 		UART1_RECEIVE

//#if defined(DEBUG_PRINT)
//#define UART_DBG_TX 		UARTL1_SEND
//#define UART_DBG_RX 		UARTL1_RECEIVE
//#endif

#elif defined(REMOTE_NODE) //&& defined(DEBUG_PRINT)

#define UART_DBG_TX 		UART1_SEND
#define UART_DBG_RX 		UART1_RECEIVE
#define LPUART_TX       LPUART1_SEND        // added by Thimo
#define LPUART_RX       LPUART1_RECEIVE     // added by Thimo

#endif

