#ifndef QC_H
#define QC_H

#include "stm32l0xx.h"
#include "node.h"

void qc_init(void);
void qc_read(node_info *);


#endif
