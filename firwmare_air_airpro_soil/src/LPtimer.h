#ifndef LPTIMER_H
#define LPTIMER_H

#include "stm32l0xx.h"


void ENABLE_LPTIMER(LPTIM_TypeDef *);
void DISABLE_LPTIMER(LPTIM_TypeDef *);
void START_LPTIMER(LPTIM_TypeDef *);
//void SET_TIMER_PRESCALER(TIM_TypeDef *,uint16_t);
void SET_LPTIMER_ARR(LPTIM_TypeDef *,uint16_t);
void SET_LPTIMER_ARR_INTERRUPT_ENABLE(LPTIM_TypeDef *);
void SET_LPTIMER_ARR_INTERRUPT_DISABLE(LPTIM_TypeDef *);

	
#endif
