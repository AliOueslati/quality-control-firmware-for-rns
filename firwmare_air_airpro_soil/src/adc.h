#ifndef ADC_H
#define ADC_H

#include "stm32l0xx.h"

/*
 * CLOCK MODES
 */
#define CK_ADC    0x00
#define CK_PCLK_2 0x01
#define CK_PCLK_4 0x02

/*
 * CHANNELS
 */
 
#define CH_0        0x00001
#define CH_1        0x00002
#define CH_2        0x00004
#define CH_3        0x00008
#define CH_4        0x00010
#define CH_5        0x00020
#define CH_6        0x00040
#define CH_7        0x00080
#define CH_8        0x00100
#define CH_9        0x00200
#define CH_10       0x00400
#define CH_11       0x00800
#define CH_12       0x01000
#define CH_13       0x02000
#define CH_14       0x04000
#define CH_15       0x08000
#define CH_16       0x10000
#define CH_17       0x20000

/*
 * OVERSAMPLER
 */
 
#define OV_2         0
#define OV_4         1
#define OV_8         2
#define OV_16        3
#define OV_32        4
#define OV_64        5
#define OV_128       6
#define OV_256       7

uint8_t ADC_CALIBRATION(ADC_TypeDef *ADCperiph);

uint8_t ADC_ENABLE(ADC_TypeDef *);

uint8_t ADC_DISABLE(ADC_TypeDef *);

void ADC_SET_AUTOFF(ADC_TypeDef *);

void ADC_CLK_SOURCE(ADC_TypeDef *, uint8_t);

void ADC_SET_SINGLE_CONVERSION(ADC_TypeDef *);

void ADC_SET_CHANNELS(ADC_TypeDef *, uint32_t);

void ADC_SET_OV(ADC_TypeDef *, uint8_t);

void ADC_START(ADC_TypeDef *);

uint8_t ADC_READ_DATA(ADC_TypeDef *, uint16_t *);



#endif
