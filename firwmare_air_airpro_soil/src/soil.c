// --------------------------------
// soil related
// --------------------------------
// History:
// JChavez 23 April update
// JLomas  26 April printing Vd and VCH4 
// JChavez 27 Jun: board_XXX.h has the pin allocation
// JChavez 27 Jun: xtal and mux included in soil
// JChavez 28 Jun: scale included
// JChavez 23 Aug: soil_procedure can perform one single measure
// --------------------------------
#include "stm32l0xx.h"
#include "adc.h"
#include "gpio.h"
#include "rcc.h"
#include "config.h"
//#include "print.h"
#include "soil.h"
#include "delay.h"
#include "print.h"
// Only for calibration
//#include "hx711.h"
#include "timer.h"
#include "system.h"
#include "rcc.h" 

extern uint8_t BUTTON_SHORT_INT;

// Muxes
// --------------------------------
void mux_init(void){
   RCC_ENABLE_GPIO (GPIOB_IO);    //Enable GPIOB clock 	
   
   // Default state, no resistor is selected
   GPIO_CLEAR(         SOIL_R1_0_PORT, SOIL_R1_0_PIN);
   GPIO_CLEAR(         SOIL_R1_1_PORT, SOIL_R1_1_PIN);
   
   SET_GPIO_AS_OUTPUT( SOIL_R1_0_PORT, SOIL_R1_0_PIN);
   SET_GPIO_AS_OUTPUT( SOIL_R1_1_PORT, SOIL_R1_1_PIN);
}

void mux_select(uint8_t select){
   switch (select) {
    case 0:
      GPIO_CLEAR( SOIL_R1_0_PORT, SOIL_R1_0_PIN);
      GPIO_CLEAR( SOIL_R1_1_PORT, SOIL_R1_1_PIN);
      break;
    case 1:
      GPIO_SET(   SOIL_R1_0_PORT, SOIL_R1_0_PIN);
      GPIO_CLEAR( SOIL_R1_1_PORT, SOIL_R1_1_PIN);
      break;
    case 2:
      GPIO_CLEAR( SOIL_R1_0_PORT, SOIL_R1_0_PIN);
      GPIO_SET(   SOIL_R1_1_PORT, SOIL_R1_1_PIN);
      break;
    case 3:
      GPIO_SET(   SOIL_R1_0_PORT, SOIL_R1_0_PIN);
      GPIO_SET(   SOIL_R1_1_PORT, SOIL_R1_1_PIN);
      break;
   }
   delay_ms(10); // TODO really usefull?
}

// Oscillator
// --------------------------------
void xtal_init(void){
   RCC_ENABLE_GPIO (GPIOB_IO);    //Enable GPIOB clock 	
   
   // known state, both disabled
   GPIO_CLEAR(         SOIL_X1_STZ_PORT, SOIL_X1_STZ_PIN);
   GPIO_CLEAR(         SOIL_X2_STZ_PORT, SOIL_X2_STZ_PIN);
   
   SET_GPIO_AS_OUTPUT( SOIL_X1_STZ_PORT, SOIL_X1_STZ_PIN);
   SET_GPIO_AS_OUTPUT( SOIL_X2_STZ_PORT, SOIL_X2_STZ_PIN);
}

void xtal_disable(void){
   // disable both
   GPIO_CLEAR(         SOIL_X1_STZ_PORT, SOIL_X1_STZ_PIN);
   GPIO_CLEAR(         SOIL_X2_STZ_PORT, SOIL_X2_STZ_PIN);
}

void xtal_enable(uint8_t xtal){
   xtal_disable(); //To ensure than only one is active at the same time
   delay_ms(1);   
   switch (xtal) {
    case 1:
      GPIO_SET(         SOIL_X1_STZ_PORT, SOIL_X1_STZ_PIN);
      break;
    case 2:
      GPIO_SET(         SOIL_X2_STZ_PORT, SOIL_X2_STZ_PIN);
      break;
   }
   delay_ms(1);   
}

// Soil
// --------------------------------

uint8_t soil_init(uint8_t  Alg){
   // --------------------------------
   // Common initialization for Algorithms
   // - Enable Vdd for NTC measures
   // - Enable ADC and define channels
   // --------------------------------
   uint8_t error;
   
   // a) NTC Measure is common
   // Rise Vdd to measure NTC (due to transitor is inverted)
   //if(vdd_soil_init==0) {
   SET_GPIO_AS_OUTPUT(SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   //GPIO_SET(        SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   GPIO_CLEAR(        SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   //}

   // b) ADC's initialization is common, later channer are defined
   // TODO might change if soil mux/osc aren't in Port-B
   RCC_ENABLE_APB2_PERIPHERAL(ADC_APB2);
   
   ADC_CLK_SOURCE(ADC1, CK_PCLK_2);
   // chefi:    ADC_CLK_SOURCE(ADC1, CK_PCLK_4);
   ADC_CALIBRATION(ADC1);
   
   //   ADC_SET_OV(ADC1,OV_2);
   // chefi:    ADC_SET_OV(ADC1,OV_256);
   ADC_SET_OV(ADC1,OV_256);
   error = ADC_ENABLE(ADC1);
   ADC_SET_AUTOFF(ADC1);
   ADC_SET_SINGLE_CONVERSION(ADC1);
   
   // c) Disable Xtal and Muxes (Alg 1)
   mux_off();
   xtal_off();
   
   // d) Sweep Port (Alg 2) fixed level
   SET_GPIO_AS_OUTPUT(SOIL_SWEEP_PORT, SOIL_SWEEP_PIN);
   GPIO_SET(          SOIL_SWEEP_PORT, SOIL_SWEEP_PIN);
   
   // e) Astable (Alg 3) disable (high level in SHDN)
   SET_GPIO_AS_OUTPUT(SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
   GPIO_SET(          SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);

   SET_GPIO_AS_INPUT( SOIL_SQ_MULT_PORT, SOIL_SQ_MULT_PIN);
   
   
   switch (Alg) {
    case 1:
    case 2:
      // b) ADC's for NTC and algorithm's: 1 and 2
      
      /// Sample soil envelope (top-bottom) and temperature (ntc)
      // TODO: Change if ADC's used aren't
      // PA0 ADC0  UP-ADC1 (Vtop)
      // PA1 ADC1  UP-ADC2 (Vbot)
      // PB0 ADC8  NTC-ADC1
      // PA4 ADC9  NTC-ADC2
      ADC_SET_CHANNELS(ADC1, CH_0|CH_1|CH_8|CH_9);
      break;
      
    case 3:
      // b) ADC's for NTC only
      // Only NTC is needed
      // PB0 ADC8 NTC-ADC1
      // PA4 ADC9 NTC-ADC2
      ADC_SET_CHANNELS(ADC1, CH_8|CH_9);
      
      // e: Enable Astable (done later...)
      //GPIO_CLEAR(          SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
      break;
   }
   
   return error;
}

uint8_t soil_deinit(void){
   // --------------------------------
   // Disable Vdd for NTC measures
   // Disable ADC
   // --------------------------------
   uint8_t error;
   
   // a) NTC-VDD disable (avoids power waste)
   GPIO_SET(  SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   //GPIO_CLEAR(  SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   
   // b) Disable ADC
   error = ADC_DISABLE(ADC1);
   RCC_DISABLE_APB2_PERIPHERAL(ADC_APB2);

   // c) Disable Xtal and Muxes (Alg 1)
   mux_init();
   xtal_init();

   // d) Sweep Port (Alg 2) fixed level
   SET_GPIO_AS_OUTPUT(SOIL_SWEEP_PORT, SOIL_SWEEP_PIN);
   GPIO_SET(          SOIL_SWEEP_PORT, SOIL_SWEEP_PIN);

   // e) Astable (Alg 3) disable (high level in SHDN)
   SET_GPIO_AS_OUTPUT(SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
   GPIO_SET(          SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
   
   // for dbg measure
   //SET_GPIO_AS_OUTPUT( CO2_TX_PORT, CO2_TX_PIN);
   //GPIO_CLEAR    (     CO2_TX_PORT, CO2_TX_PIN);
   
   
   return error;
}

void get_soil_temp (analog_result *AdcRes, soil_result *SoilRes) {
   // --------------------------------
   // Compute the soil temperature using voltages of NTC
   uint16_t v1, v2;
   
   v1=AdcRes->CH8;
   v2=AdcRes->CH9;

   // v1 is the really useful
   SoilRes->NTC_TEMP=v1;
#ifdef DEBUG_PRINT
   dbg_print("NTC vals: "); 
   dbg_print(" ADC8: "); print_BCD(v1,0);
   dbg_print(" ADC9: "); print_BCD(v2,0);
   dbg_print("\n\r");
#endif
}

uint8_t soil_adc_read(analog_result* Result, uint8_t Alg){
   // --------------------------------
   // Read ADC's channels for Soil data
   // --------------------------------   
   uint8_t error;
   
   // To Measure Soil-Temp we need to put NTC's Vcc to high value
   //GPIO_SET(GPIOC, PIN13);
   // done in soil_init

   // Sample and convert all defined channels
   ADC_START(ADC1);
   
   switch(Alg) {
    case 1:
    case 2:
      error  = ADC_READ_DATA(ADC1, &(Result->CH1));
      error |= ADC_READ_DATA(ADC1, &(Result->CH2));
      error |= ADC_READ_DATA(ADC1, &(Result->CH8));
      error |= ADC_READ_DATA(ADC1, &(Result->CH9));
      break;
    case 3:
      error |= ADC_READ_DATA(ADC1, &(Result->CH8));
      error |= ADC_READ_DATA(ADC1, &(Result->CH9));
      break;
   }
   
      
   // After Soil-Temp measure, put to low value, saves power
   //GPIO_CLEAR(GPIOC, PIN13);
   // done in soil_deinit
   
   return error;
}

void soil_env_analyze (analog_result *AdcRes, uint8_t Scenario, soil_result *SoilRes) {
   // Envelope processing, useful for algorithms 1 and 2
   // - When called with Scenario 0, voltage drop (Vd) is calculated
   // - Subsequent scenarios uses this Vd value
   // --------------------------------
   static int16_t Vd=-1;
   uint16_t Vtop, Vbot;
   uint16_t Vm;   // Mean value
   uint16_t Vpp;  // Amplitude (peak-to-peak) value

   if(Vd!=-1) {
   	Vtop=(AdcRes->CH1)+Vd;
   	Vbot=(AdcRes->CH2)-Vd;

   	Vpp=Vtop-Vbot; 
   	Vm=(Vtop+Vbot); Vm=Vm>>1; 
   }
   switch(Scenario) {
    case 0:
      // Compute diode voltage drop
      // TODO: Alarm when Vd is weird!!
      
      //Vd=0;
      Vd=4095-(AdcRes->CH1);

      // Store data in structure
      SoilRes->VD = Vd;
      
      // DBG print using serial debug port, 
      // keep while in development
		
#ifdef DEBUG_PRINT
      dbg_print("Scenario 0");
      dbg_print(" Vadc1: ");	 print_BCD(AdcRes->CH1,0);
      dbg_print(" Vadc2: ");     print_BCD(AdcRes->CH2,0);
      dbg_print(" Vd=");         print_BCD(Vd,0);
      dbg_print("\r\n");
#endif
      
      // Store Soil-Temperature (NTC) in the data structure
      //Result->TEMP[measureCount]=measuredADC.CH4;
      //Result->NTC_TEMP=measuredADC.CH4;
      
      // Compute Soil-Temperature
      get_soil_temp(AdcRes, SoilRes);
      
      break;
    default: // Real scenarios
      if(Vd==-1) {
	 dbg_print(" Vd not init\r\n");
      }
      // Store data in structure
      SoilRes->SCEN_VM [Scenario-1] = Vm;
      SoilRes->SCEN_VPP[Scenario-1] = Vpp;
      
      // DBG print using serial debug port, 
      // keep while in development
#ifdef DEBUG_PRINT
      dbg_print("Scenario ");	 print_BCD(Scenario,0);
      dbg_print(" Vtop: ");      print_BCD(Vtop,0); 
      dbg_print(" Vbot: ");      print_BCD(Vbot,0);
      dbg_print(" Vm: ");        print_BCD(Vm,0);
      dbg_print(" Vpp ");        print_BCD(Vpp,0);
      dbg_print("\r\n");   
#endif
      break;
   }
}

void soil_proc_alg1(soil_result* Result) {
   // Algorithm 1
   // Procedure to set the different scenarios
   // take measures and retrieve obtained data 
   // in Result structure
   // --------------------------------
   uint8_t           scenCnt;
   analog_result     measuredADC;
   
   dbg_print("soil_proc_alg1 info: init\r\n");
   
   // Scenary (Osc-Mux pair) loop
   for(scenCnt=0;scenCnt<=6;scenCnt++) {
      // Begin from a known state: no Xtal, Mux open
      xtal_off();
      mux_off();
      
      // Make modification of each scenary
      switch(scenCnt) {
       case 0: // No oscillator, lower value
	 // this scenary allow to compute diode drop
	 xtal_off(); 
	 mux_R2(); // Updated based on J Chavez schematic and advice
	 break;
       case 1: // Xtal=100MHz, mux=R0
	 xtal_100();
	 mux_R0();
	 break;
       case 2: // Xtal=100MHz, mux=R1
	 xtal_100();
	 mux_R1();
	 break;
       case 3:  // Xtal=100MHz, mux=R2
	 xtal_100();
	 mux_R2();
	 break;
       case 4: // Xtal=1MHz, mux=R0
	 xtal_1();
	 mux_R0();
	 break;
       case 5: // Xtal=13MHz, mux=R1
	 xtal_1();
	 mux_R1();
	 break;
       case 6: // Xtal=13MHz, mux=R2
	 xtal_1();
	 mux_R2();
	 break;
       default:
	 break;
      } // scenarios count switch
      
      // Delay needed for stabilization
      // TODO: perhaps lower?
      //delay_sec(1); 	  		
      //delay_ms(50);
      delay_ms(100);
      
      // Measure
      soil_adc_read(&measuredADC, 1);
      
      // Fill Result structure with measures
      // DBG print and 
      soil_env_analyze(&measuredADC, scenCnt, Result);
      
   } // scenarios loop	
   
}

void soil_proc_alg2(soil_result* Result) {
   //analog_result     measuredADC;
   //uint8_t	     scenCnt;
   //uint16_t k;
   //uint32_t  aux;
   //uint8_t  ret;
   /* TO BE DESCRIBED */
   dbg_print("soil_proc_alg2 warn: not implemented\r\n");

   // a) Fixed high value in SWEEP signal. Done in soil_init()
   
   // b) Measure Vd (includes NTC read)
   //scenCnt=0;
   //delay_ms(50);  // Pause for stabilization
   //soil_adc_read(&measuredADC, 2);
   //soil_env_analyze(&measuredADC, scenCnt, Result);
   
   // Change system frequency
   //RCC_SET_CLK( CLK_SRC_HSI, 0);
   
   // For dbg measure
   //GPIO_SET    (     CO2_TX_PORT, CO2_TX_PIN);
   //delay_ms(50);
   //GPIO_CLEAR  (     CO2_TX_PORT, CO2_TX_PIN);
   //delay_ms(50);
   
   // For dbg measure
   //GPIO_SET    (     CO2_TX_PORT, CO2_TX_PIN);
   
   // Enable PWM
   SET_GPIO_AS_AF(               SOIL_SWEEP_PORT, SOIL_SWEEP_PIN, AF5); // TIM2_CH1
   RCC_ENABLE_APB1_PERIPHERAL(   TIM2_APB1); 
   SET_TIMER_PWM_MODE_mod(       TIM2, 4);
   UPDATE_TIMER_REGISTER(        TIM2);
   ENABLE_TIMER(                 TIM2);

   delay_ms(100);
   
   // Re-enable LP
   //RCC_SET_CLK( CLK_SRC_MSI, 0);
   //ret=change_voltage(3);
   //RCC_CLOCK_MSI_2();   // Back to normal clock. No need, 
   //RCC_DISABLE_CLOCK_HSE();
   //RCC_DISABLE_CLOCK_HSI();
   
   //disable PWM
   DISABLE_TIMER(               TIM2);
   RCC_DISABLE_APB1_PERIPHERAL( TIM2_APB1); 
   
   // For dbg measure
   //GPIO_CLEAR  (     CO2_TX_PORT, CO2_TX_PIN);
   
   return;
   // c) Set the output frequency
   
   // TODO!!!
   // Result->ALG_FREQ has the value en kHz
   
   // d) Measure the envelope in ADC's
   //scenCnt=1;
   //delay_ms(50);
   //soil_adc_read(&measuredADC, 2);
   //soil_env_analyze(&measuredADC, scenCnt, Result);

}

void soil_timer_init (void) {
   // Configure the timer to count rising edges in an interval
   // As there are many dependencies with pins used, timer needed, channel,
   // it receives no parameter/variable...
   //uint16_t          aux;
   
   // Set as TIM22_CH1
   SET_GPIO_AS_AF(   SOIL_SQ_MULT_PORT, SOIL_SQ_MULT_PIN, AF4); // TIM22_CH1
   
   // Enable peripheral
   RCC_ENABLE_APB2_PERIPHERAL(TIM22_APB2);
   
   // Detect rising edges of TI1 (CC1S='b01)
   TIM22->CCMR1 &= ~( 0x3);
   TIM22->CCMR1 |=      1;
   
   // Configure the input filter (IC1F='b0000)
   TIM22->CCMR1 &= ~(0xf<<4); // No filter
   TIM22->CCMR1 |=     1<<4;
   
   // Rising edge polarity (CC1P=0 CC1NP=0)
   TIM22->CCER  &= ~(1<<1);
   TIM22->CCER  &= ~(1<<3);
   
   // External clock mode (SMS='b111)
   TIM22->SMCR  |=  0x7;
   
   // Select TI1 as input source (TS='b101)
   TIM22->SMCR  &= ~(0x7<<4);
   TIM22->SMCR  |=   0x5<<4;
   
   // Initalize
   TIM22->CNT=0;
   
   // Enable timer (CEN=1)
   TIM22->CR1   |=  0x01;
   
   //aux=TIM22->CCMR1; dbg_print("TIM22 CCMR1: 0x"); print16(&aux,1); dbg_print("\n\r");
   //aux=TIM22->CCER;  dbg_print("TIM22 CCER:  0x"); print16(&aux,1); dbg_print("\n\r");
   //aux=TIM22->SMCR;  dbg_print("TIM22 SMCR:  0x"); print16(&aux,1); dbg_print("\n\r");
   //aux=TIM22->CR1;   dbg_print("TIM22 CR1:   0x"); print16(&aux,1); dbg_print("\n\r");
}

void soil_timer_deinit (void) {
   
   // Disable timer (CEN=1)
   TIM22->CR1   &= ~1UL;
   TIM22->CNT=0;

   RCC_DISABLE_APB2_PERIPHERAL(TIM22_APB2);

}



void soil_proc_alg3(soil_result* Result) {
   analog_result     measuredADC;
   uint16_t          cntVal;
   
   // a) Initialization
   // Done in soil_init
   
   // b) Procedure
   // Enable Comparator
   GPIO_CLEAR (      SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
   delay_ms(10);         // wait for comparator stabilization
   // Config and start count
   soil_timer_init();   // timer used as rising-edge counter
   delay_ms(50);         // Interval to count
   // count end
   cntVal=TIM22->CNT;   // count value
   // Disable opamp
   GPIO_SET   (  SOIL_EN_MULT_PORT, SOIL_EN_MULT_PIN);
   soil_timer_deinit();
   
   // c) Store result
   Result->ALG_FREQ = cntVal;  // raw counts in 5mseg

#if (DEBUG_PRINT>=2)   
   dbg_print("soil_proc_alg3 info: cnt=");
   print_unsBCD(Result->ALG_FREQ,0); dbg_print("\r\n");
#endif   
   
   // d) NTC Measure
   soil_adc_read(&measuredADC, 3);
   get_soil_temp(&measuredADC, Result);
}

void soil_procedure(soil_result* Result) {
   // --------------------------------
   // External wrapper for the different algorithms
   // --------------------------------

   // Initialization
   soil_init(Result->ALG_ID);
   
   // Procedures
   switch(Result->ALG_ID) {
    case 1:
      soil_proc_alg1( Result );
      break;
    case 2: // Does not work
      soil_proc_alg2( Result );
      break;
    case 3:
      soil_proc_alg3( Result );
      break;
    default:
      dbg_print("soil_procedure: bad ALG_ID\r\n");
   }
   
   // Deinitialization
   soil_deinit();
}
