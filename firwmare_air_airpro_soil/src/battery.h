#ifndef BATTERY_H
#define BATTERY_H

#include "stm32l0xx.h"

void battery_measure(uint8_t *);

uint16_t get_batt_voltage(void);


#endif

