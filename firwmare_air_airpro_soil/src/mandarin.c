//Date: 2017-01-25
//Author: Thimo Findeisen
//
//Comment:
//The Soil Sensor used has one Channel reserved for Water Content
//The other Channel can be either Temp or EC, depending on which Hardware version has been purchased

#include "mandarin.h"

#include "config.h"
#include "rcc.h"
#include "gpio.h"
#include "adc.h"

#include "print.h"
#include "delay.h"

void MandarinInit(){
#ifdef SENSOR_MANDARIN

//Init Vpp for Soil Sensor
   SET_GPIO_AS_OUTPUT(   SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);
   GPIO_CLEAR(           SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);

   RCC_ENABLE_APB2_PERIPHERAL(ADC_APB2);
  
   ADC_CLK_SOURCE(ADC1, CK_PCLK_2);

   ADC_CALIBRATION(ADC1);

   ADC_SET_OV(ADC1,OV_256);
   ADC_SET_AUTOFF(ADC1);
   ADC_SET_SINGLE_CONVERSION(ADC1);

//SET GPIOs to read Wc and EC/TEMP (not completely implemented)
   SET_GPIO_AS_INPUT(SOIL_ADC1_PORT, SOIL_ADC1_PIN);    //WC
   SET_GPIO_AS_INPUT(SOIL_ADC2_PORT, SOIL_ADC2_PIN);    //EC/TEMP not completely implemented
  
   ADC_SET_CHANNELS(ADC1, CH_0|CH_1);

#endif
}

void MandarinDeinit(){
#ifdef SENSOR_MANDARIN

//disable to avoid Power Waste
   GPIO_SET(SOIL_NTC_VDD_PORT, SOIL_NTC_VDD_PIN);

//disable ADC
   RCC_DISABLE_APB2_PERIPHERAL(ADC_APB2);

#endif
}

void GetMandarinWc(mandarin_analog_result* Result){
#ifdef SENSOR_MANDARIN

//get Analog value for Wc
   ADC_READ_DATA(ADC1, &(Result->CH1));

#endif
}

void GetMandarinEcTemp(mandarin_analog_result* Result){
#ifdef SENSOR_MANDARIN

//get Analog value for EC/Temp
   ADC_READ_DATA(ADC1, &(Result->CH2));

#endif
}

void MandarinRead(mandarin_result* Result){
#ifdef SENSOR_MANDARIN
  mandarin_analog_result   measuredMandarin;
  
  MandarinInit();
  delay_ms(500);

  ADC_START(ADC1);

//get ADC values
	GetMandarinWc(&measuredMandarin);
	GetMandarinEcTemp(&measuredMandarin);

//get the values read by Soil Sensor
   Result->mandarin_wc = measuredMandarin.CH1;
   Result->mandarin_ec_temp = measuredMandarin.CH2;

  MandarinDeinit();
#endif
}
