#ifndef NODE_H
#define NODE_H

#include "stm32l0xx.h"
#include "config.h"

/*
 * Measurement message
 * Sent from the remote nodes to the central node (address 0x20)
 * Length: 48 bytes
 */
typedef struct {
   //ID
   uint16_t            nodeID;     // ID of sending node
   uint16_t            msg_type;   // Packet type, 0x0001 = measurement packet v1
   uint16_t            centralID;  // Central ID paired
   
   //Sensors
   int16_t             Temp;    // 100 * Temperature [deg Celcius] (-40 - 120) (for SHT2x)
   uint16_t            PAR;     //   1 * Photo Active Radiation [umol/m2/s] (0 - 2000) (at tropical midday)
   uint16_t            IR;      //   
   uint16_t            Humid;   //  10 * Relative Humidity [%] (0 - 100)
   int16_t             Tleaf;   // 100 * Leaf Temperature [deg Celcius] (-70 - 380) (for mlx90614)
   uint16_t            SoilHum; //   ? * Soil Humidity [?]
   uint16_t            SoilEC;  //   ? * Soil Electrical Conduction? [?]
   uint16_t            SoilTemp;  //   ? * Soil Temperature
	 uint16_t            WatermelonWc; //   ? * Soil Humidity [?]
   uint16_t            WatermelonEC;  //   ? * Soil Electrical Conduction? [?]
   uint16_t            WatermelonTemp;  //   ? * Soil Temperature
   uint16_t            CO2;     //CO2 value  -Thimo
   
   //Parameters
   uint16_t            batt;    // -2 + 0.2 * Battery voltage [V]
   int16_t             signal;  //   ? * Signal Strength [?]
   uint16_t            tsample; //       Sampling Time [s]
   
   uint16_t            errorCheck;
   
   uint16_t	       debug_field[18]; 
   uint16_t	       debug_field_length; 
   
} node_info;


#endif
