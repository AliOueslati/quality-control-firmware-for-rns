#ifndef HEXUTIL_H
#define HEXUTIL_H

#include "stm32l0xx.h"

void byte_to_hexasciichars(uint8_t, uint8_t *);
uint8_t hex2bcd (uint8_t x);
uint8_t bcd2hex (uint8_t x);

#endif
