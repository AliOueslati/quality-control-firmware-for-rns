#ifndef _LORA_H_
#define _LORA_H_
#include "stm32l0xx.h"
#include "config.h"

// ---------------------------------------- 
// Registers Mapping
#define RegFifo                                    0x00 // common
#define RegOpMode                                  0x01 // common
#define RegFrfMsb                                  0x06 // common
#define RegFrfMid                                  0x07 // common
#define RegFrfLsb                                  0x08 // common

#define RegPaConfig                                0x09 // common
#define RegPaRamp                                  0x0A // common
#define RegOcp                                     0x0B // common
#define RegLna                                     0x0C // common
#define LORARegFifoAddrPtr                         0x0D
#define LORARegFifoTxBaseAddr                      0x0E
#define LORARegFifoRxBaseAddr                      0x0F 
#define LORARegFifoRxCurrentAddr                   0x10
#define LORARegIrqFlagsMask                        0x11 
#define LORARegIrqFlags                            0x12 
#define LORARegRxNbBytes                           0x13 
#define LORARegRxHeaderCntValueMsb                 0x14 
#define LORARegRxHeaderCntValueLsb                 0x15 
#define LORARegRxPacketCntValueMsb                 0x16 
#define LORARegRxpacketCntValueLsb                 0x17 
#define LORARegModemStat                           0x18 
#define LORARegPktSnrValue                         0x19 
#define LORARegPktRssiValue                        0x1A 
#define LORARegRssiValue                           0x1B 
#define LORARegHopChannel                          0x1C 
#define LORARegModemConfig1                        0x1D 
#define LORARegModemConfig2                        0x1E 
#define LORARegSymbTimeoutLsb                      0x1F 
#define LORARegPreambleMsb                         0x20 
#define LORARegPreambleLsb                         0x21 
#define LORARegPayloadLength                       0x22 
#define LORARegPayloadMaxLength                    0x23 
#define LORARegHopPeriod                           0x24 
#define LORARegFifoRxByteAddr                      0x25
#define LORARegModemConfig3                        0x26
#define LORARegFeiMsb                              0x28
#define LORAFeiMib                                 0x29
#define LORARegFeiLsb                              0x2A
#define LORARegRssiWideband                        0x2C
#define LORARegDetectOptimize                      0x31
#define LORARegInvertIQ                            0x33
#define LORARegDetectionThreshold                  0x37
#define LORARegSyncWord                            0x39

#define RegDioMapping1                             0x40 // common
#define RegDioMapping2                             0x41 // common
#define RegVersion                                 0x42 // common
#define RegPaDac                                   0x4D // common

// preamble for lora networks (nibbles swapped)
#define LORA_MAC_PREAMBLE                          0x34


// ---------------------------------------- 
// Constants for radio registers
#define OPMODE_LORA          0x80
#define OPMODE_MASK          0x07
#define OPMODE_SLEEP         0x00
#define OPMODE_STANDBY       0x01
#define OPMODE_FSTX          0x02
#define OPMODE_TX            0x03
#define OPMODE_FSRX          0x04
#define OPMODE_RX            0x05
#define OPMODE_RX_SINGLE     0x06 
#define OPMODE_CAD       		 0x07 

// ----------------------------------------
// Bits masking the corresponding IRQs from the radio
#define IRQ_LORA_RXTOUT_MASK 0x80
#define IRQ_LORA_RXDONE_MASK 0x40
#define IRQ_LORA_CRCERR_MASK 0x20
#define IRQ_LORA_HEADER_MASK 0x10
#define IRQ_LORA_TXDONE_MASK 0x08
#define IRQ_LORA_CDDONE_MASK 0x04
#define IRQ_LORA_FHSSCH_MASK 0x02
#define IRQ_LORA_CDDETD_MASK 0x01


// ----------------------------------------
// DIO function mappings                D0D1D2D3
#define MAP_DIO0_LORA_RXDONE   0x00  // 00------
#define MAP_DIO0_LORA_TXDONE   0x40  // 01------
#define MAP_DIO1_LORA_RXTOUT   0x00  // --00----
#define MAP_DIO1_LORA_NOP      0x30  // --11----
#define MAP_DIO2_LORA_NOP      0xC0  // ----11--

// LNA settings
#define LNA_RX_GAIN            0x20

//RegModemConfig1
#define MC1_BW_125                     						 0x70
#define MC1_BW_250                                 0x80
#define MC1_BW_500                                 0x90
#define MC1_CR_4_5                                 0x02
#define MC1_CR_4_6                                 0x04
#define MC1_CR_4_7                                 0x06
#define MC1_CR_4_8                                 0x08
                                                    
//RegModemConfig2          
#define MC2_SF7                                    0x70
#define MC2_SF8                                    0x80
#define MC2_SF9                                    0x90
#define MC2_SF10                                   0xA0
#define MC2_SF11                                   0xB0
#define MC2_SF12                                   0xC0
#define MC2_RX_PAYLOAD_CRCON                       0x04

//RegModemConfig3          
#define MC3_LOW_DATA_RATE_OPTIMIZE                 0x08
#define MC3_AGCAUTO                                0x04


typedef struct {
   uint8_t SF;
   uint8_t BW;
   uint8_t TX_POWER;
   uint8_t CR;
   uint32_t FREQ;	
} LORAConfig;

typedef struct {
   uint8_t DATA[PAYLOAD_SIZE];
   uint8_t ADDRESS;
   uint8_t LENGTH;
   uint8_t RSSI;
   uint8_t SNR;
   uint8_t VALID;
   uint8_t dummy1;
   uint8_t dummy2;
} LORAData;

void lora_init(void);
void writeReg (uint8_t, uint8_t);
uint8_t readReg (uint8_t);
void writeBuf (uint8_t, uint8_t *, uint8_t); 
void readBuf (uint8_t, uint8_t *, uint8_t);
uint8_t ui8_CRCGen(uint8_t * message, uint8_t length);
void opmode (uint8_t);
void opmodeLora(void);
void radio_start_tx (LORAData *Payload, uint8_t Address);
void radio_start_rx_cont (void);
void radio_start_sleep (void);
void radio_start_rx_single (void);
void radio_receive (LORAData *); 
void radio_init_tx (LORAConfig *);
void radio_init_rx (LORAConfig *);


void DebugLoraCrc(void);
//void DebugNtc(void);
//void DebugMsgType(void);

// DEBUG COUNTERS
#ifdef LORA_DBG_CNT
typedef struct {
   uint16_t       LoraRx;    // when receiving a MSG
   uint16_t       LoraTx;    // when sending ACK
   uint16_t       FwCrcOk;    // fw-CRC is okay
   uint16_t       FwCrcKo;    // fw-CRC is NOT okay
   uint16_t       LoraCrcOk;    // LoraCRC is okay
   uint16_t       LoraCrcKo;    // LoraCRC is NOT okay
   uint16_t       AdrOk;    // msg for CN
   uint16_t       AdrKo;    // msg NOT for CN
   uint16_t       ValidOk;    // MSG is valid
   uint16_t       ValidKo;    // MSG is NOT valid
} lora_dbg_cnt;

#endif

#endif 
