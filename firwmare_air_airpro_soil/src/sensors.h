#ifndef SENSORS_H
#define SENSORS_H

#include "stm32l0xx.h"
#include "node.h"

void sensors_init(void);
void sensors_read(node_info *);
uint8_t compose_msg (uint16_t *I, node_info *);


#endif
