#ifndef TIMER_H
#define TIMER_H

#include "stm32l0xx.h"


void ENABLE_TIMER(TIM_TypeDef *);
void DISABLE_TIMER(TIM_TypeDef *);
void SET_TIMER_PRESCALER(TIM_TypeDef *,uint16_t);
void UPDATE_TIMER_REGISTER(TIM_TypeDef *);
void SET_TIMER_CMP_REG1(TIM_TypeDef *,uint16_t);
void SET_TIMER_CMP_REG1_INTERRUPT_ENABLE(TIM_TypeDef *);
void SET_TIMER_CMP_REG1_INTERRUPT_DISABLE(TIM_TypeDef *);
void SET_TIMER_OV_INTERRUPT_ENABLE(TIM_TypeDef *);
void SET_TIMER_OV_INTERRUPT_DISABLE(TIM_TypeDef *);

void SET_TIMER_PWM_MODE(TIM_TypeDef *);
void SET_TIMER_PWM_MODE_mod(TIM_TypeDef *, uint16_t );
	
#endif
