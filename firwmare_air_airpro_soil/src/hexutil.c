#include "stm32l0xx.h"

/** HEX_ASCII Table: Table for HEX ASCII conversion */
static uint8_t HEX_ASCII[]="0123456789ABCDEF";

/**
 * Converts one byte to two ascii hexadecimal characters.
 *
 * \param[in]  Byte The byte.
 * \param[out] Buf  The two ascii characters. The buffer must be already 
 *                  reserved with at least 2 chars.
 * \return  Void.
 */
void byte_to_hexasciichars(uint8_t Byte, uint8_t *Buf)
{
  uint8_t nibble;
 
  nibble = (Byte>>4)&0x0F;
  Buf[0] = HEX_ASCII[nibble];
  
  nibble=Byte&0x0F;
  Buf[1] = HEX_ASCII[nibble];
  
}


uint8_t hex2bcd (uint8_t x)
{
    uint8_t y;
    y = (x / 10) << 4;
    y = y | (x % 10);
    return (y);
}

uint8_t bcd2hex (uint8_t x)
{
    uint8_t y;
    y = ((x>>4)*10)+(x&0x0F);
    return (y);
}
