#ifndef TSL2561_H
#define TSL2561_H

#include "stm32l0xx.h" 

/*
 * TSL2561 Configurations
 */
#define TIMING_GAIN_1X                0x00
#define TIMING_GAIN_16X               0x10
#define TIMING_INTG_TIME_13MS         0x00
#define TIMING_INTG_TIME_101MS        0x01
#define TIMING_INTG_TIME_402MS        0x02
#define TIMING_INTG_TIME_MANUAL       0x03
#define TIMING_MANUAL_START           0x08 // Only has meaning when integration time on manual mode
#define TIMING_MANUAL_STOPT           0x00 // Only has meaning when integration time on manual mode


typedef struct{
	uint16_t VISIBLE_IR;
	uint16_t VISIBLE;
	uint16_t IR;
} tsl2561_result;

uint8_t tsl2561_power_on(void);
uint8_t tsl2561_power_off(void);

uint8_t tsl2561_get_light(uint16_t *);
uint8_t tsl2561_get_ir(uint16_t *);

uint8_t tsl2561_set_timing(uint8_t);

uint8_t tsl2561_read(tsl2561_result *);

#endif

