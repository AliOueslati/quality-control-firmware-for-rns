#include "config.h"
#include "gpio.h"
#include "led.h"
#include "rcc.h"

#ifndef GPIO_LED_ON
#define GPIO_LED_ON  GPIO_SET
#endif
#ifndef GPIO_LED_OFF
#define GPIO_LED_OFF GPIO_CLEAR
#endif

void led_init(void) {
	GPIO_LED_OFF(LED1_PORT, LED1_PIN);
	SET_GPIO_AS_OUTPUT(LED1_PORT, LED1_PIN);

	GPIO_LED_OFF(LED2_PORT, LED2_PIN);
	SET_GPIO_AS_OUTPUT(LED2_PORT, LED2_PIN);

#ifdef LED3_PORT
	GPIO_LED_OFF(LED3_PORT, LED3_PIN);
	SET_GPIO_AS_OUTPUT(LED3_PORT, LED3_PIN);
#endif
}

void led_off(uint8_t led){
	switch (led)
	{
		case 1:
			GPIO_LED_OFF(LED1_PORT, LED1_PIN);
			break;
		case 2:
			GPIO_LED_OFF(LED2_PORT, LED2_PIN);
		  break;
#ifdef LED3_PORT
		case 3:
			GPIO_LED_OFF(LED3_PORT, LED3_PIN);
			break;
#endif
	}
}

void led_on(uint8_t led){
	switch (led)
	{
		case 1:
			GPIO_LED_ON(LED1_PORT, LED1_PIN);
			break;
		case 2:
			GPIO_LED_ON(LED2_PORT, LED2_PIN);
		  break;
#ifdef LED3_PORT
		case 3:
			GPIO_LED_ON(LED3_PORT, LED3_PIN);
			break;
#endif
	}
}

void led_toggle(uint8_t led){
	switch (led)
	{
		case 1:
			GPIO_TOGGLE(LED1_PORT, LED1_PIN);
			break;
		case 2:
			GPIO_TOGGLE(LED2_PORT, LED2_PIN);
		  break;
#ifdef LED3_PORT
		case 3:
			GPIO_TOGGLE(LED3_PORT, LED3_PIN);
			break;
#endif
	}
}
