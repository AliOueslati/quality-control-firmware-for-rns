#ifndef BUTTON_H
#define BUTTON_H

#include "stm32l0xx.h"

void button_init(void);
uint8_t button_get_status(void);
void button_mask_irq(void);
void button_unmask_irq(void);

#endif
