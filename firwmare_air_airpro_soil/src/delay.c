#include "stm32l0xx.h"
#include "delay.h"
#include "timer.h"
#include "rcc.h"
#include "print.h"

//#define DELAY_TIMER  2
#define DELAY_TIMER  6

void delay_init(void){
#if (DELAY_TIMER==2)
   #define TIMER_CNT TIM2->CNT
   
   RCC_ENABLE_APB1_PERIPHERAL(TIM2_APB1); 
   SET_TIMER_PRESCALER(TIM2,(uint16_t)(((SystemCoreClock/1000000))-1)); // 1 MHZ Timer
   UPDATE_TIMER_REGISTER(TIM2);
   ENABLE_TIMER(TIM2);
   
#elif (DELAY_TIMER==6)
   #define TIMER_CNT TIM6->CNT
   
   RCC_ENABLE_APB1_PERIPHERAL(TIM6_APB1);
   // As TIM6 is shared with timeout, delete its bits...
   TIM6->CR1 &= ~0x0c;
   SET_TIMER_PRESCALER(TIM6,(uint16_t)(((SystemCoreClock/1000000))-1)); // 1 MHZ Timer
   UPDATE_TIMER_REGISTER(TIM6);
   ENABLE_TIMER(TIM6);
     {
	//uint16_t aux;
	//aux=TIM6->CR1;
	//dbg_print("delay  TIM6-CR:"); print16(&aux,1); dbg_print("\n\r");
     }
   
#else
   #error "Timer unknown"
#endif   
}

void delay_deinit(void){
#if (DELAY_TIMER==2)
   DISABLE_TIMER(TIM2);
   RCC_DISABLE_APB1_PERIPHERAL(TIM2_APB1);
#elif (DELAY_TIMER==6)
   DISABLE_TIMER(TIM6);
   RCC_DISABLE_APB1_PERIPHERAL(TIM6_APB1);
#else
   #error "Timer unknown"
#endif   
}


void delay_us(uint16_t TIME_US){
   uint16_t time;
   
   time=TIME_US+(TIME_US>>5)+(TIME_US>>6); //time compensation
   TIMER_CNT=0;
   while((uint16_t)(TIMER_CNT) <= time);		
}


void delay_sec(uint8_t TIME_SEC){
   uint16_t i;
   
   i=TIME_SEC;
   while (i) {
      delay_ms(1000);
      i--;
   }	
}

void delay_ms(uint16_t TIME_MS){
   uint16_t i;
   
   i=TIME_MS;
   while (i) {
      delay_us(1000);
      i--;
   }		
}


