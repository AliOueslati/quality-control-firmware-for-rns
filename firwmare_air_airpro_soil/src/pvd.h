#ifndef PVD_H
#define PVD_H

#include "stm32l0xx.h"

void PVD_ENABLE(void);

void PVD_DISABLE(void);

void PVD_SET_THRESHOLD(uint8_t); 

void PVD_GET_STATUS(uint8_t *); 

#endif

