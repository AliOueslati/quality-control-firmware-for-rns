#ifndef SPI_H
#define SPI_H

#include "stm32l0xx.h"

#define PCLK_2   0x00
#define PCLK_4   0x01
#define PCLK_8   0x02
#define PCLK_16  0x03
#define PCLK_32  0x04
#define PCLK_64  0x05
#define PCLK_128 0x06
#define PCLK_256 0x07

void SPI_DISABLE(SPI_TypeDef *);
void SPI_ENABLE(SPI_TypeDef *);
void SPI_SET_UNIDIRECTIONAL_MODE(SPI_TypeDef *);
void SPI_SET_DFF_8BITS(SPI_TypeDef *);
void SPI_SET_FULL_DUPLEX_MODE(SPI_TypeDef *);
void SPI_SEND_MSB_FIRST(SPI_TypeDef *);
void SPI_ENABLE_SOFTWARE_SLAVE_MANAGEMENT(SPI_TypeDef *);
void SPI_SET_NSS_HIGH(SPI_TypeDef *);
void SPI_SET_BAUDRATE_PRESCALER(SPI_TypeDef *, uint8_t);
void SPI_SET_MASTER_MODE(SPI_TypeDef *);
void SPI_SET_CLOCK_POLARITY_0(SPI_TypeDef *);
void SPI_SET_CLOCK_PHASE_0(SPI_TypeDef *);
void SPI_ENABLE_SS_OUTPUT(SPI_TypeDef *);
uint8_t SPI_WRITE_8(SPI_TypeDef *, uint8_t);
uint8_t SPI_WRITE_16(SPI_TypeDef *, uint16_t);
uint8_t SPI_XFER_8(SPI_TypeDef *, uint8_t, uint8_t *);
uint8_t SPI_XFER_16(SPI_TypeDef *, uint16_t, uint16_t *);

#endif
