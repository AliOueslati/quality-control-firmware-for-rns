#include "stm32l0xx.h"
#include "config.h"
#include "rcc.h"
#include "gpio.h"


/////////* POINTER FUNCTIONS *////////////
void (*button_on_press)(void);


void button_init(void)
{
	SET_GPIO_AS_INPUT(BUTTON_PORT, BUTTON_PIN);

	RCC_ENABLE_APB2_PERIPHERAL(SYSCFG_APB2);

	// Select GPIOA as source for EXT_[BUTTON_PIN]
	// TODO: allow other ports than GPIOA
	SYSCFG->EXTICR[BUTTON_PIN/4] = ( SYSCFG->EXTICR[BUTTON_PIN/4]
      & ~(0x0F << (BUTTON_PIN & 0x03)*4) )
      |   0x00 << (BUTTON_PIN & 0x03)*4;    //0x00 for PORTA

	EXTI->IMR |= (1UL<<BUTTON_PIN);  //Unmask EXT_[BUTTON_PIN]
	EXTI->FTSR |= (1UL<<BUTTON_PIN); //Falling edge

#if BUTTON_PIN >= 4
	NVIC_SetPriority(EXTI4_15_IRQn,1);
	NVIC_EnableIRQ(EXTI4_15_IRQn);
#else
#error BUTTON_PIN < 4 not implemented
#endif

}

uint8_t button_get_status(void){
	return !GPIO_GET_INPUT(BUTTON_PORT, BUTTON_PIN);
}

void button_mask_irq(void){
	EXTI->IMR &= ~(1UL<<BUTTON_PIN);
}

void button_unmask_irq(void){
	EXTI->IMR |= (1UL<<BUTTON_PIN);
}
