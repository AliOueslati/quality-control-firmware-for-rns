#ifndef VERSION_H
#define VERSION_H

// git version, generated in makefile with 'git --no-pager describe --tags --always --dirty'
// if not defined use a placeholder
#ifndef VERSION
  #define VERSION "Release 1"
#endif
// git version date, generated with 'git --no-pager show --date=short --format="%ad" --name-only'
#ifndef VERSION_DATE
  #define VERSION_DATE ""
#endif

#endif
