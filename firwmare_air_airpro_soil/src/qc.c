#include "stm32l0xx.h"
#include "tsl2561.h"  // PAR (Photo Active Radiation)
#include "SHT2x.h"    // temperature + humidity
#include "config.h"
#include "sensors.h"
#include "rcc.h"
#include "gpio.h"
#include "delay.h"
#include "node.h"
#include "soil.h"
#include "eeprom.h"
#include "calibration.h"
#include "battery.h"
#include "button.h"
#include "hx711.h"
#include "print.h"
#include "led.h"
// Use green LED when no blue available
#ifndef LED_BLUE
#define LED_BLUE LED_GREEN
#endif

void qc_init(void){
   
   //Enable I2C1 and port config
   SET_GPIO_OPEN_DRAIN(I2C_SCL_PORT, I2C_SCL_PIN);
   SET_GPIO_OPEN_DRAIN(I2C_SDA_PORT, I2C_SDA_PIN);
   
   SET_GPIO_SPEED_HIGH(I2C_SCL_PORT, I2C_SCL_PIN);
   SET_GPIO_SPEED_HIGH(I2C_SDA_PORT, I2C_SDA_PIN);
   
   SET_GPIO_AS_AF(I2C_SCL_PORT, I2C_SCL_PIN,AF1);
   SET_GPIO_AS_AF(I2C_SDA_PORT, I2C_SDA_PIN,AF1);
   
   RCC_ENABLE_APB1_PERIPHERAL(I2C1_APB1);
   
}

void qc_read(node_info *nodeInfo){
	   uint8_t batt;
   uint16_t vBatt;
   
   uint8_t error=0;
   sht2x_result   sht2x;
   tsl2561_result tsl2561;
   nodeInfo->errorCheck=0;
   

	//batt verif//
	 dbg_print("\r\n");
	 	dbg_print("//=======Battery verfication======//");
		dbg_print("\r\n");
	
	   // Measure batt
   
   battery_measure(&batt);
	 dbg_print("battery level="); print_unsBCD(batt,0); dbg_print("\n\r");
   nodeInfo->batt=(uint16_t)batt;
   
   // Measure the batteries voltage
   vBatt=get_batt_voltage();
	 nodeInfo->batt = vBatt;
   
   //check the battery level is good for sending.
   if(batt < VBAT_THRESHOLD_TX)
	 {
		 		 //hardware check
		     // error blink RED ; 1 time for battery
		 dbg_print("\r\n");	
    dbg_print("Too low level");
		 dbg_print("\r\n");	
	 led_on(LED_RED);
		 delay_ms(500);
		 led_off(LED_RED);
		 delay_ms(3000);
	 }
   else{
		 //success Blink Blue 1time
		 dbg_print("\r\n");	
		    dbg_print("vBatt="); print_unsBCD(vBatt,0); dbg_print("\n\r");
		 led_on(LED_BLUE);
		 delay_ms(500);
		 led_off(LED_BLUE);
		 delay_ms(3000);
		 
	 }

	dbg_print("\r\n");
	 	dbg_print("//======END Battery verfication======//");
		dbg_print("\r\n");   
   /*
    * Humidity + Temperature
    */
#ifdef SENSOR_HUMIDITY_SHT2X
   error  = SHT2x_read(&sht2x);
	 nodeInfo->errorCheck|=(error&0x03)<<2;
	 dbg_print("\r\n");	
		dbg_print("//=====SHT2X verification=====//");
		dbg_print("\r\n");
	 
   if (error) { 
		 //hardware check
		 //Error blinking Red 2 times for SHT2X
		 led_on(LED_RED);
		 delay_ms(300);
		 led_off(LED_RED);
		 delay_ms(300);
		 led_on(LED_RED);
		 delay_ms(300);
		 led_off(LED_RED); 
		 delay_ms(3000);
		 // print message via serial
		 dbg_print("\r\n");	
		 dbg_print("sht2x Reading error");
		 dbg_print("\r\n");

   } else {
		 // print message via serial
		 dbg_print("\r\n");	
		 dbg_print("sht2x Reading success");
		 dbg_print("\r\n");
	   nodeInfo->Humid = (uint16_t)(sht2x.HUMIDITY);
		 dbg_print("HUMIDITY: "); print_BCD(nodeInfo->Humid, 0); 
		 dbg_print("\r\n");
     nodeInfo->Temp = (int16_t)(sht2x.TEMPERATURE);
		 dbg_print("Temp: "); print_BCD(nodeInfo->Temp, 0); 
		 dbg_print("\r\n");
		 // hardware check
		 //success blinking blue (2 times)
		 led_on(LED_BLUE);
		 delay_ms(300);
		 led_off(LED_BLUE);
		 delay_ms(300);
		 led_on(LED_BLUE);
		 delay_ms(300);
		 led_off(LED_BLUE);
		 delay_ms(3000);
   }
	 dbg_print("\r\n");	
	  dbg_print("//=====End sht2x verification======//");
	  dbg_print("\r\n");
#endif
   
   
   /*
    * PAR
    */
#ifdef SENSOR_PAR_TSL2561
   // takes 100-400 ms
   error  = tsl2561_read(&tsl2561);
   nodeInfo->errorCheck|=(error&0x01)<<4;
	  dbg_print("\r\n");
		dbg_print("//=====TSL2561 verification===//");
		dbg_print("\r\n");
	 
   if (error) { 
		 //hardware check
		 //Error blinking Red (3 times)
		 led_on(LED_RED);
		 delay_ms(500);
		 led_off(LED_RED);
		 delay_ms(500);
		 led_on(LED_RED);
		 delay_ms(500);
		 led_off(LED_RED); 
		 delay_ms(500);
		 led_on(LED_RED);
		 delay_ms(500);
		 led_off(LED_RED);
		 delay_ms(3000);
		 // print message via serial
		 dbg_print("\r\n");	
		 dbg_print(" TSL2561 Reading error");
		 dbg_print("\r\n");	

   } else {
		 // print message via serial
		 dbg_print("\r\n");	
		 dbg_print(" TSL2561 Reading success");
      float PAR = 1.45 * (float)tsl2561.VISIBLE_IR - 2.15 * (float)tsl2561.IR;
      nodeInfo->PAR = (int16_t)PAR;
		 dbg_print("\r\n");
			dbg_print("PAR: "); print_BCD(nodeInfo->PAR, 0); dbg_print("\r\n");
			nodeInfo->IR =  (int16_t)tsl2561.IR;
			dbg_print("IAR: "); print_BCD(nodeInfo->IR, 0); dbg_print("\r\n");
		 // hardware check
		 //success blinking blue (3times)
		 led_on(LED_BLUE);
		 delay_ms(500);
		 led_off(LED_BLUE);
		 delay_ms(500);
		 led_on(LED_BLUE);
		 delay_ms(500);
		 led_off(LED_BLUE);
		 delay_ms(500);
		 led_on(LED_BLUE);
		 delay_ms(500);
		 led_off(LED_BLUE);
		 delay_ms(3000);
   }
	 dbg_print("\r\n");	
	  dbg_print("//======END TSL2561 verification==//");
	  dbg_print("\r\n");

#endif   

}

