#include "stm32l0xx.h"


/////////* PORT/PIN CONFIGURATION *////////////

/* INPUT - OUTPUT - AF - ANALOG*/
void SET_GPIO_AS_INPUT(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->MODER  &= ~(1UL<<((PIN<<1)+1));
	  PORT->MODER  &= ~(1UL<<(PIN<<1));
}

void SET_GPIO_AS_OUTPUT(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->MODER  &= ~(1UL<<((PIN<<1)+1));
	  PORT->MODER  |= 1UL<<(PIN<<1);
}

void SET_GPIO_AS_AF(GPIO_TypeDef *PORT, uint8_t PIN, uint8_t AF) {
		PORT->MODER  |= 1UL<<((PIN<<1)+1);
	  PORT->MODER  &= ~(1UL<<(PIN<<1));

		if (PIN<=7)
		{
			PORT->AFR[0]  &= ~(0xF<<(PIN<<2));
			PORT->AFR[0]  |= AF<<(PIN<<2);
		}
		else
		{
			PIN=PIN-8;
			PORT->AFR[1]  &= ~(0xF<<(PIN<<2));
			PORT->AFR[1]  |= AF<<(PIN<<2);
		}
}

void SET_GPIO_AS_ANALOG(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->MODER  |= 1UL<<((PIN<<1)+1);
	  PORT->MODER  |= 1UL<<(PIN<<1);
}


/* PULL UP - PULL DOWN*/
void SET_GPIO_PULL_UP(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->PUPDR  &= ~(1UL<<((PIN<<1)+1));
	  PORT->PUPDR  |= 1UL<<(PIN<<1);
}

void SET_GPIO_PULL_DOWN(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->PUPDR  |= 1UL<<((PIN<<1)+1);
	  PORT->PUPDR  &= ~(1UL<<(PIN<<1));
}

void SET_GPIO_NO_PULL(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->PUPDR  &= ~(1UL<<((PIN<<1)+1));
	  PORT->PUPDR  &= ~(1UL<<(PIN<<1));
}

void SET_GPIO_OPEN_DRAIN(GPIO_TypeDef *PORT, uint8_t PIN) {
	  PORT->OTYPER  |= 1UL<<(PIN);
}

/* SPEED */
void SET_GPIO_SPEED_LOW(GPIO_TypeDef *PORT, uint8_t PIN) {
	 PORT->OSPEEDR  &= ~(1UL<<(PIN<<1));
}

void SET_GPIO_SPEED_MEDIUM(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->OSPEEDR  &= ~(1UL<<((PIN<<1)+1));
	  PORT->OSPEEDR  |= 1UL<<(PIN<<1);
}

void SET_GPIO_SPEED_HIGH(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->OSPEEDR  |= 1UL<<((PIN<<1)+1);
	  PORT->OSPEEDR  |= 1UL<<(PIN<<1);
}


/////////* PORT/PIN ASSIGNMENT *////////////

void GPIO_CLEAR(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->BSRR  |= (1UL<<PIN)<<16;
}

void GPIO_SET(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->BSRR  |= 1UL<<PIN;
}

void GPIO_TOGGLE(GPIO_TypeDef *PORT, uint8_t PIN) {
		PORT->ODR  ^= 1UL<<PIN;
}

uint8_t GPIO_GET_INPUT(GPIO_TypeDef *PORT, uint8_t PIN) {
	uint8_t value;
	value=(uint8_t)((PORT->IDR)>>PIN)&0x1;
	return value;
}
