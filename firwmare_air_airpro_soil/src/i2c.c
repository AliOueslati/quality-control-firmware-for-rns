#include "stm32l0xx.h"
#include "i2c.h"

#define TIMEOUT_ERROR 0x1FF

void I2C_DISABLE(I2C_TypeDef *I2C){
	I2C->CR1    &= ~0x01;          // Disable I2C
}

void I2C_ENABLE(I2C_TypeDef *I2C, uint32_t MODE, uint8_t SMBus){
	I2C->CR1    &= ~0x01;          // Disable I2C
	I2C->TIMINGR  = MODE;
	//I2C->TIMEOUTR = 0x00001060;	//  test with internal timeout
	//I2C->TIMEOUTR |= 0x00008000; // enable timeout
	I2C->CR1    |= ((SMBus&0x01)<<23);          // SMBus PECEN
	I2C->CR1    |= 0x01;          // Enable I2C
}

void I2C_SET_NBYTES(I2C_TypeDef *I2C, uint8_t BYTES){
  I2C->CR2 &=~(0xFF<<16);
	I2C->CR2 |= (BYTES<<16);
}

void I2C_SET_SADD_7BITS(I2C_TypeDef *I2C, uint8_t ADD){
	I2C->CR2 |=1<<12;       // Only send first 7 bits of Address
	I2C->CR2 &=~(1<<11);    // 7 bits Address Mode

	I2C->CR2 &=~0x3FF;
	I2C->CR2 |=(ADD<<1);    // Bits 1-7
}

void I2C_SET_READ(I2C_TypeDef *I2C){
	I2C->CR2 |=1<<10;
}

void I2C_SET_WRITE(I2C_TypeDef *I2C){
	I2C->CR2 &=~(1<<10);
}

uint8_t I2C_START(I2C_TypeDef *I2C){
	uint16_t timeOut=0;
	uint8_t error=0;

	I2C->CR2 |=1<<13;

	while ((I2C->CR2 & (1<<13))==1)
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}

	return error;
}

uint8_t I2C_STOP(I2C_TypeDef *I2C){
	uint16_t timeOut=0;
	uint8_t error=0;

	I2C->ICR |=1<<5;  //Clear STOP Flag

	I2C->CR2 |=1<<14;  //STOP condition

	while ((I2C->ISR & (1<<5))==0)
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}

	return error;
}

uint8_t I2C_WAIT_TC(I2C_TypeDef *I2C){
	uint8_t error=0;
	uint16_t timeOut=0;

	while ((I2C->ISR & (1<<6))==0)
	{
	 if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}		// Transmission complete

	return error;
}

uint8_t I2C_WRITE(I2C_TypeDef *I2C, uint8_t VALUE){
	uint8_t error=0;
	uint16_t timeOut=0;

	while ((I2C->ISR & (1<<1))==0)  // TXDR empty?
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}

	I2C->TXDR  = VALUE;

  return error;
}

uint8_t I2C_PECBYTE(I2C_TypeDef *I2C){
	uint8_t error=0;
	uint16_t timeOut=0;

	while ((I2C->ISR & (1<<1))==0)  // TXDR empty?
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}

  I2C1->CR2    |= 0x4000000;          // SMBus PECBYTE

  return error;
}

uint8_t I2C_READ(I2C_TypeDef *I2C, uint8_t * Result){
	uint8_t error=0;
	uint16_t timeOut=0;

	while ((I2C->ISR & (1<<2))==0)                      // RXDR new data?
	{
		if ((timeOut)++==TIMEOUT_ERROR){error=1; break;}
	}
	*Result=(I2C->RXDR)& 0x000000FF;

	return error;
}
