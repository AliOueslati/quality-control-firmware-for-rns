#ifndef WARTERMELON_H
#define WARTERMELON_H

#include "stm32l0xx.h"
#include "config.h"

typedef struct{

   uint16_t CH1;                // Wc analog channel
   uint16_t CH2;                // TEMP analog channel
   uint16_t CH8;                // EC analog channel
} watermelon_analog_result;

typedef struct{

   uint16_t watermelon_wc;        //WC from  Soil Sensor
   uint16_t watermelon_temp;       //Temp from  Soil Sensor
	 uint16_t watermelon_ec;        //EC from  Soil Sensor

} watermelon_result;

void WatermelonInit(void);
void WatermelonDeinit(void);
void GetWatermelonWc(watermelon_analog_result* Result);
void GetWatermelonTemp(watermelon_analog_result* Result);
void GetWatermelonEc(watermelon_analog_result* Result);
void WatermelonRead(watermelon_result* Result);

#endif
