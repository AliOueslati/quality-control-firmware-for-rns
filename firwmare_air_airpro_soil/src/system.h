#ifndef SYSTEM_H
#define SYSTEM_H

#include "stm32l0xx.h"

void system_sleep(void);
void system_deep_sleep(void);
void system_reset(void);
uint8_t system_range(uint8_t);

void change_voltage(uint8_t range);

#endif
