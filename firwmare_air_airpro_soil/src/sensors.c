// JLOMAS & JCHAVEZ 23 April update

#include "stm32l0xx.h"
#include "tsl2561.h"  // PAR (Photo Active Radiation)
#include "SHT2x.h"    // temperature + humidity
#include "config.h"
#include "sensors.h"
#include "rcc.h"
#include "gpio.h"
#include "delay.h"
#include "node.h"
#include "soil.h"
#include "eeprom.h"
#include "calibration.h"
#include "battery.h"
#include "button.h"
#include "hx711.h"
#include "print.h"
#include "mandarin.h"
#include "cozir.h"
#include "watermelon.h"

#define I2C I2C1

extern calibration_values  calibrationValues;
uint8_t Cozir_Humid_Counter = 0;

//float Vpp,Vd;
soil_result   measuredSoil;
mandarin_result   measuredMandarin;
watermelon_result measuredWatermelon;


void sensors_init(void){
   
   //Enable I2C1 and port config
   SET_GPIO_OPEN_DRAIN(I2C_SCL_PORT, I2C_SCL_PIN);
   SET_GPIO_OPEN_DRAIN(I2C_SDA_PORT, I2C_SDA_PIN);
   
   SET_GPIO_SPEED_HIGH(I2C_SCL_PORT, I2C_SCL_PIN);
   SET_GPIO_SPEED_HIGH(I2C_SDA_PORT, I2C_SDA_PIN);
   
   SET_GPIO_AS_AF(I2C_SCL_PORT, I2C_SCL_PIN,AF1);
   SET_GPIO_AS_AF(I2C_SDA_PORT, I2C_SDA_PIN,AF1);
   
   RCC_ENABLE_APB1_PERIPHERAL(I2C1_APB1);
   
}

void sensors_read(node_info *nodeInfo){
   uint8_t error=0;
   sht2x_result   sht2x;
   tsl2561_result tsl2561;
   
#ifdef SENSOR_CO2_COZIR
	cozir_result cozir;

	cozirEnable();
#endif

#ifdef HAS_HX711
   int16_t  hx711Value;
#endif

   nodeInfo->errorCheck=0;
   
   
   nodeInfo->Tleaf = 32000;
   
   /*
    * Humidity + Temperature
    */
#ifdef SENSOR_HUMIDITY_SHT2X
   error  = SHT2x_read(&sht2x);
   nodeInfo->Humid = (uint16_t)(sht2x.HUMIDITY);
   if(nodeInfo->Humid > 1000 && nodeInfo->Humid < 32000) nodeInfo->Humid = 1000; //Saturation Point and Error reading at 32000+
   
   nodeInfo->Temp = (int16_t)(sht2x.TEMPERATURE);
   nodeInfo->errorCheck|=(error&0x03)<<2;
#else
   nodeInfo->Temp = 32000;
   nodeInfo->Humid = 32000;
#endif
   
   
   /*
    * PAR
    */
#ifdef SENSOR_PAR_TSL2561
   // takes 100-400 ms
   error  = tsl2561_read(&tsl2561);
   nodeInfo->errorCheck|=(error&0x01)<<4;
   if (error) {
      nodeInfo->PAR = 32000 + error;
		  nodeInfo->IR = 32000 + error;
   } else {
      float PAR = 1.45 * (float)tsl2561.VISIBLE_IR - 2.15 * (float)tsl2561.IR;
      PAR *= calibrationValues.calib_par_a1;
		  if (PAR>64000) {
			 PAR=64000;
			}
			if (PAR<0) {
			 PAR=0;
			}
      nodeInfo->PAR = (int16_t)PAR;
		  nodeInfo->IR =  (int16_t)tsl2561.IR;
   }
#else
   nodeInfo->PAR = 32000;
#endif
   
   /*
    * Soil moisture 
    */
   //nodeInfo->SoilTemp=32000;
   //nodeInfo->SoilEC  =32000;
   

   
#ifdef SENSOR_SOIL_HUMIDITY
   // Store Algorithm: Harcoded for now...
   measuredSoil.ALG_ID= SOIL_ALG_ID;
   measuredSoil.ALG_FREQ=0; // Avoid rare values...
   
   // Perform Soil procedure
   soil_procedure(&measuredSoil);
   
   // Soil Temperature
   // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   //nodeInfo->SoilTemp=soil.NTC_TEMP;
   //nodeInfo->SoilTemp=(uint16_t)(-0.167*(measuredSoil.NTC_TEMP)+88);
   //nodeInfo->SoilTemp=(uint16_t)(-0.167*(measuredSoil.NTC_TEMP)+88);
   
   // Store NTC in soilTemp 
   // Calculations to be performed in parser_injector
   // JChavez hack: due to lack of NTC field in some messages
   nodeInfo->SoilTemp=measuredSoil.NTC_TEMP;

   // Soil Humidity
   // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   
   // Compute Wc
   //Vpp=measuredSoil.SCEN_VPP[0];

   // Calibration-------------------- linear against 0% vs 50% 
   //Wc(%)= -0.060*Vpp4+229           // (3817,0) & (2989,50) 
   //Wc(%)= -0.061*Vpp5+105           // (1728,0) &  (909,50)
   // using Vpp4
   //if (Vpp>3817)Vpp=3817;  // to force it to 0 % Wc min
   //if (Vpp<2000)Vpp=2000;  // to force it to 50% Wc max
   //nodeInfo->SoilHum=(uint16_t)(calibrationValues.calib_soil_humidity_a1*Vpp+calibrationValues.calib_soil_humidity_a0);
   //dbg_print("SoilHum calculated: ");   print_BCD(nodeInfo->SoilHum,0);
   //dbg_print("\n\r"); 
   //if(nodeInfo->SoilHum>100)
   //  nodeInfo->SoilHum = 100;
   //if(nodeInfo->SoilHum<0)   // Useless as it's uint?
   //  nodeInfo->SoilHum = 0;
   
   // Calculations to be performed in parser_injector
   // JChavez Hack: Store Vpp4 in SoilHum
   nodeInfo->SoilHum=measuredSoil.SCEN_VPP[3];
   
   
#if defined(BOARD_DEBUG)&&defined(HAS_HX711)
   // HX711 scale
   // SHORT-BUTTON-PRESS  the measure is Offset
   // LONG-BUTTON-PRESS   computes scale
   // NOTE: as the returned value is the scaled one, calibration must be made
   // prior to get correct values
   hx711Value=hx711_procedure();
   measuredSoil.WEIGHT=hx711Value * CALIB_SCALE;
#endif
   
#else
   nodeInfo->SoilHum = 32000;
#endif

#ifdef SENSOR_CO2_COZIR
	nodeInfo->CO2 = 32000;        //to prevent no changing value
	error = cozirGetResult(&cozir, 64);
	if(error != 0) nodeInfo->CO2 = 32000 + error;
	else  nodeInfo->CO2 = cozir.CO2;
	dbg_print("CO2-Value = ");
	print_BCD(nodeInfo->CO2,0);
	dbg_print("\r\n");
	
	if(nodeInfo->Humid > 900 && nodeInfo->Humid < 32000){
		Cozir_Humid_Counter = 1;
		dbg_print("Cozir Sleep Off\r\n");
	}
	else{
		Cozir_Humid_Counter = 0;
		dbg_print("Cozir Sleep On\r\n");
	}
	
	if(Cozir_Humid_Counter == 0) {
		cozirDisable();
	}
#endif

#ifdef SENSOR_MANDARIN
//reading analog values produced by Sensor
  MandarinRead(&measuredMandarin);
  
	nodeInfo->SoilHum = CALIB_WCC_ADD + CALIB_WCC_MULT * measuredMandarin.mandarin_wc;				//get read value and multiply to calibrate
	dbg_print("SoilHum = "); print_BCD(nodeInfo->SoilHum, 0); dbg_print("\r\n");

#ifdef MANDARIN_TEMP
  nodeInfo->SoilTemp = measuredMandarin.mandarin_ec_temp * CALIB_MANDARIN_TEMP;         //TODO: Multiply by calibration value
  dbg_print("SoilTemp = "); print_BCD(nodeInfo->SoilTemp, 0); dbg_print("\r\n");

#elif defined MANDARIN_EC
  nodeInfo->SoilEC = measuredMandarin.mandarin_ec_temp * CALIB_MANDARIN_EC;           //TODO: Multiply by calibration value
  dbg_print("SoilEC = "); print_BCD(nodeInfo->SoilEC, 0); dbg_print("\r\n");
#endif

#else
  nodeInfo->SoilHum  = 32000;
  nodeInfo->SoilEC   = 32000;
  nodeInfo->SoilTemp = 32000;
#endif

#ifdef SENSOR_WATERMELON
//reading analog values produced by Sensor
  WatermelonRead(&measuredWatermelon);
  
	nodeInfo->WatermelonWc = measuredWatermelon.watermelon_wc;		/*CALIB_WCC_ADD + CALIB_WCC_MULT **/ 		//get read value and multiply to calibrate
	dbg_print("WatermelonWc = "); print_BCD(nodeInfo->WatermelonWc, 0); dbg_print("\r\n");

#ifdef WATERMELON_TEMP
  nodeInfo->WatermelonTemp = measuredWatermelon.watermelon_temp ;//* CALIB_MANDARIN_TEMP;         //TODO: Multiply by calibration value
  dbg_print("WatermelonTemp = "); print_BCD(nodeInfo->WatermelonTemp, 0); dbg_print("\r\n");

#ifdef WATERMELON_EC
  nodeInfo->WatermelonEC = measuredWatermelon.watermelon_ec ;//* CALIB_MANDARIN_EC;           //TODO: Multiply by calibration value
  dbg_print("WatermelonEC = "); print_BCD(nodeInfo->WatermelonEC, 0); dbg_print("\r\n");
#endif

#else
  nodeInfo->WatermelonWc  = 32000;
	nodeInfo->WatermelonTemp = 32000;
  nodeInfo->WatermelonEC   = 32000;

#endif
#endif
}

// Fill data structure
// and return bytes2send
// --------------------------------
uint8_t compose_msg (uint16_t *IdxData, node_info *NodeInfo) {
   uint8_t batt;
   uint8_t bytes2send;
   uint16_t vBatt;
   
   // Measure sensors & batt
   sensors_read(NodeInfo);
   
   battery_measure(&batt);
   NodeInfo->batt=(uint16_t)batt;
   
   // Measure the batteries voltage
   vBatt=get_batt_voltage();
   // TODO: better store in NodeInfo?
#ifdef DEBUG_PRINT
   dbg_print("vBatt="); print_unsBCD(vBatt,0); dbg_print("\n\r");
#endif
   // JChavez Hack
   NodeInfo->batt = vBatt;
   
   //EUTANASIA: If vbat is below VBAT_THRESHOLD_TX it don't let TX.
   if(batt < VBAT_THRESHOLD_TX)
     bytes2send = 0;
   else{
      // Store common measurements, Air node by default           
      IdxData[0]=NodeInfo->msg_type; 
      
      //Air measurements		// Message order (in UART)
      if (IdxData[0]==MSG_MEASUREMENT_AIR){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=NodeInfo->IR;        // Field  10
	 bytes2send=20;
      }
		// Add CO2 Level measurements
      if (IdxData[0]==MSG_MEASUREMENT_AIR_CO2){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=NodeInfo->CO2;    	 // Field  10
	 bytes2send=20;
		}
	
      // Add Soil measurements
      if (IdxData[0]==MSG_MEASUREMENT_SOIL){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=NodeInfo->WatermelonWc;//SoilHum;    // Field  10
	 IdxData[10]=NodeInfo->WatermelonTemp; //SoilTemp // Field  11
	 IdxData[11]=NodeInfo->WatermelonEC; //SoilEC// Field  12
	 bytes2send=24; 
      }
	     // Add Watermelon measurements
      if (IdxData[0] == MSG_MEASUREMENT_WATERMELON){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=NodeInfo->WatermelonWc;    // Field  10
	 IdxData[10]=NodeInfo->WatermelonTemp;  // Field  11
	 IdxData[11]=NodeInfo->WatermelonEC; // Field  12
	 bytes2send=24; 
      }
      // Add Water Level measurements
      if (IdxData[0]==MSG_MEASUREMENT_WATER_LEV){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=measuredSoil.NTC_TEMP;     // Field 10
	 IdxData[10]=measuredSoil.SCEN_VPP[3]; // Field 11
	 IdxData[11]=measuredSoil.VD;          // Field 12
	 bytes2send=24; 
      }
      
			//Soil and CO2 measurements added
			if (IdxData[0]==MSG_MEASUREMENT_COMBI){
	 IdxData[0]=NodeInfo->msg_type;  // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID; // Field  2
	 IdxData[2]=NodeInfo->nodeID;    // Field  3
	 IdxData[3]=NodeInfo->batt;      // Field  4
	 IdxData[4]=NodeInfo->signal;    // Field  5
	 IdxData[5]=NodeInfo->tsample;   // Field  6
	 IdxData[6]=NodeInfo->PAR;       // Field  7
	 IdxData[7]=NodeInfo->Temp;      // Field  8
	 IdxData[8]=NodeInfo->Humid;     // Field  9
	 IdxData[9]=NodeInfo->CO2;    	 // Field  10
	 IdxData[10]=NodeInfo->SoilHum;    // Field  11
	 IdxData[11]=NodeInfo->SoilTemp;  // Field  12
	 IdxData[12]=NodeInfo->SoilEC; // Field  13
	 bytes2send=26;
		}
      
      // Add debug measurements
      if (IdxData[0]==MSG_MEASUREMENT_DEBUG){
	 IdxData[0]=NodeInfo->msg_type;   // Field  0  // Field  1 (lengthBytes)
	 IdxData[1]=NodeInfo->centralID;  // Field  2
	 IdxData[2]=NodeInfo->nodeID;     // Field  3
	 IdxData[3]=NodeInfo->batt;       // Field  4
	 IdxData[4]=NodeInfo->signal;     // Field  5
	 IdxData[5]=NodeInfo->tsample;    // Field  6
	 IdxData[6]=NodeInfo->PAR;        // Field  7
	 IdxData[7]=NodeInfo->Temp;       // Field  8
	 IdxData[8]=NodeInfo->Humid;      // Field  9
	 IdxData[9]=NodeInfo->SoilHum;    // Field  10
	 IdxData[10]=NodeInfo->SoilTemp;  // Field  11
	 IdxData[11]=NodeInfo->SoilEC;    // Field  12
	 
	 IdxData[12]=measuredSoil.ALG_ID;    // Field  13
	 IdxData[13]=measuredSoil.ALG_FREQ;  // Field  14
	 IdxData[14]=measuredSoil.NTC_TEMP;  // Field  15
	 
	 IdxData[15]=measuredSoil.VD;        // Field  16
	 IdxData[16]=measuredSoil.WEIGHT;    // Field  17
		    
	 IdxData[17]=measuredSoil.SCEN_VPP[0]; // Field 18
	 IdxData[18]=measuredSoil.SCEN_VPP[1]; // Field 19
	 IdxData[19]=measuredSoil.SCEN_VPP[2]; // Field 20
	 IdxData[20]=measuredSoil.SCEN_VPP[3]; // Field 21
	 IdxData[21]=measuredSoil.SCEN_VPP[4]; // Field 22
	 IdxData[22]=measuredSoil.SCEN_VPP[5]; // Field 23
	 
	 IdxData[23]=measuredSoil.SCEN_VM[0];  // Field 24
	 IdxData[24]=measuredSoil.SCEN_VM[1];  // Field 25
	 IdxData[25]=measuredSoil.SCEN_VM[2];  // Field 26
	 IdxData[26]=measuredSoil.SCEN_VM[3];  // Field 27
	 IdxData[27]=measuredSoil.SCEN_VM[4];  // Field 28
	 IdxData[28]=measuredSoil.SCEN_VM[5];  // Field 29
	 //
	 bytes2send=58;	
	 //IdxData[29]=vBatt;  // Field 29      
	 //bytes2send=60;	
	 //Hack
	 IdxData[11]=vBatt;
      }
   }
   return bytes2send;
}
