#ifndef SOIL_H
#define SOIL_H

#include "stm32l0xx.h"
#include "config.h"

// Mux related
#define mux_off()      mux_select(0)
#define mux_R0()       mux_select(1)
#define mux_R1()       mux_select(2)
#define mux_R2()       mux_select(3)

void mux_init(void);
void mux_select(uint8_t);

// Xtal related
#define xtal_100()     xtal_enable(1)
#define xtal_1()       xtal_enable(2)
#define xtal_off()     xtal_disable()

void xtal_init(void);
void xtal_disable(void);
void xtal_enable(uint8_t);

// Soil related
typedef struct{
   uint16_t CH1; // Alg 1/2: Vtop
   uint16_t CH2; // Alg 1/2: Vbot
   uint16_t CH8; // NTC voltage divider
   uint16_t CH9; // Useless?
} analog_result;

typedef struct{
   uint8_t  ALG_ID;      // Algorithm to use: 1,2,3
   uint16_t NTC_TEMP;    // NTC temperature in the leg (raw ADC)
   uint16_t WEIGHT;      // HX711 obtained weigth
   // Algorithm 1, and 2 
   uint16_t SCEN_VM[6];  // Received Mean Voltage
   uint16_t SCEN_VPP[6]; // Received Peak-to-Peak Voltage
   uint16_t VD;          // Diode Voltage Drop
   uint16_t ALG_FREQ;    // Alg2: Freq to apply
                         // Alg3: Measured Freq 
} soil_result;


uint8_t soil_init(uint8_t alg);
uint8_t soil_adc_read(analog_result*, uint8_t );
uint8_t soil_deinit(void);

void soil_procedure (soil_result *);
void soil_env_analyze(analog_result *AdcRes, uint8_t, soil_result *SoilRes);

void soil_proc_alg1(soil_result* Result);
void soil_proc_alg2(soil_result* Result);
void soil_proc_alg3(soil_result* Result);

#endif
