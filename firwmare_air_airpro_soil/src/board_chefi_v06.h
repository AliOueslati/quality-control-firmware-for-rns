/*
 * Board pinout of Chefi 0.6 (and 0.5)
 */

#define BOARD_CHEFI_V06

// LEDs
#define LED1_PORT  GPIOA
#define LED1_PIN   PIN12
#define LED_GREEN  1
#define LED2_PORT  GPIOA
#define LED2_PIN   PIN8
#define LED_RED    2
//#define LED3_PORT  GPIOB
//#define LED3_PIN   PIN4
//#define LED_BLUE   3
// LEDs are inverted compared to v2k
#define GPIO_LED_ON GPIO_SET
#define GPIO_LED_OFF GPIO_CLEAR

// Button
#define BUTTON_PORT GPIOA  // Must be GPIOA, see button.c
#define BUTTON_PIN  PIN11

// LORA RADIO
#define LORA_RESET_PORT GPIOA
#define LORA_RESET_PIN PIN3
#define LORA_SPI_CS_PORT GPIOA
#define LORA_SPI_CS_PIN PIN4
#define LORA_DIO0_PORT GPIOB
#define LORA_DIO0_PIN PIN14
#define LORA_DIO1_PORT GPIOB
#define LORA_DIO1_PIN PIN13
#define LORA_DIO2_PORT GPIOB
#define LORA_DIO2_PIN PIN12
#define LORA_DIO4_PORT GPIOB
#define LORA_DIO4_PIN PIN15

// CC1101 Radio
#define CC1101_IRQ_PORT_GPIOB
#define CC1101_IRQ_PORT GPIOB
#define CC1101_IRQ_PIN  PIN2  // SPI-IRQ1 on schematic, GDO2 pin on CC1101
#define CC1101_GDO0_PORT GPIOB
#define CC1101_GDO0_PIN  PIN10 // SPI-CE1 on schematic
#define CC1101_SPI_CS_PORT GPIOB
#define CC1101_SPI_CS_PIN  PIN1  // SPI-CSN1 on schematic

// SD card
#define SD_SPI_CS_PORT GPIOB
#define SD_SPI_CS_PIN  PIN0  // SPI-CE2 on schematic

// SPI
#define SPI_SCK_PORT GPIOA
#define SPI_SCK_PIN  PIN5
#define SPI_MISO_PORT GPIOA
#define SPI_MISO_PIN  PIN6
#define SPI_MOSI_PORT GPIOA
#define SPI_MOSI_PIN  PIN7

// I2C
#define I2C_SCL_PORT GPIOB
#define I2C_SCL_PIN  PIN6
#define I2C_SDA_PORT GPIOB
#define I2C_SDA_PIN  PIN7

// Bluetooth
#define BT_KEY_PORT GPIOB
#define BT_KEY_PIN  PIN15
#define BT_RST_PORT GPIOA
#define BT_RST_PIN  PIN8

// Soil Sensor
#define SOIL_ADC1_PORT     GPIOA
#define SOIL_ADC1_PIN      PIN0
#define SOIL_ADC2_PORT     GPIOA
#define SOIL_ADC2_PIN      PIN1

#define SOIL_X1_STZ_PORT   GPIOB
#define SOIL_X1_STZ_PIN    PIN9
#define SOIL_X2_STZ_PORT   GPIOB
#define SOIL_X2_STZ_PIN    PIN8

#define SOIL_R1_0_PORT     GPIOB
#define SOIL_R1_0_PIN      PIN4
#define SOIL_R1_1_PORT     GPIOB
#define SOIL_R1_1_PIN      PIN5

#define SOIL_NTC_ADC_PORT  GPIOA
#define SOIL_NTC_ADC_PIN   PIN4
#define SOIL_NTC_VDD_PORT  GPIOC
#define SOIL_NTC_VDD_PIN   PIN13


