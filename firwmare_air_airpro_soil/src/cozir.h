#ifndef	COZIR_H
#define	COZIR_H

#include "stm32l0xx.h"

#define		COZIR_STREAM_MODE		"K 1\r\n"
#define		COZIR_POLLING_MODE		"K 2\r\n"
#define		COZIR_COMMAND_MODE		"K 0\r\n"

#define		COZIR_FILTER_1			"A 1\r\n"       //used as standard setting, for other filters delay has to be adjusted
#define		COZIR_FILTER_2			"A 2\r\n"
#define		COZIR_FILTER_4			"A 4\r\n"
#define		COZIR_FILTER_8			"A 8\r\n"
#define		COZIR_FILTER_16			"A 16\r\n"
#define		COZIR_FILTER_32			"A 32\r\n"

#define		COZIR_CO2_FILTERED		"Z\r\n"
#define		COZIR_CO2_UNFILTERED	"z\r\n"
//#define		COZIR_HUMIDITY			"H\r\n"
//#define		COZIR_LIGHT				"L\r\n"
//#define		COZIR_TEMPERATURE		"T\r\n"

#define		COZIR_RECENT_FIELD		"Q\r\n"
#define		COZIR_VERSION_ID		"Y\r\n"
#define		COZIR_RETURN_CONFIG		"*\r\n"
#define		COZIR_RETURN_MULTIPLIER	".\r\n"

//	Zero Point Calibration		not implemented
//	#define		COZIR_FINETUNE		"F ##### #####\r\n"		//# is for numbers that are not yet implemented
//	#define		COZIR_MANUAL_ZEROPOINT	"u #####\r\n"
	#define		COZIR_AIR_ZEROPOINT		"G\r\n"
//	#define		COZIR_NITROGEN_ZEROPOINT	"U\r\n"
//	#define		COZIR_GAS_ZEROPOINT			"X #####\r\n"	//using known gas calibration

//	Auto Calibration
#define COZIR_AC_OFF	"@ 0\r\n"			//disables cozir auto calibration

/*//	EEPROM						not implemented
//	#define		COZIR_SET_EEPROM	"P ### ###\r\n"
//	#define		COZIR_RETURN_EEPROM	"p ###\r\n"

//	Output fields				not implemented
//	#define		COZIR_OUTPUT_FIELD	"M #####\r\n"

//	Span Calibration			not implemented
//	#define		COZIR_SET_SPAN		"S #####\r\n"
//	#define		COZIR_RETURN_SPAN	"s #####\r\n"*/


extern uint16_t  cozir_CO2;                //CO2 value from Sensor


typedef struct{
  
  uint16_t  CO2;        //cozir_result has the complete sensor reading
  
} cozir_result;

void cozirEnable(void);
void cozirDisable(void);
void cozirInit(void);
void cozirCalibrate(void);
void cozirPrint(char *Buf);
void cozirSend (uint8_t *Buf, uint16_t Size, uint8_t HexFlag);
uint16_t cozirGetResult(cozir_result* result, uint16_t min_count);

void cozirSendCmd(char *Buf);

#endif
