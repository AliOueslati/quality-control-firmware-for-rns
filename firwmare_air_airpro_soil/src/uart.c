#include "stm32l0xx.h"
#include "gpio.h" // needed for uart_init()
#include "rcc.h"  // needed for uart_init()
#include "uart.h"

#define UART1_TX_BUF_SIZE   1024 /**< UART Buffer Size: Must be power of 2*/
#define UART1_TX_BUF_MASK   UART1_TX_BUF_SIZE-1 /**< UART Buffer Mask: UART_TX_BUF_SIZE - 1*/

#define UART2_TX_BUF_SIZE   2    /**< UART Buffer Size: Must be power of 2*/
#define UART2_TX_BUF_MASK   UART2_TX_BUF_SIZE-1 /**< UART Buffer Mask: UART_TX_BUF_SIZE - 1*/

#define LPUART1_TX_BUF_SIZE   256 /**< UART Buffer Size: Must be power of 2*/
#define LPUART1_TX_BUF_MASK   LPUART1_TX_BUF_SIZE-1 /**< UART Buffer Mask: UART_TX_BUF_SIZE - 1*/

static uint8_t uart1_tx_buf[UART1_TX_BUF_SIZE];  /**< UART1 TX Buffer*/ 
static uint16_t uart1_tx_tail=0;                /**< UART1 next character to process*/ 
static uint16_t uart1_tx_head=0;                /**< UART1 where to store next character to send*/ 

static uint8_t uart2_tx_buf[UART2_TX_BUF_SIZE];  /**< UART2 TX Buffer*/ 
static uint16_t uart2_tx_tail=0;                /**< UART2 next character to process*/ 
static uint16_t uart2_tx_head=0;                /**< UART2 where to store next character to send*/   

static uint8_t lpuart1_tx_buf[LPUART1_TX_BUF_SIZE];  /**< LPUART1 TX Buffer*/ 
static uint16_t lpuart1_tx_tail=0;                /**< LPUART1 next character to process*/ 
static uint16_t lpuart1_tx_head=0;                /**< LPUART1 where to store next character to send*/ 

void UART1_ERROR_HANDLER(void);
void UART2_ERROR_HANDLER(void);
void LPUART1_ERROR_HANDLER(void);

void UART1_OVERFLOW_HANDLER(void);
void UART2_OVERFLOW_HANDLER(void);
void LPUART1_OVERFLOW_HANDLER(void);

/////////* POINTER FUNCTIONS *////////////

void (*UART1_ON_RX_BYTE)(uint8_t);
void (*UART2_ON_RX_BYTE)(uint8_t);
void (*LPUART1_ON_RX_BYTE)(uint8_t);

void (*UART1_ON_ERROR)(void);
void (*UART2_ON_ERROR)(void);
void (*LPUART1_ON_ERROR)(void);

/////////* UART CONFIGURATION *////////////
extern uint32_t SystemCoreClock;

void UART_ENABLE(USART_TypeDef *USART, uint32_t BAUDRATE, uint32_t MODE){
   uint32_t baudReg;
   
   USART->CR1       =MODE ;	   
   USART->CR3       =UART_ERROR_INTERRUPT ;		
   
   baudReg=SystemCoreClock/BAUDRATE;
   
   USART->BRR       =baudReg;     
   
   /*Enable Uart*/
   USART->CR1      |=0x00000001;     
}

void LPUART_ENABLE(USART_TypeDef *USART, uint32_t BAUDRATE, uint32_t MODE){
   uint32_t baudReg;
   
   RCC->CCIPR       =1UL<<10;              //System clock
   
   USART->CR1       =MODE ;	   
   USART->CR3       =UART_ERROR_INTERRUPT ;		
   
   baudReg=(SystemCoreClock<<8)/BAUDRATE;
   
   USART->BRR       =baudReg;     
   
   /*Enable Uart*/
   USART->CR1      |=0x00000001;     
}


/////////* RX FUNCTION ASSOCIATION *////////////

void UART1_SET_RX_FUNCTION(void (*PFUNCTION)(uint8_t)){
   UART1_ON_RX_BYTE = PFUNCTION;  
}

void UART2_SET_RX_FUNCTION(void (*PFUNCTION)(uint8_t)){
   UART2_ON_RX_BYTE = PFUNCTION;  
}

void LPUART1_SET_RX_FUNCTION(void (*PFUNCTION)(uint8_t)){
   LPUART1_ON_RX_BYTE = PFUNCTION;  
}

void UART1_SET_ERROR_FUNCTION(void (*PFUNCTION)(void)){
   UART1_ON_ERROR = PFUNCTION;  
}

void UART2_SET_ERROR_FUNCTION(void (*PFUNCTION)(void)){
   UART2_ON_ERROR = PFUNCTION;  
}

void LPUART1_SET_ERROR_FUNCTION(void (*PFUNCTION)(void)){
   LPUART1_ON_ERROR = PFUNCTION;  
}

/////////* INTERRUPT UART1 HANDLER *////////////
void USART1_IRQHandler (void)
{           
 
  if (USART1->ISR & 0x08)                              //OverRun Error
  {
		USART1->ICR |= 0x08;
		UART1_ERROR_HANDLER();
  }
  
  if (USART1->ISR & 0x04)                              //Noise Error
  {
    USART1->ICR |= 0x04;
		UART1_ERROR_HANDLER();
  }
  
  if (USART1->ISR & 0x02)                              //Framing Error
  {
    USART1->ICR |= 0x02;
	//	UART1_ERROR_HANDLER();
  } 
 
	if (USART1->ISR & 0x20)                                  //RX
  {     
		UART1_RECEIVE((uint8_t)(USART1->RDR & (uint8_t)0x00FF));	
  }
  else if (USART1->ISR & 0x80)                              //TX
  {
    
		if (uart1_tx_tail!=uart1_tx_head)
      {     
		    USART1->TDR=uart1_tx_buf[uart1_tx_tail];
        uart1_tx_tail = (uart1_tx_tail + 1) & UART1_TX_BUF_MASK;  		  
      }
      else
      {
        USART1->CR1   &= ~(1<<7);                      //TEI Disabled 
      }
   }
 } 

 /////////* INTERRUPT UART2 HANDLER *////////////
void USART2_IRQHandler (void)
{           
 
  if (USART2->ISR & 0x08)                             //OverRun Error
  {
		USART2->ICR |= 0x08;
		UART2_ERROR_HANDLER();
  }
  
  if (USART2->ISR & 0x04)                              //Noise Error
  {
    USART2->ICR |= 0x04;
		UART2_ERROR_HANDLER();
  }
  
  if (USART2->ISR & 0x02)                              //Framing Error
  {
    USART2->ICR |= 0x02;
	//	UART2_ERROR_HANDLER();
  } 
	
  if (USART2->ISR & 0x20)                              //RX
  {    
		UART2_RECEIVE((uint8_t)(USART2->RDR & (uint8_t)0x00FF));		
  }
  else if (USART2->ISR & 0x80)                              //TX
  {
    if (uart2_tx_tail!=uart2_tx_head)
      {     
		    USART2->TDR=uart2_tx_buf[uart2_tx_tail];
        uart2_tx_tail = (uart2_tx_tail + 1) & UART2_TX_BUF_MASK;  			  
      }
      else
      {
        USART2->CR1   &= ~(1<<7);                      //TEI Disabled 
      }
    }
 } 

/////////* INTERRUPT LPUART1 HANDLER *////////////
// JChavez (check)
void LPUART1_IRQHandler (void) {           
   
   if (LPUART1->ISR & 0x08) {                           //OverRun Error
      LPUART1->ICR |= 0x08;
      LPUART1_ERROR_HANDLER();
   }
   
   if (LPUART1->ISR & 0x04) {                             //Noise Error
      LPUART1->ICR |= 0x04;
      LPUART1_ERROR_HANDLER();
   }
   
   if (LPUART1->ISR & 0x02) {                             //Framing Error
      LPUART1->ICR |= 0x02;
      //	UART1_ERROR_HANDLER();
   } 
   
   if (LPUART1->ISR & 0x20) {                            //RX
      LPUART1_RECEIVE((uint8_t)(LPUART1->RDR & (uint8_t)0x00FF));	
   }
   else if (LPUART1->ISR & 0x80) {                        //TX
      if (lpuart1_tx_tail!=lpuart1_tx_head) {
 	 LPUART1->TDR=lpuart1_tx_buf[lpuart1_tx_tail];
	 lpuart1_tx_tail = (lpuart1_tx_tail + 1) & LPUART1_TX_BUF_MASK;  		  
      }
      else {
	 LPUART1->CR1   &= ~(1<<7);                      //TEI Disabled 
      }
   }
 } 


/////////* UART1 SEND BYTE *////////////
void UART1_SEND(uint8_t Buf){
  uint16_t bufTmpIndex;

  bufTmpIndex =(uart1_tx_head + 1) & UART1_TX_BUF_MASK;

  if(bufTmpIndex==uart1_tx_tail)
  {
    UART1_OVERFLOW_HANDLER();
  }
  else
  {
    uart1_tx_buf[uart1_tx_head]=Buf;
    uart1_tx_head = bufTmpIndex;

    if ((USART1->CR1  & 0x80)==0)			              
    {
      USART1->CR1   |= 1<<7; 
    }
   
  }             
}

/////////* UART2 SEND BYTE *////////////
void UART2_SEND(uint8_t Buf){
  uint16_t  bufTmpIndex;

  bufTmpIndex =(uart2_tx_head + 1) & UART2_TX_BUF_MASK;

  if(bufTmpIndex==uart2_tx_tail)
  {
    UART2_OVERFLOW_HANDLER();
  }
  else
  {
    uart2_tx_buf[uart2_tx_head]=Buf;
    uart2_tx_head = bufTmpIndex;

    if ((USART2->CR1  & 0x80)==0)			              
    {
      USART2->CR1   |= 1<<7; 
    }
   
  }             
}

/////////* LPUART1 SEND BYTE *////////////
// JChavez (check)
void LPUART1_SEND(uint8_t Buf){
   uint16_t bufTmpIndex;
   
   bufTmpIndex =(lpuart1_tx_head + 1) & LPUART1_TX_BUF_MASK;
   
   if(bufTmpIndex==lpuart1_tx_tail)  {
      LPUART1_OVERFLOW_HANDLER();
   }
   else  {
      lpuart1_tx_buf[lpuart1_tx_head]=Buf;
      lpuart1_tx_head = bufTmpIndex;
      
      if ((LPUART1->CR1  & 0x80)==0) {
	 LPUART1->CR1   |= 1<<7; 
      }
      
   }             
}

/////////* UART IN TX PROCESS *////////////
uint8_t UART_IN_TX(USART_TypeDef *USART){
   uint8_t status;
   if (USART->CR1 & 1<<7) status=1;
   else status=0; 
   return status;	
}

/////////* UART1 RECEIVE BYTE *////////////
void UART1_RECEIVE(uint8_t Buf){
   (*UART1_ON_RX_BYTE)(Buf);
}

/////////* UART2 RECEIVE BYTE *////////////
void UART2_RECEIVE(uint8_t Buf){
   (*UART2_ON_RX_BYTE)(Buf);
}

/////////* UART1 RECEIVE BYTE *////////////
void LPUART1_RECEIVE(uint8_t Buf){
   (*LPUART1_ON_RX_BYTE)(Buf);
}

/////////* ERROR MANAGEMENT *////////////

void UART1_OVERFLOW_HANDLER(void){
   (*UART1_ON_ERROR)();
}

void UART2_OVERFLOW_HANDLER(void){
   (*UART2_ON_ERROR)();
}

void LPUART1_OVERFLOW_HANDLER(void){
   (*LPUART1_ON_ERROR)();
}

void UART1_ERROR_HANDLER(void){
   (*UART1_ON_ERROR)();
}

void UART2_ERROR_HANDLER(void){
   (*UART2_ON_ERROR)();
}

void LPUART1_ERROR_HANDLER(void){
   (*LPUART1_ON_ERROR)();
}
