#include "stm32l0xx.h"
#include "hexutil.h"
#include "uart.h"
#include "print.h"
#include "rcc.h"
#include "gpio.h"
#include "config.h"
#include "string.h"

void dummy_functionERROR(void) {
}
void dummy_functionREAD(uint8_t Buf) {
}

void dbg_print_init() {
   // TODO:
   // Some config to allow the same file can be used by Remote and Central?
#ifdef DEBUG_PRINT
#ifdef REMOTE_NODE
   //Pull up to Rx pin to avoid Break (if not present)
   SET_GPIO_PULL_UP(   UART1_RX_PORT, UART1_RX_PIN);     

   // Configure Pins TX1, RX1 as UART1
   SET_GPIO_AS_AF(     UART1_TX_PORT, UART1_TX_PIN, AF4);
   SET_GPIO_AS_AF(     UART1_RX_PORT, UART1_RX_PIN, AF4);
   
   RCC_ENABLE_APB2_PERIPHERAL(USART1_APB2);  // Enable USART1 clock
   UART_ENABLE(USART1,9600,UART_TX_RX);
   
   UART1_SET_RX_FUNCTION(dummy_functionREAD);
   UART1_SET_ERROR_FUNCTION(dummy_functionERROR);
   
   NVIC_SetPriority(USART1_IRQn,2);          //Low priority
   NVIC_EnableIRQ(USART1_IRQn);
   
   /*
   SET_GPIO_AS_AF(UART1_RX_PORT, UART1_RX_PIN, AF4);  // Configure Pins PA9 (TX1), PA10 (RX1) as AF4
   SET_GPIO_AS_AF(UART1_TX_PORT, UART1_TX_PIN, AF4);
   RCC_ENABLE_APB2_PERIPHERAL(USART1_APB2);  // Enable USART1 clock
   UART_ENABLE(USART1,9600,UART_TX_RX);
   UART1_SET_RX_FUNCTION(dummy_functionREAD);
   UART1_SET_ERROR_FUNCTION(dummy_functionERROR);
   NVIC_SetPriority(USART1_IRQn,0);          //Low priority
   NVIC_EnableIRQ(USART1_IRQn);
    */
   
#elif defined(CENTRAL_NODE)
   //Pull up to Rx pin to avoid Break (if not present)
   SET_GPIO_PULL_UP(   LPUART1_RX_PORT, LPUART1_RX_PIN);     

   // Configure Pins TX1, RX1 as UARTL1
   SET_GPIO_AS_AF(     LPUART1_TX_PORT, LPUART1_TX_PIN, AF4);
   SET_GPIO_AS_AF(     LPUART1_RX_PORT, LPUART1_RX_PIN, AF4);
   
   RCC_ENABLE_APB1_PERIPHERAL(LPUART1_APB1);  // Enable LPUSART1 clock
	 
   LPUART_ENABLE(LPUART1,9600,UART_TX_RX);
   
   LPUART1_SET_RX_FUNCTION(dummy_functionREAD);
   LPUART1_SET_ERROR_FUNCTION(dummy_functionERROR);
   
   NVIC_SetPriority(LPUART1_IRQn,2);          //Low priority
   NVIC_EnableIRQ(LPUART1_IRQn);
   
#else 
    #error "No comm port defined??"
#endif

#endif

}

// JCH DBG print
uint8_t my_strlen(uint8_t *s) {
   uint8_t i;
   
   i= 0;
   while(s[i]) {
      i+= 1;
   }
   return i;
}

void dbg_print(char *Buf) {
#ifdef DEBUG_PRINT
   uint8_t n;
   n=my_strlen((uint8_t *) Buf);
   print((uint8_t *)Buf,n,0);
#endif
}

void srv_print(char *Buf) {
#ifdef DEBUG_PRINT
   uint8_t n;
   n=my_strlen((uint8_t *) Buf);
   print((uint8_t *)Buf,n,0);
#endif
}

// JCH DBG print

/**
 * Print Command.
 * This function send data to the UARTs 2 of the STM32 
 * 
 * \param[in] Buf  Buffer to send.
 * \param[in] Size Size of the buffer
 * \return Error code ::ERROR_CODE
 */     
void print (uint8_t *Buf, uint16_t Size, uint8_t HexFlag) {
#ifdef DEBUG_PRINT
   int16_t i=0;
   uint8_t auxBuf[2];
   
   if(!HexFlag) {
      for (i=0;i<Size;i++) {
         UART_DBG_TX(Buf[i]);
      }
   } 
   else {
      for (i=0;i<Size;i++) {
         byte_to_hexasciichars(Buf[i], auxBuf);
         UART_DBG_TX(auxBuf[0]);
         UART_DBG_TX(auxBuf[1]);
      }
   }
#endif   
}


/**
 * Print Command 8 bits.
 * This function send data to the UARTs 2 of the STM32 
 * 
 * \param[in] Buf  Buffer to send.
 * \param[in] Size Size of the buffer
 * \return Error code ::ERROR_CODE
 */     
void print8 (uint8_t *Buf, uint16_t Size) {
#ifdef DEBUG_PRINT
   
   int16_t i=0;
   uint8_t auxBuf[2];
   
   for (i=0;i<Size;i++) {
      //byte_to_hexasciichars((uint8_t)(Buf[i]>>8), auxBuf);
      //byte_to_hexasciichars((uint8_t)(Buf[i]&0x00FF), auxBuf+2); 
      byte_to_hexasciichars((uint8_t)Buf[i], auxBuf); 
      UART_DBG_TX(auxBuf[0]);
      UART_DBG_TX(auxBuf[1]);
      //UART_DBG_TX(auxBuf[2]);
      //UART_DBG_TX(auxBuf[3]);
   }
#endif
}


/**
 * Print Command 16 bits.
 * This function send data to the UARTs 2 of the STM32 
 * 
 * \param[in] Buf  Buffer to send.
 * \param[in] Size Size of the buffer
 * \return Error code ::ERROR_CODE
 */     
void print16 (uint16_t *Buf, uint16_t Size) {
#ifdef DEBUG_PRINT
   
   int16_t i=0;
   uint8_t auxBuf[4];
   
   for (i=0;i<Size;i++) {
      byte_to_hexasciichars((uint8_t)(Buf[i]>>8), auxBuf);
      byte_to_hexasciichars((uint8_t)(Buf[i]&0x00FF), auxBuf+2); 
      UART_DBG_TX(auxBuf[0]);
      UART_DBG_TX(auxBuf[1]);
      UART_DBG_TX(auxBuf[2]);
      UART_DBG_TX(auxBuf[3]);
   }
#endif
}

/**
 * Print Command 32 bits.
 * This function send data to the UARTs 2 of the STM32 
 * 
 * \param[in] Buf  Buffer to send.
 * \param[in] Size Size of the buffer
 * \return Error code ::ERROR_CODE
 */     
void print32 (uint32_t *Buf, uint16_t Size) {
#ifdef DEBUG_PRINT
   int16_t i=0;
   uint8_t auxBuf[8];
   
   for (i=0;i<Size;i++) {
      byte_to_hexasciichars((uint8_t)(Buf[i]>>24), auxBuf);
      byte_to_hexasciichars((uint8_t)((Buf[i]>>16)&0x00FF), auxBuf+2); 
      byte_to_hexasciichars((uint8_t)((Buf[i]>>8)&0x00FF), auxBuf+4);
      byte_to_hexasciichars((uint8_t)(Buf[i]&0x00FF), auxBuf+6); 
      UART_DBG_TX(auxBuf[0]);
      UART_DBG_TX(auxBuf[1]);
      UART_DBG_TX(auxBuf[2]);
      UART_DBG_TX(auxBuf[3]);
      UART_DBG_TX(auxBuf[4]);
      UART_DBG_TX(auxBuf[5]);
      UART_DBG_TX(auxBuf[6]);
      UART_DBG_TX(auxBuf[7]);
   }     
#endif
}

/**
 * Print Command for digits in BCD digit with decimal part. Up to 5 character
 * This function send data to the UARTs  of the STM32 
 * 
 * \param[in] Buf  Buffer to send.
 * \param[in] Size Size of the buffer
 * \return Error code ::ERROR_CODE
 */     
void print_BCD (int16_t Buf, uint8_t decimalPart) {
#ifdef DEBUG_PRINT
   uint16_t intPart,rem;
   int8_t   Ndigits;
   int8_t   k;
   uint8_t  digits[5];
   uint32_t pow=1;
   
   if (Buf<0) {
      UART_DBG_TX(0x2D);
      Buf=-Buf;
   }

   if (Buf!=0) {
      for (k=0;k<decimalPart;k++) {
         pow*=10;
      }
      
      intPart=(uint16_t)(Buf/pow);
      
      if (intPart==0) Ndigits=decimalPart;
      else if (intPart<10) Ndigits=1+decimalPart;
      else if (intPart<100) Ndigits=2+decimalPart;
      else if (intPart<1000) Ndigits=3+decimalPart;
      else if (intPart<10000) Ndigits=4+decimalPart;
      else Ndigits=5;
      
      //Calc digits
      digits[0]=(uint8_t)((Buf/10000)+0x30);
      rem=(Buf%10000);
      digits[1]=(uint8_t)((rem/1000)+0x30);
      rem=(rem%1000);
      digits[2]=(uint8_t)((rem/100)+0x30);
      rem=(rem%100);
      digits[3]=(uint8_t)((rem/10)+0x30);
      digits[4]=(uint8_t)((rem%10)+0x30);
     
      for (k=(5-Ndigits);k<5;k++) {
         if (k==(5-decimalPart))UART_DBG_TX(0x2E);
         UART_DBG_TX(digits[k]);
      }
   }
   else {
      UART_DBG_TX(0x30);
   }
#endif
}
      
// Alternative to print_BCD for uint16_t
// JCH 11 Jul 2016
void print_unsBCD (uint16_t Buf, uint8_t decimalPart) {
#ifdef DEBUG_PRINT
   uint16_t intPart,rem;
   int8_t   Ndigits;
   int8_t   k;
   uint8_t  digits[6];
   uint32_t pow=1;
   
   if (Buf!=0) {
      for (k=0;k<decimalPart;k++) {
         pow*=10;
      }
      
      intPart=(uint16_t)(Buf/pow);
      
      if (intPart==0) Ndigits=decimalPart;
      else if (intPart<10) Ndigits=1+decimalPart;
      else if (intPart<100) Ndigits=2+decimalPart;
      else if (intPart<1000) Ndigits=3+decimalPart;
      else if (intPart<10000) Ndigits=4+decimalPart;
      else if (intPart<100000) Ndigits=5+decimalPart;
      else Ndigits=6;

      //Calc digits
      digits[0]=(uint8_t)((Buf/100000)+0x30);
      rem=(Buf%100000);
      digits[1]=(uint8_t)((Buf/10000)+0x30);
      rem=(Buf%10000);
      digits[2]=(uint8_t)((rem/1000)+0x30);
      rem=(rem%1000);
      digits[3]=(uint8_t)((rem/100)+0x30);
      rem=(rem%100);
      digits[4]=(uint8_t)((rem/10)+0x30);
      digits[5]=(uint8_t)((rem%10)+0x30);
      
      for (k=(6-Ndigits);k<6;k++) {
         if (k==(6-decimalPart))UART_DBG_TX(0x2E);
         UART_DBG_TX(digits[k]);
      }
   }
   else {
      UART_DBG_TX(0x30);
   }
#endif
}
      
