#include  "stm32l0xx.h"
#include  "gpio.h"
#include  "rcc.h"
#include  "config.h"
#include  "uart.h"
#include  "hexutil.h"
#include  "cozir.h"
#include  "print.h"
#include  "delay.h"
#include  "string.h"
#include	"led.h"

#ifndef LED_BLUE
#define LED_BLUE LED_GREEN
#endif

#define TIMEOUT_ERROR             999


//  cozir_read_control checks that every interrupt is received
uint16_t  cozir_read_control;       //controls that CO2 reading is not missing any part of the Msg
uint16_t  cozir_CO2;                //CO2 value from Sensor
uint16_t  cozir_counter = 0;        //number of received CO2 readings
uint16_t  error_control = 1;        //ensures only 1 Debug Msg per received CO2 reading in case of error
uint16_t  result_error = 0;         //variable to store errorvalue
  
void cozirFunctionError(void) {
}

//Interrupt Function that gets the Readings from the CO2 sensor
// Readings will come in this format: ' Z 00000 z 00000\r\n'
void cozirInterruptRead(uint8_t Buf) {
  
//cozir_read_control shows how much of the Msg has been received so far
  
//when 'Z' received and only one Blank Prior (Position 1)
  if(Buf == 'Z' & cozir_read_control == 1){

    error_control = 1;
    cozir_CO2 = 0;
    cozir_read_control++;
  }

//if receiving ' ', 'z' or '\r' just increase counter
  else if((Buf == ' ' || Buf == 'z' || Buf == '\r')) cozir_read_control++;


//if receiving the first few digits (meaning position 3 to 7)
  else if(Buf >= '0' & Buf <= '9' & cozir_read_control >= 3 & cozir_read_control <= 7){

//add received value to variable, *10 because first received is the highest position (12345)
    cozir_CO2 *= 10;
    cozir_CO2 += (Buf - '0');
    
    cozir_read_control++;
    
    if (cozir_read_control == 8){
       dbg_print("Cozir = "); print_BCD(cozir_CO2, 0); dbg_print("\r\n");
    }
  }
	
//if receiving the second row of digits (position 11 to 15)
	else if(Buf >= '0' & Buf <= '9' & cozir_read_control >= 11 & cozir_read_control <= 15) cozir_read_control++;

//last part of Cozir transmission, increase number of received Msgs by 1, reset cozir_read_control
  else if(Buf == '\n'){

    result_error = 0;
    cozir_read_control = 0;

    cozir_counter++;
  }

//when readings are missing/errors enter here
  else{
#ifdef DEBUG_PRINT
//first time one enters here will print when in the transmission error occured
    if(error_control){

//depending on cozir_read_control, the error happened in a different part of the transmission
    if(cozir_read_control == 1)																															dbg_print("Wrong Measurement Type\r\n");
    else if(cozir_read_control == 2 || cozir_read_control == 8 || cozir_read_control == 10)	dbg_print("Blank Space Missing\r\n");
		else if(cozir_read_control >= 3 && cozir_read_control <= 7)															dbg_print("Filtered Value incomplete\r\n");
		else if(cozir_read_control >= 11 && cozir_read_control <= 15)														dbg_print("Unfiltered reading incomplete\r\n");
    else if(cozir_read_control == 16)																												dbg_print("No Carriage Return\r\n");
    else if(cozir_read_control == 17)																												dbg_print("No New Line Command\r\n");
    }
#endif
//set error_control to 0 to prevent dbg_print of errors for rest of the transmission
//reset cozir_read_control
    error_control = 0; 
    cozir_read_control = 0;
  }

}

//get CO2 measurement and return it to sensors.c
  uint16_t cozirGetResult(cozir_result* result, uint16_t min_count){
  uint16_t timeOut = 0;
  
  result_error = 0;
  cozir_counter = 0;

//ensure a minimum number of correctly received readings before returning CO2 value
  while(cozir_counter < min_count){

    if(timeOut == TIMEOUT_ERROR){
      result_error = TIMEOUT_ERROR;

      dbg_print("CozirGetResult TimeOut.");
      return(result_error);
    }
      delay_ms(100);
    
    timeOut++;
  };
  
  result->CO2 = cozir_CO2;
  return(result_error);
}

//Turn on CO2 sensor and delay for warmup time
//warmup time depends on Filter setting
void cozirEnable(){
  
  GPIO_CLEAR(GPIOB, PIN2);
  delay_ms(1200);
}

//Turn of CO2 sensor
void cozirDisable(){
  GPIO_SET(GPIOB, PIN2);
}

//Initialize LPUART1 and CO2 sensor at startup
void cozirInit(void) {
#ifdef SENSOR_CO2_COZIR
  
   SET_GPIO_PULL_UP(CO2_RX_PORT, CO2_RX_PIN);
   SET_GPIO_AS_OUTPUT(GPIOB, PIN2);
  GPIO_SET(GPIOB, PIN2);


   // Configure Pins TX1, RX1 as LPUART1
   SET_GPIO_AS_AF(CO2_TX_PORT, CO2_TX_PIN, AF4);
   SET_GPIO_AS_AF(CO2_RX_PORT, CO2_RX_PIN, AF4);
   
   RCC_ENABLE_APB1_PERIPHERAL(LPUART1_APB1);  // Enable LPUART1 clock
   LPUART_ENABLE(LPUART1,9600,UART_TX_RX);
   
   LPUART1_SET_RX_FUNCTION(cozirInterruptRead);
   LPUART1_SET_ERROR_FUNCTION(cozirFunctionError);
   
   NVIC_SetPriority(LPUART1_IRQn,2);          //Low priority
   NVIC_EnableIRQ(LPUART1_IRQn);

//Turn off CO2 Sensor
   cozir_read_control = 0;
   
   cozirEnable();

#ifndef HAS_RELAY
   cozirDisable();
#endif

#endif
}

/*Calibration function for Cozir
Sensor needs to be outside and no people close to give the most accurate calibration
Starts with 3 blue blinks
*/
void cozirCalibrate(void) {
	cozir_result cozir_reading;
	
	cozirEnable();
	led_on(LED_BLUE); delay_ms(200);


//1. Turns off auto calibration
	cozirPrint(COZIR_AC_OFF);

	led_off(LED_BLUE); delay_ms(200);
	led_on(LED_BLUE); delay_ms(200);

//2. Sets filter to 32
	cozirPrint(COZIR_FILTER_32);

	led_off(LED_BLUE); delay_ms(200);
	led_on(LED_BLUE); delay_ms(200);
	led_off(LED_BLUE);delay_ms(500);

//3. Waits 60 seconds to get acurate readings
	for(int i = 0; i<= 60; i++){
		led_on(LED_BLUE); delay_ms(500);
		led_off(LED_BLUE); delay_ms(500);
	}
	
//4. Sends G command to set readings to 400ppm
	cozirPrint(COZIR_AIR_ZEROPOINT);
	
//5. If next reading 300 <= x <= 500 -> 10s blue led, otherwise 10s red led
	cozirGetResult(&cozir_reading, 2);
	if((300 < cozir_reading.CO2) && (cozir_reading.CO2 < 500)) {
		led_on(LED_BLUE);
		delay_ms(10000);
		led_off(LED_BLUE);
	}
	else {
		led_on(LED_RED);
		delay_ms(10000);
		led_off(LED_RED);
	}

}

//Determine String Length
uint8_t cozirStrlen(uint8_t *s) {
   uint8_t i;
   
   i= 0;
   while(s[i]) {
      i+= 1;
   }
   return i;
}

//For sending command to CO2 sensor
void cozirPrint(char *Buf) {
   uint8_t n;
   n= cozirStrlen((uint8_t *) Buf);
   cozirSend((uint8_t *)Buf,n,0);
}

void cozirSend(uint8_t *Buf, uint16_t Size, uint8_t HexFlag) {
#ifdef  SENSOR_CO2_COZIR
  int16_t i=0;
   uint8_t auxBuf[2];
   
   if(!HexFlag) {
      for (i=0;i<Size;i++) {
         LPUART_TX(Buf[i]);
      }
   }
   else {
      for (i=0;i<Size;i++) {
         byte_to_hexasciichars(Buf[i], auxBuf);
         LPUART_TX(auxBuf[0]);
         LPUART_TX(auxBuf[1]);
      }
   }

#endif

}

//Turning on CO2 sensor, sending Command, Turning it off
void cozirSendCmd(char *Buf){
  GPIO_CLEAR(GPIOB, PIN2);
  delay_ms(1200);
  cozirPrint((char *) Buf);
  delay_ms(10);
  GPIO_SET(GPIOB, PIN2);
}
