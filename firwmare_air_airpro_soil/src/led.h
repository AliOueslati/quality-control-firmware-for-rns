#ifndef LED_H
#define LED_H

#include "stm32l0xx.h"

void led_init(void);
void led_on(uint8_t);
void led_off(uint8_t);
void led_toggle(uint8_t);

#endif
