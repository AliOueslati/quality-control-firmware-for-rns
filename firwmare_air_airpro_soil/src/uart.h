#ifndef UART_H
#define UART_H

#include "stm32l0xx.h"

/*Default UART configuration: Tx Enabled, Rx Enable, Ov=16, Tx interrupt disabled (enabled after), RX interrupt enabled, 1 bit Start, 8 bits data */
#define UART_TX_RX            0x0000002C
#define UART_TX_ONLY          0x00000008
#define UART_ERROR_INTERRUPT  0x00000001

void UART_ENABLE(USART_TypeDef *, uint32_t, uint32_t);
void LPUART_ENABLE(USART_TypeDef *, uint32_t, uint32_t);
uint8_t UART_IN_TX(USART_TypeDef *);
void UART_DISABLE_RX(USART_TypeDef *);

void UART1_SET_RX_FUNCTION(void (*)(uint8_t));
void UART2_SET_RX_FUNCTION(void (*)(uint8_t));
void LPUART1_SET_RX_FUNCTION(void (*)(uint8_t));

void UART1_SET_ERROR_FUNCTION(void (*)(void));
void UART2_SET_ERROR_FUNCTION(void (*)(void));
void LPUART1_SET_ERROR_FUNCTION(void (*)(void));

void UART1_SEND(uint8_t);
void UART2_SEND(uint8_t);
void LPUART1_SEND(uint8_t);

void UART1_RECEIVE(uint8_t);
void UART2_RECEIVE(uint8_t);
void LPUART1_RECEIVE(uint8_t);


#endif
